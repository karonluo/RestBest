package main

// golang datetime format 2006-01-02 15:04:05
import (
	"RestBest/conf"
	"RestBest/pkg/biz"
	"RestBest/pkg/cache"
	"RestBest/pkg/dao"
	"RestBest/pkg/storage"
	"RestBest/web"
	"fmt"
	"os"

	"github.com/ahmetb/go-linq/v3"
	"github.com/kataras/iris/v12"
)

// 初始化 Web 站点
func InitWebSite() {
	file, _ := os.Create("./pid")
	pid := fmt.Sprintf("%d", os.Getpid())
	_, err := file.Write([]byte(pid))
	if err != nil {
		fmt.Println(err.Error())
	}
	file.Close()
	fmt.Println("初始化 Web Server")
	conf.LoadWebConfig("conf/WebConfig.json")
	if dao.InitDatabase() {
		if cache.InitRedisDatabase() {
			biz.InitSysUserToRedis() // 将所有用户初始化到cache
		}
		//biz.InitSystemLogger()
		//biz.InitSysUserToRedis()
		//biz.InitSysFuncPageToRedis()
		//biz.InitDictToRedis()
		// web.ConnectionUWBTagMQTTServer("go_mqtt_client_subscriber") // 链接UWB MQTT 服务，并建立 Web Socket Server.

		//}

	}
	storage.InitStoreServer() // 初始化对象存储服务
}

func main() {
	fmt.Println("==========================================================================")
	InitWebSite()
	fmt.Println("==========================================================================")
	fmt.Printf("Restaurants Management Platform Powered by Karonsoft. Version: %s\r\n", conf.WebConfiguration.Version)
	app := iris.New()
	app.AllowMethods("GET,POST,PUT,DELETE,OPTIONS")
	app.Use(iris.Compression)
	app.Use(web.Before)
	web.RegisterServices(app)
	app.Post("/upload", storage.WSUploadFile)                // 上传文件
	app.Get("/download", storage.WSDownloadFile)             // 下载文件
	app.Get("/nodog/auth", web.NoDogSplashAuthorizedService) // 上网认证
	ros := app.GetRoutes()
	var paths []string
	for _, r := range ros { // 切片 性能高
		paths = append(paths, r.Path)
	}
	linq.From(paths).DistinctByT(
		func(r string) string {
			return r
		}).ToSlice(&paths)
	conf.WebConfiguration.UrlPathList = paths // 所有注册的服务接口地址。
	for _, path := range paths {
		app.Options(path, web.Cors)
	}
	irisConf := iris.WithConfiguration(iris.Configuration{
		EnableOptimizations: true,
		Charset:             "UTF-8",
		DisableStartupLog:   false,
		PostMaxMemory:       conf.WebConfiguration.PostDataMaxMBSize,
	})
	app.Run(iris.Addr(conf.WebConfiguration.Port), irisConf)

}
