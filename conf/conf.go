package conf

import (
	"encoding/json"
	"fmt"
	"os"
)

var WebConfiguration *WebConfig

type DBConfiguration struct {
	Host     string
	Port     string
	User     string
	Password string
	DBName   string
}

type RedisConfiguration struct {
	Hosts    []string
	Password string
}

type WebConfig struct {
	Port                string
	SessionExpireMinute int
	Version             string
	PostDataMaxMBSize   int64
	DBConf              DBConfiguration
	RedisConf           RedisConfiguration
	MQTTServerConf      MQTTServerConfiguration
	EmailSmtpServerConf EmailSmtpServerConfiguration
	UrlPathList         []string
	StorageServerConf   StorageServerConfiguration
}

type MQTTServerConfiguration struct {
	Port           int
	Password       string
	WebSockertPort int
	Broker         string
	User           string
}

type EmailSmtpServerConfiguration struct {
	Host                              string
	Password                          string
	Port                              string
	UserName                          string
	Identity                          string
	ResetSysUserPasswordEmailTemplate string
	ResetSysUserPasswordEmailTimeout  int
}
type StorageServerConfiguration struct {
	Address     string
	AccessKey   string
	SecretKey   string
	API         string
	Path        string
	BucketName  string
	ObjectName  string
	Region      string
	PartSize    int64 // 分部分上传，每部分多少字节
	Concurrency int64 // 最多分多少部分
}

func LoadWebConfig(confpath string) WebConfig {
	fmt.Print("载入配置文件")
	var conf WebConfig
	jsonData, _ := os.ReadFile(confpath)
	//jsonData, _ = tools.Utf8ToGbk(jsonData)
	fmt.Print("..")

	err := json.Unmarshal([]byte(string(jsonData)), &WebConfiguration)
	if err != nil {
		fmt.Println(err.Error())
	}
	// data := make(map[string]interface{})
	// json.Unmarshal(jsonData, &data)
	fmt.Print("..")

	conf = *WebConfiguration
	fmt.Print("..")
	fmt.Println("成功")
	return conf
}
