package entities

import "time"

type User struct {
	Id             string    `gorm:"column:id;primaryKey;type:char(36)"`
	Name           string    `gorm:"column:name;type:varchar(32)"`
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:Creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:Modifier;type:varchar(16)"`
	LineNumber     string    `gorm:"column:LineNumber;type:varchar(16)"`
	mobile         string    `gorm:"column:mobile;type:varchar(16)"`
	Email          string    `gorm:"column:email;type:varchar(32)"`
	Status         string    `gorm:"column:status;type:varchar(8)"`
}

type UserOption = func(opts *User)

func UserOptionMobile(v string) UserOption {
	return func(opts *User) {
		opts.mobile = v
	}
}

func UserOptionEmail(v string) UserOption {
	return func(opts *User) {
		opts.Email = v
	}
}
