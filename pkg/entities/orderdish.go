package entities

import "time"

// 订单菜品
type OrderDish struct {
	Id             string    `gorm:"column:id;primaryKey;type:char(36)"`
	OrderID        string    `gorm:"column:order_id;type:char(36)"`
	DishID         string    `gorm:"column:dishes_id;type:char(36)"`
	Spec           string    `gorm:"column:spec;type:varchar(8)"`
	Price          float64   `gorm:"column:price;type:decimal(6,2)"`
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:modifier;type:varchar(16)"`
	Name           string    `gorm:"column:name;type:varchar(32)"`
	Category       string    `gorm:"column:category;type:varchar(8)"`
	Unit           string    `gorm:"column:unit;type:varchar(8)"`
	Image          string    `gorm:"column:image;type:varchar(64)"`
	SmallImage     string    `gorm:"column:small_image;type:varchar(64)"`
	Banner         string    `gorm:"column:banner;type:varchar(64)"`
	Taste          string    `gorm:"column:taste;type:varchar(64)"`
	Status         string    `gorm:"column:status;type:varchar(8)"`
}
