package entities

import "time"

/*
菜品规格表
*/
type DishSpec struct {
	Id             string    `gorm:"column:id;type:char(36);not null;primaryKey"`
	DishesID       string    `gorm:"column:dishes_id;type:char(36)"`
	Name           string    `gorm:"column:name;type:varchar(32)"`
	Price          float64   `gorm:"column:price;type:decimal(6, 2)"`
	Calories       int32     `gorm:"column:calories"`
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:Creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:Modifier;type:varchar(16)"`
	Status         string    `gorm:"column:status;type:varchar(8)"`
	Unit           string    `gorm:"column:unit;type:varchar(8)"`
}
