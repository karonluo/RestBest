package entities

import "time"

type WiFi struct {
	Id             string    `gorm:"column:id;primaryKey;type:char(36)"`
	RestId         string    `gorm:"column:rest_id;primaryKey;type:char(36)"`
	WifiName       string    `gorm:"column:wifi_name;primaryKey;type:varchar(32)"`
	Password       string    `gorm:"column:password;primaryKey;type:varchar(32)"`
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:Creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:Modifier;type:varchar(16)"`
	Wificrcode     string    `gorm:"column:wifi_crcode;primaryKey;type:varchar(64)"`
}
