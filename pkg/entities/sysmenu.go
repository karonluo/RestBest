package entities

import (
	"time"
)

type SysMenu struct {
	Id             string    `gorm:"column:id;type:char(36);not null;primaryKey"`
	Name           string    `gorm:"column:name;type:varchar(32)"`
	Category       string    `gorm:"column:category;type:varchar(8)"`
	URL            string    `gorm:"column:url;type:varchar(64)"`
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:Creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:Modifier;type:varchar(16)"`
	Status         string    `gorm:"column:status;type:varchar(8)"`
	ParentID       string    `gorm:"column:parent_id;type:char(36)"`
	Haschild       bool      `gorm:"column:haschild;type:boolean"`
}
