package entities

import "time"

type Room struct {
	HallID         string    `gorm:"column:hall_id"`
	RoomID         string    `gorm:"column:room_id;primaryKey"`
	RoomName       string    `gorm:"column:room_name"`
	Status         string    `gorm:"column:Status"`
	CreateDatetime time.Time `gorm:"column:create_datetime"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime"`
	Creator        string    `gorm:"column:Creator"`
	Modifier       string    `gorm:"column:Modifier"`
}
