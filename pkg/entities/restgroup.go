package entities

import "time"

type RestGroup struct {
	Id             string    `gorm:"primary_key;type:char(36);column:id"`
	GroupName      string    `gorm:"type:varchar(32);column:group_name"`
	CreateDatetime time.Time `gorm:"type:timestamp;column:create_datetime"`
	ModifyDatetime time.Time `gorm:"type:timestamp;column:modify_datetime"`
	Creator        string    `gorm:"type:varchar(16);column:Creator"`
	Modifier       string    `gorm:"type:varchar(16);column:Modifier"`
	Status         string    `gorm:"type:VARCHAR(8);column:status"`
	ParentId       string    `gorm:"type:char(36);column:parent_id"`
	Haschild       bool      `gorm:"column:haschild;type:boolean"`
}

type RestGroupForQuery struct {
	Id             string    `gorm:"primary_key;type:char(36);column:id"`
	GroupName      string    `gorm:"type:varchar(32);column:group_name"`
	CreateDatetime time.Time `gorm:"type:timestamp;column:create_datetime"`
	ModifyDatetime time.Time `gorm:"type:timestamp;column:modify_datetime"`
	Creator        string    `gorm:"type:varchar(16);column:Creator"`
	Modifier       string    `gorm:"type:varchar(16);column:Modifier"`
	Status         string    `gorm:"type:VARCHAR(8);column:status"`
	ParentId       string    `gorm:"type:char(36);column:parent_id"`
	Haschild       bool      `gorm:"column:haschild;type:boolean"`
	Restaurants    string
	Tenantusers    string
}
