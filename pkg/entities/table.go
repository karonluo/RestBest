package entities

import "time"

type Table struct {
	Id             string    `gorm:"column:id;primaryKey;type:char(36)"`
	Name           string    `gorm:"column:name;type:varchar(64)"`
	RestID         string    `gorm:"column:rest_id;not null;type:char(36)"`
	HallID         string    `gorm:"column:hall_id;type:char(36)"`
	RoomID         string    `gorm:"column:room_id;type:char(36)"`
	ImageURL       string    `gorm:"column:image_url;type:varchar(64)"`
	Status         string    `gorm:"column:status;type:varchar(8)"`
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:Creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:Modifier;type:varchar(16)"`
	Category       string    `gorm:"column:category;type:varchar(8)"`
	Code           string    `gorm:"column:code;type:varchar(64)"`
}
