package entities

import "time"

type Menu struct {
	Id             string    `gorm:"column:id;primaryKey;type:char(36);not null" `
	RestaurantID   string    `gorm:"column:rest_id;primaryKey;type:char(36);not null" `
	Name           string    `gorm:"column:name;type:varchar(32)" `
	Category       string    `gorm:"column:category;type:varchar(8)" `
	Image          string    `gorm:"column:image;type:varchar(64)" `
	SmallImage     string    `gorm:"column:small_image;type:varchar(64)" `
	Banner         string    `gorm:"column:banner;type:varchar(64)" `
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp" `
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp" `
	Creator        string    `gorm:"column:Creator;type:varchar(16)" `
	Modifier       string    `gorm:"column:Modifier;type:varchar(16)" `
	Order          int       `gorm:"column:order;type:int4"`
	Status         string    `gorm:"column:status;type:varchar(8)" `
	Desc           string    `gorm:"column:desc;type:varchar(256)"`
	Dishes         string    `gorm:"column:dishes;type:varchar(256)"`

	// DishList []FullDish
}

type FullMenuList struct {
	MenuList []FullMenu
}

type FullMenu struct {
	Menu     Menu
	DishList []FullDish
}

// 菜品
type FullDish struct {
	Dish       Dish
	DishSpeces []DishSpec
	Toppings   []FullTopping
}

type FullTopping struct {
	Topping       Topping
	ToppingSpeces []ToppingSpec
}
