package entities

import "time"

type Set struct {
	Id             string    `gorm:"column:id;primaryKey;type:char(36)"`
	RestID         string    `gorm:"column:rest_id;type:char(36)"`
	Name           string    `gorm:"column:name;type:varchar(64)"`
	Price          float64   `gorm:"column:price;type:decimal(6,2)"`
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:modifier;type:varchar(16)"`
	Remark         string    `gorm:"column:remark;type:varchar(64)"`
}
