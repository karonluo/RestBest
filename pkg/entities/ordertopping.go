package entities

import "time"

type OrderTopping struct {
	Id              string    `gorm:"column:ordering_dished_id;type:char(36);not null;primaryKey"`
	OrderDishesID   string    `gorm:"column:order_dishes_id;type:char(36)"`
	ToppingsID      string    `gorm:"column:toppings_id;type:char(36)"`
	Spec            string    `gorm:"column:spec;type:varchar(8)"`
	SpecDisplayname string    `gorm:"column:spec_displayname;type:varchar(64)"`
	Price           float64   `gorm:"column:price;type:decimal(6, 2)"`
	Name            string    `gorm:"column:name;type:varchar(32)"`
	Category        string    `gorm:"column:category;type:varchar(8)"`
	Unit            string    `gorm:"column:unit;type:varchar(8)"`
	Image           string    `gorm:"column:image;type:varchar(64)"`
	SmallImage      string    `gorm:"column:small_image;type:varchar(64)"`
	Banner          string    `gorm:"column:banner;type:varchar(64)"`
	CreateDatetime  time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime  time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator         string    `gorm:"column:Creator;type:varchar(16)"`
	Modifier        string    `gorm:"column:Modifier;type:varchar(16)"`
	Status          string    `gorm:"column:status;type:varchar(8)"`
}
