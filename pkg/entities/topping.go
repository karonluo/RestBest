package entities

import "time"

/*
配料
*/
type Topping struct {
	Id       string `gorm:"column:id;primaryKey;type:char(36)"`
	Name     string `gorm:"column:name;type:varchar(32)"`
	Category string `gorm:"column:category;type:varchar(8)"`
	Unit     string `gorm:"column:unit;type:varchar(8)"`

	Image          string    `gorm:"column:image;type:varchar(64)"`
	SmallImage     string    `gorm:"column:small_image;type:varchar(64)"`
	Banner         string    `gorm:"column:banner;type:varchar(64)"`
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:modifier;type:varchar(16)"`
	Index          int16     `gorm:"column:index;type:int(4)"`

	ToppingSpecList []ToppingSpec
}
