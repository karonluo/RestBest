package entities

import (
	"encoding/json"
	"fmt"
	"time"
)

func UnmarshalSysRole(data []byte) (SysRole, error) {
	var r SysRole
	err := json.Unmarshal(data, &r)
	fmt.Println(err)
	fmt.Println("===================")
	fmt.Println(data)
	return r, err
}

func (r *SysRole) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type SysRole struct {
	Id          string `gorm:"column:id;primaryKey;type:char(36)"`
	Name        string `gorm:"column:name;type:varchar(32)"`
	Description string `gorm:"column:description;type:varchar(128)"`
	// CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	// ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	CreateDatetime time.Time
	ModifyDatetime time.Time

	Creator  string `gorm:"column:creator;type:varchar(16)"`
	Modifier string `gorm:"column:modifier;type:varchar(16)"`
	Status   string `gorm:"column:status;type:varchar(8)"`
}
