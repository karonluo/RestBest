package entities

import "time"

/*
商家
*/
type Restaurant struct {
	Id                 string    `gorm:"column:id;primaryKey"`
	Name               string    `gorm:"column:name;type:varchar(32)"`
	CreateDatetime     time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime     time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator            string    `gorm:"column:creator;type:varchar(16)"`
	Modifier           string    `gorm:"column:modifier;type:varchar(16)"`
	Certificate        string    `gorm:"column:certificate;type:varchar(32)"`
	CertificateDesc    string    `gorm:"column:certificate_desc;type:varchar(512)"`
	Phone              string    `gorm:"column:phone;type:varchar(64)"`
	Address            string    `gorm:"column:address;type:varchar(64)"`
	ServiceCharge      float64   `gorm:"column:service_charge;type:DECIMAL(4, 2)"`
	Tax                float64   `gorm:"column:tax;type:DECIMAL(4, 2)"`
	Status             string    `gorm:"column:status;type:varchar(8)"`
	StartBusinessHours string    `gorm:"column:start_business_hours;type:time"`
	EndBusinessHours   string    `gorm:"column:end_business_hours;type:time"`
	Code               string    `gorm:"column:code;type:varchar(64)"`
}
