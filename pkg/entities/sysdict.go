package entities

import "time"

type SystemDict struct {
	Code           string    `gorm:"column:code;primaryKey;type:varchar(16)"`
	Key            string    `gorm:"column:key;type:varchar(32)"`
	Value          string    `gorm:"column:value;type:varchar(128)"`
	ParentCode     string    `gorm:"column:parent_code;type:varchar(16)" json:"ParentCode"`
	TopCode        string    `gorm:"column:top_code;type:varchar(16)"`
	PathCode       string    `gorm:"column:path_code;type:varchar(128)"`
	DataType       string    `gorm:"column:data_type;type:varchar(16)"`
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:modifier;type:varchar(16)"`
	Status         string    `gorm:"column:status;type:varchar(8)"`
}

func (SystemDict) TableName() string {
	return "system_dict"
}
