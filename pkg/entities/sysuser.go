package entities

import "time"

type SysUser struct {
	Id             string `gorm:"primaryKey"`
	LoginName      string
	DisplayName    string
	Cellphone      string
	Email          string
	Wechat         string
	QQ             string
	IDCardNumber   string `gorm:"column:id_card_number" json:"IDCardNumber"`
	IsDisableLogin bool   `gorm:"column:is_disable_login"` // 指定 字段名为 is_disable_login
	CreateDatetime time.Time
	ModifyDatetime time.Time
	Creator        string
	Modifier       string
	PasswdMD5      string
	Line           string
	UserRole       string //`gorm:"column:user_role"`
	RestCodes      string //`gorm:"column:rest_codes"`
	IsSys          bool   `gorm:"column:isSys" json:"IsSys"`
	Restaurants    string
	RestNames      string
}

type SysUserForCreate struct {
	Id             string `gorm:"primaryKey"`
	LoginName      string
	DisplayName    string
	Cellphone      string
	Email          string
	Wechat         string
	QQ             string
	IDCardNumber   string `gorm:"column:id_card_number" json:"IDCardNumber"`
	IsDisableLogin bool   `gorm:"column:is_disable_login"` // 指定 字段名为 is_disable_login
	CreateDatetime time.Time
	ModifyDatetime time.Time
	Creator        string
	Modifier       string
	PasswdMD5      string
	Line           string
	IsSys          bool `gorm:"column:isSys" json:"IsSys"`
}

type SysUserRole struct {
	Id   string
	Name string
}
