package entities

import "time"

type Order struct {
	Id             string    `gorm:"column:id;primaryKey;type:char(36)"`
	RestID         string    `gorm:"column:rest_id;type:char(36)"`
	TotalAmount    float64   `gorm:"column:total_amount;type:decimal(6,2)"`
	ActualAmount   float64   `gorm:"column:actual_amount;type:decimal(6,2)"`
	RefundAmount   float64   `gorm:"column:refund_amount;type:decimal(6,2)"`
	Status         string    `gorm:"column:status;type:varchar(8)"`
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:modifier;type:varchar(16)"`
	GuestNum       int       `gorm:"column:guest_num;type:int4"`
	Remark         string    `gorm:"column:remark;type:varchar(64)"`
	ServiceCharge  float64   `gorm:"column:service_charge;type:numeric(15,6)"`
	Tax            float64   `gorm:"column:tax;type:numeric(15,6)"`
}
