package entities

import "time"

/*
配料规格
*/
type ToppingSpec struct {
	Id             string    `gorm:"column:id;type:char(36);not null;primaryKey;type:char(36)"`
	ToppingId      string    `gorm:"column:topping_id;type:char(36)"`
	Name           string    `gorm:"column:name;type:varchar(32)"`
	Price          float64   `gorm:"column:price;type:DECIMAL(6, 2)"`
	Calories       int16     `gorm:"column:calories;type:integer"`
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:modifier;type:varchar(16)"`
	Status         string    `gorm:"column:status;type:varchar(8)"`
	Unit           string    `gorm:"column:unit;type:varchar(8)"`
}
