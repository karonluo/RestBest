package entities

import "time"

type TenantUser struct {
	Id             string `gorm:"column:id;type:char(36);primary_key"`
	LoginName      string `gorm:"column:login_name;type:varchar(16)"`
	DisplayName    string `gorm:"column:display_name;type:varchar(16)"`
	Cellphone      string
	Email          string
	Wechat         string
	QQ             string
	IDCardNumber   string    `gorm:"column:id_card_number" json:"IDCardNumber"`
	IsDisableLogin bool      `gorm:"column:is_disable_login"` // 指定 字段名为 is_disable_login
	CreateDatetime time.Time `gorm:"column:create_datetime;type:timestamp"`
	ModifyDatetime time.Time `gorm:"column:modify_datetime;type:timestamp"`
	Creator        string    `gorm:"column:Creator;type:varchar(16)"`
	Modifier       string    `gorm:"column:Modifier;type:varchar(16)"`
	PasswdMD5      string
	Line           string
}
