package storage

import (
	"RestBest/conf"
	"RestBest/pkg/tools"
	"RestBest/web"
	"bytes"
	"fmt"
	"io"

	"os"
	"path/filepath"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/google/uuid"
	"github.com/kataras/iris/v12"
)

var StorageSession *session.Session

func InitStoreServer() {
	var err error
	fmt.Print("初始化对象存储服务...")
	storageConf := conf.WebConfiguration.StorageServerConf
	StorageSession, err = session.NewSession(&aws.Config{
		Credentials:      credentials.NewStaticCredentials(storageConf.AccessKey, storageConf.SecretKey, ""),
		Endpoint:         aws.String(storageConf.Address),
		Region:           aws.String(storageConf.Region),
		S3ForcePathStyle: aws.Bool(true),
	})
	if err != nil {
		fmt.Printf("...失败, 原因是:%s", err.Error())
		tools.ProcessError("storage.InitStoreServer", `
		StorageSession, err = session.NewSession(&aws.Config{
			Credentials:      credentials.NewStaticCredentials(storageConf.AccessKey, storageConf.SecretKey, ""),
			Endpoint:         aws.String(storageConf.Address),
			Region:           aws.String(storageConf.Region),
			S3ForcePathStyle: aws.Bool(true),
		})
		`, err, "pkg/storage/storage.go")
	} else {
		fmt.Println("...成功")
	}
}
func UploadFile(filePath string) string {
	file, err := os.ReadFile(filePath)
	var fileStoreServerPath string = ""
	if err == nil {
		storageConf := conf.WebConfiguration.StorageServerConf
		objectName := storageConf.ObjectName + "/" + uuid.NewString() + filepath.Ext(filePath)
		svc := s3.New(StorageSession)
		_, err = svc.PutObject(&s3.PutObjectInput{
			Bucket: aws.String(storageConf.BucketName),
			Key:    aws.String(objectName),
			Body:   bytes.NewReader(file),
		})
		if err == nil {
			fileStoreServerPath = fmt.Sprintf("%s/%s/%s", storageConf.Address, storageConf.BucketName, objectName)
		} else {
			tools.ProcessError("storage.UploadFile", `file, err := os.ReadFile(filePath)`, err, "pkg/storage/storage.go")
		}
	} else {
		tools.ProcessError("storage.UploadFile", `_, err = svc.PutObject(&s3.PutObjectInput{
			Bucket: aws.String(storageConf.BucketName),
			Key:    aws.String(objectName),
			Body:   bytes.NewReader(file),
		})`, err, "pkg/storage/storage.go")
	}
	return fileStoreServerPath
}

func WSUploadFile(ctx iris.Context) {
	message := web.WebMessage{StatusCode: 200, Message: "OK"}
	// 设置 MinIO 服务的访问和密钥
	storageConf := conf.WebConfiguration.StorageServerConf
	// 设置 MinIO 服务的信息
	file, info, err := ctx.FormFile("file")
	objectName := storageConf.ObjectName + "/" + uuid.NewString() + filepath.Ext(info.Filename)
	if err == nil {
		var data []byte
		data, err = io.ReadAll(file)
		if err == nil {
			// 创建一个 S3 服务实例
			svc := s3.New(StorageSession)
			// 上传文件至 MinIO
			_, err = svc.PutObject(&s3.PutObjectInput{
				Bucket: aws.String(storageConf.BucketName),
				Key:    aws.String(objectName),
				Body:   bytes.NewReader(data),
			})
			if err == nil {

				message.Message = fmt.Sprintf("%s/%s/%s", storageConf.Address, storageConf.BucketName, objectName)
			} else {
				tools.ProcessError("storage.WSUploadFile", `_, err = svc.PutObject(&s3.PutObjectInput{
					Bucket: aws.String(storageConf.BucketName),
					Key:    aws.String(objectName),
					Body:   bytes.NewReader(data),
				})`, err, "pkg/storage/storage.go")
			}

		} else {
			tools.ProcessError("storage.WSUploadFile", `data, err = io.ReadAll(file)`, err, "pkg/storage/storage.go")
		}
	}
	defer file.Close()
	if err != nil {
		message.Message = err
		message.StatusCode = 500
	}
	ctx.JSON(message)
}

func WSDownloadFile(ctx iris.Context) {
	fileName := ctx.FormValue("filename")
	storageConf := conf.WebConfiguration.StorageServerConf
	objectName := storageConf.ObjectName + "/" + fileName
	var resp *s3.GetObjectOutput
	var contentType string
	var err error

	svc := s3.New(StorageSession)
	resp, err = svc.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(storageConf.BucketName),
		Key:    aws.String(objectName),
	})
	if err == nil {
		if resp.ContentType != nil {
			contentType = *resp.ContentType
		}
		ctx.ContentType(contentType)
		ctx.Header("Content-Disposition", "attachment; filename="+fileName)
		io.Copy(ctx.ResponseWriter(), resp.Body)
		resp.Body.Close()
	} else {
		tools.ProcessError("storage.WSDowloadFile", `	resp, err = svc.GetObject(&s3.GetObjectInput{
			Bucket: aws.String(storageConf.BucketName),
			Key:    aws.String(objectName),
		})`, err, "pkg/storage/storage.go")
		var message web.WebMessage
		message.StatusCode = 500
		message.Message = err

	}

}

/*

package main

import (
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func main() {
	// 初始化AWS会话
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String("us-west-2"),
		Credentials: credentials.NewStaticCredentials("YOUR-ACCESSKEY", "YOUR-SECRETKEY", ""),
	})
	if err != nil {
		log.Fatal(err)
	}

	// 创建一个S3上传管理器，并设置PartSize和Concurrency参数
	uploader := s3manager.NewUploader(sess, func(u *s3manager.Uploader) {
		u.PartSize = 10 * 1024 * 1024 // 每个部分的大小为10MB
		u.Concurrency = 5            // 同时最多上传5个部分
	})

	// 打开要上传的文件
	file, err := os.Open("your-large-file")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// 使用分块上传上传文件
	_, err = uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String("your-bucket"),
		Key:    aws.String("your-object"),
		Body:   file,
	})
	if err != nil {
		log.Fatal("Failed to upload file:", err)
	}

	fmt.Printf("Successfully uploaded %s\n", "your-large-file")
}

*/
