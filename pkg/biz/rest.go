package biz

import (
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"math"
	"time"

	"github.com/google/uuid"
)

func CreateRest(rest entities.Restaurant) (string, error) {
	rest.CreateDatetime = time.Now()
	if rest.Creator == "" {
		rest.Creator = "amdin"
	}
	rest.Id = uuid.NewString()
	rest.Modifier = rest.Creator
	rest.ModifyDatetime = rest.CreateDatetime

	err := dao.CreateRest(rest)
	return rest.Id, err
}

func GetRestById(id string) (entities.Restaurant, error) {
	return dao.GetRestById(id)
}

func DeleteRestById(id string) (bool, error) {

	return dao.DeleteRestById(id)
}

func QueryRests(queryCondition entities.QueryCondition) ([]entities.Restaurant, int64, int64, error) {
	var rests []entities.Restaurant
	dataRecordCount, err := dao.GetRestsCount(queryCondition)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		rests, err = dao.QueryRests(queryCondition)
	}
	return rests, int64(math.Ceil(pageCount)), dataRecordCount, err
}

func UpdateRest(role *entities.Restaurant) error {
	var tmpRole entities.Restaurant
	var err error
	tmpRole, err = dao.GetRestById(role.Id)
	if err == nil {
		// 防止以下字段被修改
		role.CreateDatetime = tmpRole.CreateDatetime
		role.Creator = tmpRole.Creator
		role.ModifyDatetime = time.Now()

		if role.Modifier == "" {
			role.Modifier = "admin"
		}
		err = dao.UpdateRest(role)
	}
	return err
}
