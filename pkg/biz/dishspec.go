package biz

import (
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"math"
	"time"

	"github.com/google/uuid"
)

func CreateDishSpec(dishspec *entities.DishSpec) error {
	dishspec.CreateDatetime = time.Now()
	if dishspec.Creator == "" {
		dishspec.Creator = "amdin"
	}
	dishspec.Id = uuid.NewString()
	dishspec.Modifier = dishspec.Creator
	dishspec.ModifyDatetime = dishspec.CreateDatetime

	return dao.Database.Table("dish_specs").Create(&dishspec).Error
}

func GetDishSpecById(id string) (entities.DishSpec, error) {
	var dishspec entities.DishSpec
	err := dao.Database.Where("id = ?", id).First(&dishspec).Error
	return dishspec, err
}

func DeleteDishSpecById(id string) (bool, error) {
	var dish entities.DishSpec
	dish.Id = id
	return dao.DeleteDishSpecById(dish.Id)
}

func QueryDishesSpec(queryCondition entities.QueryCondition, dish_id string) ([]entities.DishSpec, int64, int64, error) {
	var dishes []entities.DishSpec
	dataRecordCount, err := dao.GetDishesSpecCount(queryCondition, dish_id)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		dishes, err = dao.QueryDishesSpec(queryCondition, dish_id)
	}
	return dishes, int64(math.Ceil(pageCount)), dataRecordCount, err
}

func UpdateDishSpec(role *entities.DishSpec) error {
	var tmpRole entities.DishSpec
	var err error
	tmpRole, err = GetDishSpecById(role.Id)
	if err == nil {
		// 防止以下字段被修改
		role.CreateDatetime = tmpRole.CreateDatetime
		role.Creator = tmpRole.Creator
		role.ModifyDatetime = time.Now()

		if role.Modifier == "" {
			role.Modifier = "admin"
		}
		err = dao.UpdateDishSpec(role)
	}
	return err
}
