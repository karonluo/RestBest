package biz

import (
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"math"
	"time"

	"github.com/google/uuid"
)

func CreateOrder(order *entities.Order) error {
	order.CreateDatetime = time.Now()
	if order.Creator == "" {
		order.Creator = "amdin"
	}
	order.Id = uuid.NewString()
	order.Modifier = order.Creator
	order.ModifyDatetime = order.CreateDatetime

	return dao.Database.Create(&order).Error
}

func GetOrderById(id string) (entities.Order, error) {
	var order entities.Order
	err := dao.Database.Where("id = ?", id).First(&order).Error
	return order, err
}

func DeleteOrderById(id string) (bool, error) {
	var order entities.Order
	order.Id = id
	return dao.DeleteOrderById(order.Id)
}

func QueryOrders(queryCondition entities.QueryCondition, table_id string, rest_id string) ([]entities.Order, int64, int64, error) {
	var orders []entities.Order
	dataRecordCount, err := dao.GetOrdersCount(queryCondition, table_id, rest_id)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		orders, err = dao.QueryOrders(queryCondition, table_id, rest_id)
	}
	return orders, int64(math.Ceil(pageCount)), dataRecordCount, err
}

func UpdateOrder(order *entities.Order) error {
	var tmpRole entities.Order
	var err error
	tmpRole, err = GetOrderById(order.Id)
	if err == nil {
		// 防止以下字段被修改
		order.CreateDatetime = tmpRole.CreateDatetime
		order.Creator = tmpRole.Creator
		order.ModifyDatetime = time.Now()

		if order.Modifier == "" {
			order.Modifier = "admin"
		}
		err = dao.UpdateOrder(order)
	}
	return err
}

func ClearTablesOrders(order_id string) error {
	return dao.ClearTablesOrders(order_id)
}

// 设置订单与餐桌关联
func OrderJoinInTables(order_id string, table_ids []string) error {
	return dao.OrderJoinInTables(order_id, table_ids)
}
