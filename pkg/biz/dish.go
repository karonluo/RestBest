package biz

import (
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"math"
	"time"

	"github.com/google/uuid"
)

func CreateDish(dish *entities.Dish) error {
	dish.CreateDatetime = time.Now()
	if dish.Creator == "" {
		dish.Creator = "admin"
	}
	dish.Id = uuid.NewString()
	dish.Modifier = dish.Creator
	dish.ModifyDatetime = dish.CreateDatetime

	return dao.Database.Create(&dish).Error
}

func GetDishById(id string) (entities.Dish, error) {
	var dish entities.Dish
	err := dao.Database.Where("id = ?", id).First(&dish).Error
	return dish, err
}

func DeleteDishById(id string) (bool, error) {
	var dish entities.Dish
	dish.Id = id
	return dao.DeleteDishById(dish.Id)
}

func QueryDishes(queryCondition entities.QueryCondition, rest_id string) ([]entities.Dish, int64, int64, error) {
	var dishes []entities.Dish
	dataRecordCount, err := dao.GetDishesCount(queryCondition, rest_id)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		dishes, err = dao.QueryDishes(queryCondition, rest_id)
	}
	return dishes, int64(math.Ceil(pageCount)), dataRecordCount, err
}

func UpdateDish(role *entities.Dish) error {
	var tmpRole entities.Dish
	var err error
	tmpRole, err = GetDishById(role.Id)
	if err == nil {
		// 防止以下字段被修改
		role.CreateDatetime = tmpRole.CreateDatetime
		role.Creator = tmpRole.Creator
		role.ModifyDatetime = time.Now()

		if role.Modifier == "" {
			role.Modifier = "admin"
		}
		err = dao.UpdateDish(role)
	}
	return err
}

func ClearDishToppings(dish_id string) error {
	return dao.ClearDishToppings(dish_id)
}

// 设置菜品与配菜关联
func DishJoinInToppings(dish_id string, topping_ids []string) error {
	return dao.DishJoinInToppings(dish_id, topping_ids)
}
