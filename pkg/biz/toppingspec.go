package biz

import (
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"errors"
	"math"
	"time"

	"github.com/google/uuid"
)

func DeleteToppingSpecById(id string) (bool, error) {
	return dao.DeleteToppingSpecById(id)
}

func CreateToppingSpec(toppingSpec entities.ToppingSpec) (string, error) {
	var err error
	if toppingSpec.Id == "" {
		toppingSpec.Id = uuid.New().String()
	}
	toppingSpec.CreateDatetime = time.Now()
	toppingSpec.ModifyDatetime = time.Now()
	if toppingSpec.Creator != "" {
		toppingSpec.Creator = "admin"
	}
	toppingSpec.Modifier = toppingSpec.Creator

	dao.CreateToppingSpec(toppingSpec)

	return toppingSpec.Id, err
}

func QueryToppingSpeces(queryCondition entities.QueryCondition, rest_id string) ([]entities.ToppingSpec, int64, int64, error) {
	var users []entities.ToppingSpec
	dataRecordCount, err := dao.GetToppingSpecCount(queryCondition, rest_id)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		users, err = dao.QueryToppingSpeces(queryCondition, rest_id)
	}
	return users, int64(math.Ceil(pageCount)), dataRecordCount, err
}

func UpdateToppingSpec(user entities.ToppingSpec) error {
	var topping entities.ToppingSpec
	var err error
	var result bool

	topping, result, err = dao.GetToppingSpecById(user.Id)

	if !result {
		err = errors.New("更新的Table ID不存在。")
	}

	if err == nil {
		// 防止以下字段被修改
		user.CreateDatetime = topping.CreateDatetime
		user.Creator = topping.Creator
		user.ModifyDatetime = time.Now()

		if user.Modifier == "" {
			user.Modifier = "admin"
		}
		err = dao.UpdateToppingSpec(&user)

	}

	return err
}

func GetToppingSpecById(id string) (entities.ToppingSpec, bool, error) {
	result, ss, error := dao.GetToppingSpecById(id)
	return result, ss, error
}
