package biz

import (
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"errors"
	"math"
	"time"

	"github.com/google/uuid"
)

func DeleteTableById(id string) (bool, error) {
	return dao.DeleteTableById(id)
}

func CreateTable(user entities.Table) (string, error) {
	var err error
	if user.Id == "" {
		user.Id = uuid.New().String()
	}
	user.CreateDatetime = time.Now()
	user.ModifyDatetime = time.Now()
	if user.Creator != "" {
		user.Creator = "admin"
	}
	user.Modifier = user.Creator

	dao.CreateTable(user)

	return user.Id, err
}

func QueryTables(queryCondition entities.QueryCondition, rest_id string) ([]entities.Table, int64, int64, error) {
	var users []entities.Table
	dataRecordCount, err := dao.GetTableCount(queryCondition, rest_id)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		users, err = dao.QueryTables(queryCondition, rest_id)
	}
	return users, int64(math.Ceil(pageCount)), dataRecordCount, err
}

func UpdateTable(user entities.Table) error {
	var tmpUser entities.Table
	var err error
	var result bool

	tmpUser, result, err = dao.GetTableById(user.Id)

	if result == false {
		err = errors.New("更新的Table ID不存在。")
	}

	if err == nil {
		// 防止以下字段被修改
		user.CreateDatetime = tmpUser.CreateDatetime
		user.Creator = tmpUser.Creator
		user.ModifyDatetime = time.Now()

		if user.Modifier == "" {
			user.Modifier = "admin"
		}
		err = dao.UpdateTable(user)

	}

	return err
}

func GetTableById(login_name string) (entities.Table, bool, error) {
	result, ss, error := dao.GetTableById(login_name)
	return result, ss, error
}
