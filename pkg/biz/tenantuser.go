package biz

import (
	"RestBest/conf"
	"RestBest/pkg/cache"
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"encoding/json"

	"RestBest/pkg/message"
	"RestBest/pkg/tools"
	"context"
	"errors"
	"fmt"
	"math"
	"strings"
	"time"

	"github.com/google/uuid"
)

func GetUserFromRedisByLoginName(login_name string) (entities.SysUser, bool) {
	var sysuser entities.SysUser
	var result bool = true
	ctx := context.Background()
	res := cache.RedisDatabase.Get(ctx, "restbest_sysuser_"+login_name)
	if res != nil {
		err := json.Unmarshal([]byte(res.Val()), &sysuser)
		if err != nil {
			result = false

		}
	} else {
		result = false
	}
	return sysuser, result
}

func CreateTenantUser(user entities.TenantUser) (string, error) {
	var err error
	if user.Id == "" {
		user.Id = uuid.New().String()
	}
	user.CreateDatetime = time.Now()
	user.ModifyDatetime = time.Now()
	if user.Creator != "" {
		user.Creator = "admin"
	}
	user.Modifier = user.Creator
	user.PasswdMD5 = tools.SHA1(user.PasswdMD5)
	if dao.CheckHaveReTenantUser(user) {
		err = errors.New("有相同的系统用户信息, 请注意: 电子邮箱地址、手机号、登录名、身份证号 不能重复且必填。")
		user.Id = ""
	} else {
		dao.CreateTenantUser(user)
	}
	return user.Id, err
}

func GetTenantUserByLoginName(login_name string) entities.TenantUser {
	result, _, _ := dao.GetTenantUserByLoginName(login_name)
	return result
}

func CheckHaveReTenantUserByLoginName(login_name string) bool {
	var user entities.TenantUser
	user.LoginName = login_name
	return dao.CheckHaveReTenantUser(user)
}

func CheckHaveReTenantUserByEmail(email string) bool {
	var user entities.TenantUser
	user.Email = email
	return dao.CheckHaveReTenantUser(user)
}

func CheckHaveReTenantUserByCellphone(cellphone string) bool {
	var user entities.TenantUser
	user.Cellphone = cellphone
	return dao.CheckHaveReTenantUser(user)
}

func DeleteTenantUser(id string) (bool, error) {
	var user entities.TenantUser
	user.Id = id
	return dao.DeleteTenantUser(user)
}

func DeleteTenantUsers(ids []string) error {
	return dao.DeleteTenantUsers(ids)
}
func QueryTenantUsers(queryCondition entities.QueryCondition) ([]entities.TenantUser, int64, int64, error) {
	var users []entities.TenantUser
	dataRecordCount, err := dao.GetTenantUserCount(queryCondition)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		users, err = dao.QueryTenantUsers(queryCondition)
	}
	return users, int64(math.Ceil(pageCount)), dataRecordCount, err
}

func UpdateTenantUserPassword(sysUserId string, originPassword string, newPassword string) error {
	var tmpUser entities.TenantUser
	var err error
	var result bool
	tmpUser, result, err = dao.GetTenantUserById(sysUserId)

	if result == false {
		err = errors.New("更新的TenantUserID不存在。")
	}
	if err == nil {
		if tmpUser.PasswdMD5 == tools.SHA1(originPassword) {
			err = dao.UpdateTenantUserPassword(sysUserId, tools.SHA1(newPassword))
		}
	}
	return err
}

func UpdateTenantUser(user entities.TenantUser) error {
	var tmpUser entities.TenantUser
	var err error
	var result bool

	tmpUser, result, err = dao.GetTenantUserById(user.Id)

	if result == false {
		err = errors.New("更新的TenantUserID不存在。")
	}

	if err == nil {
		// 防止以下字段被修改
		user.CreateDatetime = tmpUser.CreateDatetime
		user.Creator = tmpUser.Creator
		user.ModifyDatetime = time.Now()
		user.PasswdMD5 = tmpUser.PasswdMD5
		if user.Modifier == "" {
			user.Modifier = "admin"
		}
		err = dao.UpdateTenantUser(user)
		// 更新所有冗余字段，需要事务
		// if user.DisplayName != tmpUser.DisplayName {
		// 	//TODO: 更新所有关联表
		// 	// 1. 场地用户信息
		// 	err = dao.UpdateSiteUserUserDisplayName(user.Id, user.DisplayName)
		// }
		//cache.SetUserToRedis(user, 0) // 更新缓存

	}

	return err
}

func ResetTenantUserPassword(password string, token string) error {
	var err error
	fmt.Println(password, token)
	//TODO: ResetSysUserPassword
	return err
}

func SendTenantUserForgetPasswordEmail(email string) error {
	emailConf := conf.WebConfiguration.EmailSmtpServerConf
	var err error
	if CheckHaveReSysUserByEmail(email) {
		// 当检测到有该邮件地址时发送重置密码的邮件。
		token := tools.SHA1(uuid.New().String())
		rctx := context.Background()
		//将 token 存入 redis 用于重置密码的key中, 并根据配置进行超时设置。
		val := fmt.Sprintf(`{"email":"%s", "token":"%s"}`, email, token)
		cache.RedisDatabase.Set(rctx, "resetpwd_"+token, val, time.Minute*time.Duration(emailConf.ResetSysUserPasswordEmailTimeout))
		html := strings.ReplaceAll(emailConf.ResetSysUserPasswordEmailTemplate, "{{token}}", token)
		html = strings.ReplaceAll(html, "{{timeout}}", fmt.Sprintf("%d", emailConf.ResetSysUserPasswordEmailTimeout))
		err = message.SendEmail([]string{email}, html, "密码重置邮件")
	}
	return err
	// "ResetSysUserPasswordEmailTemplate1": "Click <a href='http://172.0.0.1/resetpwd?token={{token}}' target='_blank'> to reset password. </a>or enter reset password page and input code: {{token}} <p>, The code timeout is  {{timeout}} minute. <br> thanks!",
}

func GetTenantUserByLoginNameRedis(loginName string) (entities.TenantUser, bool) {

	sysuser, res := dao.GetTenantUserFromRedisByLoginName(loginName)
	if !res {
		// 尝试从数据库中获取
		sysuser, res, _ = dao.GetTenantUserByLoginName(loginName)
		if res {
			// 设置到缓存中

			cache.SetTenantUserToRedis(sysuser, 0)
		} else {

			// 当没有从数据库中找到用户时为防止缓存穿透对数据库进行冲击，放置一个空用户信息
			sysuser.LoginName = loginName
			sysuser.IsDisableLogin = true
			sysuser.DisplayName = "anonymous"
			cache.SetTenantUserToRedis(sysuser, 1) // 一分钟之内查询得到空用户。
		}

	} else if sysuser.DisplayName == "anonymous" {
		// 匿名用户，代表该用户其实不存在，通过系统虚拟出来的。
		res = false

	}
	return sysuser, res
}

func GetTenantUserById(Id string) (entities.TenantUser, bool, error) {

	// 尝试从数据库中获取
	sysuser, res, err := dao.GetTenantUserById(Id)

	return sysuser, res, err
}

func LoginSystemForTenantUser(loginName string, password string, code string) (string, bool, string, string) {
	var success bool = true
	var msg string

	fmt.Println("LoginSystemForTenantUser start ")
	sysuser, res := GetSysUserByLoginName(loginName)

	fmt.Println("GetSysUserByLoginName end ")
	fmt.Println(res)
	if res {
		fmt.Println(sysuser.PasswdMD5)
		fmt.Println("LoginSystemForTenantUser sysuser.PasswdMD5 " + tools.SHA1(password))
		if sysuser.PasswdMD5 == tools.SHA1(password) { // 比对密码成功
			fmt.Println("LoginSystemForTenantUser sysuser.PasswdMD5 " + sysuser.PasswdMD5)
			fmt.Println("LoginSystemForTenantUser sysuser.loginName " + sysuser.LoginName)
			fmt.Println(sysuser.RestCodes)
			// 比对 商家简称是否在该用户名下
			if !strings.Contains(sysuser.RestNames, code) {
				// 未找到该用户
				success = false
				msg = "用户名下未找到该商家。"
				return msg, success, "", ""
			}

			rctx := context.Background()
			msg = tools.SHA1(uuid.New().String())
			var token_val map[string]interface{} = make(map[string]interface{})
			token_val["token"] = msg
			token_val["login_name"] = loginName
			token_val["display_name"] = sysuser.DisplayName
			bytes, _ := json.Marshal(sysuser)
			token_val["sysuser"] = string(bytes)
			token_val["code"] = code
			token_val["Restaurants"] = sysuser.Restaurants

			err1 := cache.RedisDatabase.HSet(rctx, "restbest_token_"+msg, token_val).Err()
			if err1 != nil {
				tools.ProcessError(`biz.LoginSystemForTenantUser`, `dao.RedisDatabase.HSet(rctx, "restbest_token_"+msg, token_val)`, err1, "pkg/biz/sysuser.go")
				msg = err1.Error()
				success = false
			}
			// TODO: 放置 ACL (该用户可访问的URL和功能模块)
			acl := GetUserACLByAuthorization()
			res, _ := json.Marshal(acl)
			err2 := cache.RedisDatabase.HSet(rctx, "restbest_token_"+msg, "acl", res).Err()
			if err2 != nil {
				tools.ProcessError(`biz.LoginSystemForTenantUser`, `dao.RedisDatabase.HSet(rctx, "restbest_token_"+msg, "acl", res)`, err2, "pkg/biz/sysuser.go")
				msg = err2.Error()
				success = false
			}
			cache.RedisDatabase.Expire(rctx, "restbest_token_"+msg, time.Duration(conf.WebConfiguration.SessionExpireMinute)*time.Minute) // 设置20分钟后过期
		} else {
			// 密码不正确
			success = false
			msg = "未找到该用户或密码错误。"
		}
	} else {
		// 未找到该用户
		success = false
		msg = "未找到该用户或密码错误。"
	}
	return msg, success, sysuser.Id, sysuser.Restaurants
}

// func ClearSysUserSysRoles(sysRoleId string) error {
// 	return dao.ClearSysUserSysRoles(sysRoleId)
// }

// // 将系统角色和系统用户进行绑定
// func SysUserJoinInSysRoles(user_id string, role_ids []string) error {
// 	return dao.SysUserJoinInSysRoles(user_id, role_ids)
// }
