package biz

import (
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"fmt"
	"math"
	"time"

	"github.com/google/uuid"
)

func CreateSysMenu(sysMenu entities.SysMenu) (string, error) {
	var err error
	if sysMenu.Id == "" {
		sysMenu.Id = uuid.New().String()
	}
	sysMenu.CreateDatetime = time.Now()
	sysMenu.ModifyDatetime = time.Now()
	if sysMenu.Creator != "" {
		sysMenu.Creator = "admin"
	}
	sysMenu.Modifier = sysMenu.Creator

	// 计算 haschild字段值
	// 判断本条记录是否有child
	pageCount, err := dao.GetChildItem(sysMenu.Id)

	sysMenu.Haschild = false
	if pageCount > 0 {
		sysMenu.Haschild = true
	}

	err = dao.CreateSysMenu(sysMenu)
	// 判断本条记录是否是 其他记录的child
	if err == nil {
		var parentsysMenu entities.SysMenu
		parentsysMenu, err = dao.GetSysMenuById(sysMenu.ParentID)

		fmt.Println("============2222222===========")
		fmt.Println("parentsysMenu.Id" + parentsysMenu.Id + "sss")
		fmt.Println(parentsysMenu.Haschild)
		if parentsysMenu.Id != "" {

			fmt.Println("parentsysMenu.Id !=" + parentsysMenu.Id + "sss")
			parentsysMenu.Haschild = true
			UpdateSysMenu(&parentsysMenu)
		} else {

			err = nil
		}
	}

	// fmt.Println("=======================")
	// fmt.Println(err)

	return sysMenu.Id, err
}

func GetSysMenuById(Id string) (entities.SysMenu, error) {

	return dao.GetSysMenuById(Id)
}

func GetSysMenuByParentId(Id string) ([]entities.SysMenu, error) {

	return dao.GetSysMenuByParentId(Id)
}

func QuerySysMenus(queryCondition entities.QueryCondition, parent_id string) ([]entities.SysMenu, int64, int64, error) {
	var sysMenus []entities.SysMenu
	dataRecordCount, err := dao.GetSysMenuCount(queryCondition, parent_id)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		sysMenus, err = dao.QuerySysMenus(queryCondition, parent_id)
	}
	return sysMenus, int64(math.Ceil(pageCount)), dataRecordCount, err
}

func QueryAllSysMenus() ([]entities.SysMenu, error) {
	var sysMenus []entities.SysMenu

	sysMenus, err := dao.QueryAllSysMenus()

	return sysMenus, err
}

func DeleteSysMenu(id string) (bool, error) {
	var sysMenu entities.SysMenu
	sysMenu.Id = id
	var err error
	var result bool
	println(id)
	sysMenu, err = dao.GetSysMenuById(id)
	println("GetSysMenuById")
	result, err = dao.DeleteSysMenu(sysMenu)
	println("DeleteSysMenu")
	println(err)
	// 在某个节点新增下级节点后再删除所有的下级节点   该根节点的HasChild还是true
	if err == nil {
		// 删除成功则 查询 和本条 菜单记录是否有相同的 ParentID
		// 如果有 则不做处理 ,如果无则 更新 id = ParentID 的那条记录 haschild字段
		sysMenus, err2 := dao.GetSysMenuByParentId(sysMenu.ParentID)
		println("err != nil")
		println(sysMenus)
		if sysMenus == nil || len(sysMenus) == 0 {

			println("UpdateSysMenuHasChild")
			dao.UpdateSysMenuHasChild(sysMenu.ParentID, false)
		}

		if err2 != nil {
			err = err2
		}
	}
	return result, err
}

func UpdateSysMenu(sysMenu *entities.SysMenu) error {

	var tmpSysMenu entities.SysMenu
	var err error
	tmpSysMenu, err = dao.GetSysMenuById(sysMenu.Id)

	if err == nil {
		// 防止以下字段被修改
		sysMenu.CreateDatetime = tmpSysMenu.CreateDatetime
		sysMenu.Creator = tmpSysMenu.Creator
		sysMenu.ModifyDatetime = time.Now()

		if sysMenu.Modifier == "" {
			sysMenu.Modifier = "admin"
		}

		// 计算 haschild字段值
		var pageCount int64
		pageCount, err = dao.GetChildItem(sysMenu.Id)
		sysMenu.Haschild = false
		if pageCount >= 0 {
			sysMenu.Haschild = true
		}

		err = dao.UpdateSysMenus(sysMenu)

	}

	return err
}
