package biz

import (
	"RestBest/conf"
	"RestBest/pkg/cache"
	"context"
	"fmt"
	"time"
)

func CheckLogin(authorization string) (bool, error) {
	result := true

	// TODO: 用于测试代码，允许 authorization 为 test 的时候通过。
	if authorization == "test" {
		return true, nil
	}
	rctx := context.Background()
	res := cache.RedisDatabase.HExists(rctx, "restbest_token_"+authorization, "token")
	fmt.Println("restbest_token_" + authorization)
	err := res.Err()
	if err != nil {
		result = false

	} else {
		result = res.Val()
		cache.RedisDatabase.Expire(rctx, "restbest_token_"+authorization, time.Duration(conf.WebConfiguration.SessionExpireMinute)*time.Minute) // 重置超时时间
	}
	return result, err
}

func GetLoginInformation(authorization string, fieldName string) (string, error) {
	rctx := context.Background()
	result := cache.RedisDatabase.HGet(rctx, "restbest_token_"+authorization, fieldName)
	err := result.Err()
	msg := result.Val()

	return msg, err
}
