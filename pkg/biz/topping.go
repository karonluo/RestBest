package biz

import (
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"errors"
	"fmt"
	"math"
	"time"

	"github.com/google/uuid"
)

func DeleteToppingById(id string) (bool, error) {
	return dao.DeleteToppingById(id)
}

func CreateTopping(topping entities.Topping, dishes_id string) (string, error) {
	var err error
	var idssssss string

	if topping.Id == "" {
		topping.Id = uuid.New().String()
	}
	topping.CreateDatetime = time.Now()
	topping.ModifyDatetime = time.Now()
	if topping.Creator != "" {
		topping.Creator = "admin"
	}
	topping.Modifier = topping.Creator

	idssssss, err = dao.CreateTopping(topping)

	var toppingIds []string
	toppingIds = append(toppingIds, topping.Id)

	//创建成功后，绑定菜品 和 配菜
	fmt.Println("DishId" + dishes_id)
	if err == nil && dishes_id != "" {
		err = DishJoinInToppings(dishes_id, toppingIds)
	}
	return idssssss, err
}

func QueryToppings(queryCondition entities.QueryCondition, dishes_id string) ([]entities.Topping, int64, int64, error) {
	var users []entities.Topping
	dataRecordCount, err := dao.GetToppingsCount(queryCondition, dishes_id)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		users, err = dao.QueryToppings(queryCondition, dishes_id)
	}
	return users, int64(math.Ceil(pageCount)), dataRecordCount, err
}

func UpdateTopping(user entities.Topping) error {
	var topping entities.Topping
	var err error
	var result bool

	topping, result, err = dao.GetToppingById(user.Id)

	if !result {
		err = errors.New("更新的Table ID不存在。")
	}

	if err == nil {
		// 防止以下字段被修改
		user.CreateDatetime = topping.CreateDatetime
		user.Creator = topping.Creator
		user.ModifyDatetime = time.Now()

		if user.Modifier == "" {
			user.Modifier = "admin"
		}
		err = dao.UpdateTopping(&user)

	}

	return err
}

func GetToppingById(id string) (entities.Topping, bool, error) {
	result, ss, error := dao.GetToppingById(id)
	return result, ss, error
}
