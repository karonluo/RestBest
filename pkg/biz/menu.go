package biz

import (
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"encoding/json"
	"fmt"
	"math"
	"time"

	"github.com/google/uuid"
)

func CreateMenu(menu *entities.Menu) error {
	menu.CreateDatetime = time.Now()
	if menu.Creator == "" {
		menu.Creator = "amdin"
	}
	menu.Id = uuid.NewString()
	menu.Modifier = menu.Creator
	menu.ModifyDatetime = menu.CreateDatetime

	return dao.Database.Create(&menu).Error
}

func GetMenu(menu_id string) (entities.Menu, error) {
	var menu entities.Menu
	err := dao.Database.Where("id = ?", menu_id).First(&menu).Error
	return menu, err
}

func DeleteMenu(id string) (bool, error) {
	var menu entities.Menu
	menu.Id = id
	return dao.DeleteMenuById(menu.Id)
}

func QueryMenus(queryCondition entities.QueryCondition, restaurantID string) ([]entities.Menu, int64, int64, error) {
	var menus []entities.Menu
	dataRecordCount, err := dao.GetMenusCount(queryCondition, restaurantID)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		menus, err = dao.QueryMenus(queryCondition, restaurantID)
	}
	return menus, int64(math.Ceil(pageCount)), dataRecordCount, err
}

func UpdateMenu(menu *entities.Menu) error {
	var tmpMenu entities.Menu
	var err error
	fmt.Println(menu.Id)
	tmpMenu, err = GetMenu(menu.Id)
	if err == nil {
		// 防止以下字段被修改
		menu.CreateDatetime = tmpMenu.CreateDatetime
		menu.Creator = tmpMenu.Creator
		menu.ModifyDatetime = time.Now()

		if menu.Modifier == "" {
			menu.Modifier = "admin"
		}
		err = dao.UpdateMenu(menu)
	}
	return err
}

func ClearMenuDishes(restGroupId string) error {
	return dao.ClearMenuDishes(restGroupId)
}

// 设置菜单与菜品关联
func MenuJoinInDishes(menuId string, dishesIds []string, restId string) error {
	return dao.MenuJoinInDishes(menuId, dishesIds, restId)
}

func QueryFullMenus(restaurantID string) (entities.FullMenuList, error) {
	var menus []entities.Menu
	var queryCondition entities.QueryCondition
	queryCondition.PageSize = 9999
	queryCondition.PageIndex = 1
	var err error
	menus, err = dao.QueryMenus(queryCondition, restaurantID)

	var fullMenuList entities.FullMenuList
	fullMenuList.MenuList = nil

	fmt.Println("QueryFullMenus start restaurantID:" + restaurantID)

	// 获取店家下素有菜品，通过

	for _, menu := range menus {
		var fullMenu entities.FullMenu

		// 菜单下获取 菜品
		var dishes []entities.Dish
		dishes, err = dao.QueryFullDishes(restaurantID, menu.Id)

		// 循环赋值
		for _, dish := range dishes {
			var fd entities.FullDish
			fd.Dish = dish
			// 菜品下获取  菜品 spec start ------------------------------------
			var dishSpeces []entities.DishSpec
			dishSpeces, err = dao.QueryDishesSpec(queryCondition, dish.Id)

			for _, dishspec := range dishSpeces {
				fd.DishSpeces = append(fd.DishSpeces, dishspec)
			}
			// 菜品下获取  菜品 spec  end ------------------------------------

			// 每个菜品 获取其下配菜start ------------------------------------
			var toppings []entities.Topping
			toppings, err = dao.QueryToppings(queryCondition, dish.Id)
			for _, topping := range toppings {
				var ft entities.FullTopping
				ft.Topping = topping
				// 配菜获取 其下配菜 spec
				var toppingSpeces []entities.ToppingSpec

				fmt.Println("topping.Id" + topping.Id)
				// fmt.Println(dishesCount)
				toppingSpeces, err = dao.QueryToppingSpeces(queryCondition, topping.Id)
				ft.ToppingSpeces = toppingSpeces

				fd.Toppings = append(fd.Toppings, ft)

			}

			fullMenu.DishList = append(fullMenu.DishList, fd)

			//var dishesCount = len(fullMenu.DishList)
			// fmt.Println("每个菜品 获取其下 菜品 数量")
			// fmt.Println(dishesCount)
		}

		fullMenu.Menu = menu
		fullMenuList.MenuList = append(fullMenuList.MenuList, fullMenu)

	}

	//fmt.Println("QueryFullMenus end fullMenuList:")
	v, err := json.Marshal(fullMenuList)
	fmt.Println(err)
	fmt.Println(string(v))
	return fullMenuList, err
}

func QueryFullMenusback(restaurantID string) (entities.FullMenuList, error) {
	var menus []entities.Menu
	var queryCondition entities.QueryCondition
	queryCondition.PageSize = 9999
	queryCondition.PageIndex = 1
	var err error
	menus, err = dao.QueryMenus(queryCondition, restaurantID)

	var fullMenuList entities.FullMenuList
	fullMenuList.MenuList = nil

	fmt.Println("QueryFullMenus start restaurantID:" + restaurantID)

	for _, menu := range menus {
		var fullMenu entities.FullMenu
		fullMenu.Menu = menu
		fullMenuList.MenuList = append(fullMenuList.MenuList, fullMenu)

	}

	for _, menu := range fullMenuList.MenuList {
		// fmt.Println(sysuser)
		// data := tools.ReflectMethod(sysuser)
		//cache.SetUserToRedis(menu, 0)

		// 菜单下获取 菜品
		var dishes []entities.Dish
		dishes, err = dao.QueryDishes(queryCondition, restaurantID)

		// 循环赋值
		for _, dish := range dishes {
			var fd entities.FullDish
			fd.Dish = dish

			// 每个菜品 获取其下 菜品spec
			fmt.Println("每个菜品 获取其下 菜品ID")
			fmt.Println(dish.Id)

			menu.DishList = append(menu.DishList, fd)

			var dishesCount = len(menu.DishList)
			fmt.Println("每个菜品 获取其下 菜品 数量")
			fmt.Println(dishesCount)

			// dishSpeces, err := dao.QueryDishesSpec(queryCondition, dish.Id)
			// if err != nil {
			// 	continue
			// }

			//var sss = len(dishSpeces)
			//fmt.Println("每个菜品 获取其下 菜品spec 数量")
			//fmt.Println(sss)

			// for _, dishSpec := range dishSpeces {
			// 	//fmt.Println("每个菜品 获取其下 菜品spec")
			// 	//fmt.Println(dishSpec.Name)

			// 	fd.DishSpeces = append(fd.DishSpeces, dishSpec)
			// }

		}

		// // 每个菜品 获取其下 菜品spec
		// for _, dish := range menu.Dishes {
		// 	// dishSpeces, err := dao.QueryDishesSpec(queryCondition, dish.Dish.Id)

		// 	// dish.DishSpeces = dishSpeces

		// 	// if err != nil {
		// 	// 	continue
		// 	// }

		// 	// 菜品spec 赋值

		// 	// 	// 每个菜品 获取其下配菜
		// 	// 	var toppings []entities.Topping
		// 	// 	toppings, err = dao.QueryToppings(queryCondition, restaurantID)

		// 	// 	// 配菜获取 其下配菜 spec
		// 	// 	var toppingSpeces []entities.ToppingSpec
		// 	// 	for _, topping := range toppings {

		// 	// 		toppingSpeces, err = dao.QueryToppingSpeces(queryCondition, topping.Id)
		// 	// 		topping.ToppingSpecList = toppingSpeces
		// 	// 	}
		// }

		//menu.DishList = dishes

	}
	//fmt.Println("QueryFullMenus end fullMenuList:")
	v, err := json.Marshal(fullMenuList)
	fmt.Println(err)
	fmt.Println(string(v))
	return fullMenuList, err
}
