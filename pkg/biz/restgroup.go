package biz

import (
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"fmt"
	"math"
	"time"

	"github.com/google/uuid"
)

func CreateRestGroup(restGroup entities.RestGroup) (string, error) {
	restGroup.CreateDatetime = time.Now()
	if restGroup.Creator == "" {
		restGroup.Creator = "amdin"
	}
	restGroup.Id = uuid.NewString()
	restGroup.Modifier = restGroup.Creator
	restGroup.ModifyDatetime = restGroup.CreateDatetime

	// 计算 haschild字段值
	// 判断本条记录是否有child
	pageCount, err := dao.GetChildRestGroup(restGroup.Id)

	restGroup.Haschild = false
	if pageCount > 0 {
		restGroup.Haschild = true
	}

	err = dao.CreateRestGroup(restGroup)

	// 判断本条记录是否是 其他记录的child
	if err == nil {
		var parentRestGroup entities.RestGroup
		parentRestGroup, err = dao.GetRestGroupByIdSimple(restGroup.ParentId)

		fmt.Println("============2222222===========")
		fmt.Println("parentsysMenu.Id" + parentRestGroup.Id + "sss")
		fmt.Println(parentRestGroup.Haschild)
		if parentRestGroup.Id != "" {

			fmt.Println("parentRestGroup.Id !=" + parentRestGroup.Id + "sss")
			parentRestGroup.Haschild = true
			UpdateRestGroup(&parentRestGroup)
		} else {

			err = nil
		}
	}

	return restGroup.Id, err
}

func GetRestGroupById(id string) (entities.RestGroupForQuery, error) {
	return dao.GetRestGroupById(id)
}

func DeleteRestGroupById(id string) (bool, error) {
	var restGroup entities.RestGroup
	restGroup.Id = id
	var err error
	var result bool

	restGroup, err = dao.GetRestGroupByIdSimple(id) // 保存删除对象

	result, err = dao.DeleteRestGroupById(id)

	// 在某个节点新增下级节点后再删除所有的下级节点   该根节点的HasChild还是true
	if err == nil {
		// 删除成功则 查询 和本条 菜单记录是否有相同的 ParentID
		// 如果有 则不做处理 ,如果无则 更新 id = ParentID 的那条记录 haschild字段
		sysMenus, err2 := dao.GetRestGroupByParentId(restGroup.ParentId)
		println("err != nil")
		println(sysMenus)
		if sysMenus == nil || len(sysMenus) == 0 {
			println("UpdateSysMenuHasChild")
			dao.UpdateRestGroupHasChild(restGroup.ParentId, false)
		}

		if err2 != nil {
			err = err2
		}
	}

	return result, err
}

func QueryRestGroups(queryCondition entities.QueryCondition) ([]entities.RestGroupForQuery, int64, int64, error) {
	var rests []entities.RestGroupForQuery
	dataRecordCount, err := dao.GetRestsCount(queryCondition)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		rests, err = dao.QueryRestGroups(queryCondition)
	}
	return rests, int64(math.Ceil(pageCount)), dataRecordCount, err
}

func UpdateRestGroup(role *entities.RestGroup) error {
	var tmpRole entities.RestGroupForQuery
	var err error
	tmpRole, err = dao.GetRestGroupById(role.Id)
	if err == nil {
		// 防止以下字段被修改
		role.CreateDatetime = tmpRole.CreateDatetime
		role.Creator = tmpRole.Creator
		role.ModifyDatetime = time.Now()

		if role.Modifier == "" {
			role.Modifier = "admin"
		}
		err = dao.UpdateRestGroup(role)
	}
	return err
}

func RestGroupJoinInRests(restGroupId string, restIds []string) error {
	return dao.RestGroupJoinInRests(restGroupId, restIds)
}

func ClearRestGroupRests(restGroupId string) error {
	return dao.ClearRestGroupRests(restGroupId)
}

func ClearRestGroupTenantusers(restGroupId string) error {
	return dao.ClearRestGroupTenantusers(restGroupId)
}

func RestGroupJoinInTenantusers(restGroupId string, tenantUserIds []string) error {
	return dao.RestGroupJoinInTenantusers(restGroupId, tenantUserIds)
}

func QueryRestGroupRests(id string) ([]entities.Restaurant, error) {
	var rests []entities.Restaurant

	rests, err := dao.QueryRestGroupRests(id)

	return rests, err
}
