package biz

import (
	"RestBest/pkg/dao"
	"RestBest/pkg/entities"
	"math"
	"time"
)

func CreateSysDict(sysdict entities.SystemDict) (string, error) {
	//sysdict.Code = uuid.New().String()
	if sysdict.Creator == "" {
		sysdict.Creator = "admin"
	}
	if sysdict.Modifier == "" {
		sysdict.Modifier = "admin"

	}
	sysdict.CreateDatetime = time.Now()
	sysdict.ModifyDatetime = sysdict.CreateDatetime
	err := dao.CreateSysDict(sysdict)
	return sysdict.Code, err
}

func GetSysDictByCode(code string) (entities.SystemDict, error) {
	return dao.GetSysDictByCode(code)
}

func QuerySysDicts(queryCondition entities.QueryCondition, parent_code string) ([]entities.SystemDict, int64, int64, error) {
	var roles []entities.SystemDict
	dataRecordCount, err := dao.GetSysDictCount(queryCondition)
	pageCount := float64(dataRecordCount) / float64(queryCondition.PageSize)
	if err == nil {
		roles, err = dao.QuerySysDicts(queryCondition, parent_code)
	}
	return roles, int64(math.Ceil(pageCount)), dataRecordCount, err

}

func UpdateSysDict(role *entities.SystemDict) error {
	var tmpRole entities.SystemDict
	var err error
	tmpRole, err = dao.GetSysDictByCode(role.Code)
	if err == nil {
		// 防止以下字段被修改
		role.CreateDatetime = tmpRole.CreateDatetime
		role.Creator = tmpRole.Creator
		role.ModifyDatetime = time.Now()

		if role.Modifier == "" {
			role.Modifier = "admin"
		}
		err = dao.UpdateSysDict(role)
	}
	return err
}

func DeleteSysDict(code string) (bool, error) {
	var systemDict entities.SystemDict
	systemDict.Code = code
	return dao.DeleteSysDict(systemDict)
}
