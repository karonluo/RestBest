package services

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/entities"
	"RestBest/pkg/tools"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/kataras/iris/v12"
)

func WSCreateOrder(ctx iris.Context) {
	var order entities.Order
	message := WebServiceMessage{StatusCode: 200, Message: true}
	body, err := ctx.GetBody()
	if err == nil {
		err = json.Unmarshal(body, &order)
		if err == nil {
			err = biz.CreateOrder(&order)
			//fmt.Println("====================	")
			//fmt.Println(err)
			if err == nil {
				message.Message = order
			} else {
				message.StatusCode = 500
				message.Message = err
				tools.ProcessError("services.WSCreateOrder", "err = biz.CreateOrder(&menu)", err)
			}
		} else {
			message.StatusCode = 500
			message.Message = err
			tools.ProcessError("services.WSCreateOrder", "err = biz.CreateOrder(&menu)", err)
		}
	} else {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.WSCreateOrder", "body, err := ctx.GetBody()", err)
	}

	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetOrderById(ctx iris.Context) {

	message := WebServiceMessage{StatusCode: 200, Message: true}
	id := ctx.FormValue("id")
	menu, err := biz.GetOrderById(id)

	if err != nil {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.GetOrderById", "menu, err := biz.GetOrderById(id)", err)
	} else {
		message.Message = menu
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSDeleteOrder(ctx iris.Context) {

	message := WebServiceMessage{StatusCode: 200, Message: true}
	id := ctx.FormValue("id")
	deletedCount, err := biz.DeleteOrderById(id)

	if err != nil {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.WSDeleteOrder", "menu, err := biz.DeleteOrderById(menu_id)", err)
	} else {
		if deletedCount {
			message.StatusCode = 200
			message.Message = "成功"
		} else {
			message.StatusCode = 404
			message.Message = "未找到删除对象"
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询订单列表
func WSQueryOrders(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var queryCondition entities.QueryCondition
	var orders []entities.Order
	var err, query_err error
	var pageCount, recordCount int64
	result := make(map[string]interface{})

	queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))

	if err != nil {
		tools.ProcessError(`services.WSQueryOrders`,
			`queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {

		table_id := ctx.FormValue("table_id")
		rest_id := ctx.FormValue("rest_id")
		orders, pageCount, recordCount, query_err = biz.QueryOrders(queryCondition, table_id, rest_id)
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQueryOrders`, `users, pageCount, recordCount, query_err = biz.QueryMenus(queryCondition, restaurantID)`, query_err)
		} else {
			result["page_count"] = pageCount
			result["record_count"] = recordCount
			result["orders"] = orders
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 修改订单
func WSUpdateOrder(ctx iris.Context) {
	var order entities.Order
	message := WebServiceMessage{Message: true, StatusCode: 200}
	body, _ := ctx.GetBody()
	err := json.Unmarshal(body, &order)

	if err == nil {
		err = biz.UpdateOrder(&order)
		if err != nil {
			tools.ProcessError("services.WSUpdateOrder", "err = biz.UpdateOrder(&order)", err)
			message.Message = err.Error()
			message.StatusCode = 500
		}
	} else {
		tools.ProcessError("services.WSUpdateOrder", `role, err := entities.UnmarshalSysRole(body)`, err)
		message.Message = err.Error()
		message.StatusCode = 500
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 设置订单与餐桌关联
func WSOrderJoinInTables(ctx iris.Context) {
	message := WebServiceMessage{Message: true, StatusCode: 200}
	order_id := ctx.FormValue("order_id")                       //订单唯一编码
	table_ids := strings.Split(ctx.FormValue("table_ids"), ",") //餐桌唯一编码，逗号分隔

	fmt.Println(order_id)
	fmt.Println(table_ids)

	err := biz.ClearTablesOrders(order_id)
	if err == nil {
		if len(table_ids) > 0 && table_ids[0] != "" {
			err = biz.OrderJoinInTables(order_id, table_ids)
			if err != nil {
				message.StatusCode = 500
				message.Message = err.Error()
			}
		}
	} else {
		message.StatusCode = 500
		message.Message = err.Error()
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}
