package services

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/entities"
	"RestBest/pkg/tools"
	"encoding/json"
	"fmt"

	"github.com/kataras/iris/v12"
)

// 创建配菜
func WSCreateTopping(ctx iris.Context) {
	var message WebServiceMessage
	var topping entities.Topping
	message.Message = "OK"
	message.StatusCode = 200
	bsysuser, _ := ctx.GetBody()
	err := json.Unmarshal(bsysuser, &topping)
	if err != nil {
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		userId, err := biz.CreateTopping(topping, ctx.FormValue("dishes_id"))
		if err == nil {
			message.Message = userId

		} else {
			message.Message = err.Error()
			message.StatusCode = 500
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 删除配菜
func WSDeleteTopping(ctx iris.Context) {
	var message WebServiceMessage
	var result bool = true
	var err error
	message.StatusCode = 200
	if ctx.FormValue("id") != "" {
		result, err = biz.DeleteToppingById(ctx.FormValue("id"))
		fmt.Println("DeleteToppingById")
		fmt.Println(result)
		if err != nil {
			message.Message = err.Error()
			message.StatusCode = 500
		} else {
			if !result {
				message.Message = "删除对象不存在"
				message.StatusCode = 404
			}
		}
	} else {
		message.Message = "查询条件不为空"
		message.StatusCode = 404
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 修改配菜
func WSUpdateTopping(ctx iris.Context) {
	var topping entities.Topping
	message := WebServiceMessage{Message: true, StatusCode: 200}
	body, _ := ctx.GetBody()
	err := json.Unmarshal(body, &topping)
	if err == nil {
		err = biz.UpdateTopping(topping)
		if err != nil {
			tools.ProcessError("services.WSUpdateTable", "err = biz.UpdateSysUser(user)", err)
			message.Message = err.Error()
			message.StatusCode = 500
		}
	} else {
		tools.ProcessError("services.WSUpdateTable", `err:=json.Unmarshal(body, &user)`, err)
		message.Message = err.Error()
		message.StatusCode = 500
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询配菜
func WSQueryToppings(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var queryCondition entities.QueryCondition
	var toppings []entities.Topping
	var err, query_err error
	var pageCount, recordCount int64
	result := make(map[string]interface{})
	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)
	queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))
	//companyID := ctx.FormValue("company_id")
	if err != nil {
		tools.ProcessError(`services.WSQueryTopping`,
			`queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		toppings, pageCount, recordCount, query_err = biz.QueryToppings(queryCondition, ctx.FormValue("dishes_id"))
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQueryTopping`, `users, pageCount, recordCount, query_err = biz.QueryToppings(queryCondition, rest_id)`, query_err)
		} else {
			result["page_count"] = pageCount
			result["record_count"] = recordCount
			result["toppings"] = toppings
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetTopping(ctx iris.Context) {
	var message WebServiceMessage
	message.Message = true
	message.StatusCode = 200
	var res bool
	var topping entities.Topping

	topping, res, _ = biz.GetToppingById(ctx.FormValue("id"))

	if res {
		message.Message = topping
	} else {
		message.Message = false
		message.StatusCode = 404
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}
