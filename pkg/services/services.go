package services

import (
	"fmt"

	"github.com/kataras/iris/v12"
)

func WSServiceReadme(ctx iris.Context) {
	fmt.Println("Services readme")
	ctx.HTML("<h1>RestBest Service Layer!</h1>")
}

type WebServiceMessage struct {
	StatusCode int
	Message    interface{}
}
