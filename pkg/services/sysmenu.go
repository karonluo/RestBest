package services

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/entities"
	"RestBest/pkg/tools"
	"encoding/json"
	"fmt"

	"github.com/kataras/iris/v12"
)

func WSCreateSysMenu(ctx iris.Context) {
	var message WebServiceMessage
	var sysmenu entities.SysMenu
	message.Message = "OK"
	message.StatusCode = 200
	bsysuser, _ := ctx.GetBody()
	err := json.Unmarshal(bsysuser, &sysmenu)
	if err != nil {
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		userId, err := biz.CreateSysMenu(sysmenu)
		fmt.Println("=============111111==========")
		fmt.Println(err)
		if err == nil {
			message.Message = userId

		} else {
			message.Message = err.Error()
			message.StatusCode = 500
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询系统菜单列表
func WSQuerySysMenus(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var queryCondition entities.QueryCondition
	var users []entities.SysMenu
	var err, query_err error
	var pageCount, recordCount int64
	result := make(map[string]interface{})
	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)
	queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))
	//companyID := ctx.FormValue("company_id")
	if err != nil {
		tools.ProcessError(`services.WSQuerySysMenus`,
			`queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		users, pageCount, recordCount, query_err = biz.QuerySysMenus(queryCondition, ctx.FormValue("parent_id"))
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQuerySysMenus`, `users, pageCount, recordCount, query_err = biz.QuerySysMenus(queryCondition, companyID)`, query_err)
		} else {
			result["page_count"] = pageCount
			result["record_count"] = recordCount
			result["sys_users"] = users
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 返回全部的菜单列表
func WSQueryAllSysMenus(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}

	var users []entities.SysMenu
	var err, query_err error
	result := make(map[string]interface{})

	if err != nil {
		tools.ProcessError(`services.WSQuerySysMenusAll`,
			`queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		users, query_err = biz.QueryAllSysMenus()
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQueryAllSysMenus`, `users, pageCount, recordCount, query_err = biz.QuerySysMenus(queryCondition, companyID)`, query_err)
		} else {

			result["sys_users"] = users
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetSysMenuById(ctx iris.Context) {
	var message WebServiceMessage
	message.Message = true
	message.StatusCode = 200
	sysuser, err := biz.GetSysMenuById(ctx.FormValue("id"))
	if err == nil {
		message.Message = sysuser
	} else {
		message.Message = err.Error()
		message.StatusCode = 500
		tools.ProcessError("services.WSGetSysMenuById", `sysMenu, err := biz.GetSysMenuById(ctx.FormValue("id"))`, err)
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetSysMenuByParentId(ctx iris.Context) {
	var message WebServiceMessage
	message.Message = true
	message.StatusCode = 200
	sysuser, err := biz.GetSysMenuByParentId(ctx.FormValue("parent_id"))
	if err == nil {
		message.Message = sysuser
	} else {
		message.Message = err.Error()
		message.StatusCode = 500
		tools.ProcessError("services.WSGetSysMenuById", `sysMenu, err := biz.GetSysMenuById(ctx.FormValue("id"))`, err)
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 修改系统菜单
func WSUpdateSysMenu(ctx iris.Context) {
	var sysmenu entities.SysMenu
	message := WebServiceMessage{Message: true, StatusCode: 200}
	body, _ := ctx.GetBody()
	//role, err := entities.UnmarshalSysRole(body)
	err := json.Unmarshal(body, &sysmenu)

	if err == nil {
		err = biz.UpdateSysMenu(&sysmenu)
		if err != nil {
			tools.ProcessError("services.WSUpdateSysMenu", "err = biz.UpdateSysMenu(&role)", err)
			message.Message = err.Error()
			message.StatusCode = 500
		}
	} else {
		tools.ProcessError("services.WSUpdateSysMenu", `role, err := entities.UnmarshalSysRole(body)`, err)
		message.Message = err.Error()
		message.StatusCode = 500
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 删除系统菜单
func WSDeleteSysMenu(ctx iris.Context) {
	var message WebServiceMessage
	var result bool = true
	var err error
	message.StatusCode = 200
	if ctx.FormValue("id") != "" {
		result, err = biz.DeleteSysMenu(ctx.FormValue("id"))
		if err != nil {
			message.Message = err.Error()
			message.StatusCode = 500
		} else {
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}
