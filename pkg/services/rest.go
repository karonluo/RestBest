package services

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/entities"
	"RestBest/pkg/tools"
	"encoding/json"

	"github.com/kataras/iris/v12"
)

func WSCreateRest(ctx iris.Context) {
	var rest entities.Restaurant
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	body, err := ctx.GetBody()
	if err == nil {
		err = json.Unmarshal(body, &rest)

		if err != nil {
			tools.ProcessError(`services.WSCreateRest`, `role, err := json.Unmarshal(body, &rest)`, err, `pkg/services/rest.go`)
			message.StatusCode = 500
			message.Message = err.Error()

		} else {
			id, err := biz.CreateRest(rest)
			if err != nil {
				message.StatusCode = 500
				message.Message = err.Error()
			} else {
				message.Message = id
			}
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetRestById(ctx iris.Context) {
	var message = WebServiceMessage{Message: true, StatusCode: 200}
	message.Message = true
	message.StatusCode = 200
	sysRole, err := biz.GetRestById(ctx.FormValue("id"))
	if err == nil {
		message.Message = sysRole
	} else {
		message.Message = err.Error()
		message.StatusCode = 500
		tools.ProcessError("services.WSGetRestById", `sysDict, err := biz.WSGetRestById(ctx.FormValue("id"))`, err)
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询列表
func WSQueryRests(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var queryCondition entities.QueryCondition
	var rests []entities.Restaurant
	var err, query_err error
	var pageCount, recordCount int64
	result := make(map[string]interface{})
	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)
	queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))

	if err != nil {
		tools.ProcessError(`services.WSQueryRests`,
			`companyQueryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		rests, pageCount, recordCount, query_err = biz.QueryRests(queryCondition)
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQueryRests`, `rests, pageCount, recordCount, query_err = biz.QueryRests(queryCondition)`, query_err)
		} else {
			result["page_count"] = pageCount
			result["record_count"] = recordCount
			result["rests"] = rests
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 修改商家
func WSUpdateRest(ctx iris.Context) {
	var rest entities.Restaurant
	message := WebServiceMessage{Message: true, StatusCode: 200}
	body, _ := ctx.GetBody()
	err := json.Unmarshal(body, &rest)
	//fmt.Println(body)
	//fmt.Println("sysDict.ParentCode")

	//fmt.Println(sysDict.ParentCode)

	if err == nil {
		err = biz.UpdateRest(&rest)
		if err != nil {
			tools.ProcessError("services.WSUpdateRest", "err = biz.UpdateSysRole(&role)", err)
			message.Message = err.Error()
			message.StatusCode = 500
		}
	} else {
		tools.ProcessError("services.WSUpdateRest", `role, err := entities.UnmarshalSysRole(body)`, err)
		message.Message = err.Error()
		message.StatusCode = 500
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 删除商家
func WSDeleteRest(ctx iris.Context) {
	var message WebServiceMessage
	var result bool = true
	var err error
	message.StatusCode = 200
	if ctx.FormValue("id") != "" {
		result, err = biz.DeleteRestById(ctx.FormValue("id"))
		if err != nil {
			message.Message = err.Error()
			message.StatusCode = 500
		} else {
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}
