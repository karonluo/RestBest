package services

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/entities"
	"RestBest/pkg/tools"
	"encoding/json"

	"github.com/kataras/iris/v12"
)

func WSCreateDishSpec(ctx iris.Context) {
	var dishSpec entities.DishSpec
	message := WebServiceMessage{StatusCode: 200, Message: true}
	body, err := ctx.GetBody()
	if err == nil {
		err = json.Unmarshal(body, &dishSpec)
		if err == nil {
			err = biz.CreateDishSpec(&dishSpec)
			//fmt.Println("====================	")
			//fmt.Println(err)
			if err == nil {
				message.Message = dishSpec
			} else {
				message.StatusCode = 500
				message.Message = err
				tools.ProcessError("services.WSCreateDishSpec", "err = biz.CreateDishSpec(&menu)", err)
			}
		} else {
			message.StatusCode = 500
			message.Message = err
			tools.ProcessError("services.WSCreateDishSpec", "err = biz.CreateDishSpec(&menu)", err)
		}
	} else {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.WSCreateDishSpec", "body, err := ctx.GetBody()", err)
	}

	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetDishSpecById(ctx iris.Context) {

	message := WebServiceMessage{StatusCode: 200, Message: true}
	id := ctx.FormValue("id")
	menu, err := biz.GetDishSpecById(id)

	if err != nil {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.WSGetDishSpecById", "menu, err := biz.GetDishSpecById(id)", err)
	} else {
		message.Message = menu
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSDeleteDishSpec(ctx iris.Context) {

	message := WebServiceMessage{StatusCode: 200, Message: true}
	id := ctx.FormValue("id")
	deletedCount, err := biz.DeleteDishSpecById(id)

	if err != nil {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.WSDeleteDishSpec", "menu, err := biz.DeleteDishSpecById(menu_id)", err)
	} else {
		if deletedCount {
			message.StatusCode = 200
			message.Message = "成功"
		} else {
			message.StatusCode = 404
			message.Message = "未找到删除对象"
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询菜品规格
func WSQueryDishesSpec(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var queryCondition entities.QueryCondition
	var dishesspeces []entities.DishSpec
	var err, query_err error
	var pageCount, recordCount int64
	result := make(map[string]interface{})
	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)
	queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))

	if err != nil {
		tools.ProcessError(`services.WSQueryDishesSpec`,
			`queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {

		dishes_id := ctx.FormValue("dishes_id")
		dishesspeces, pageCount, recordCount, query_err = biz.QueryDishesSpec(queryCondition, dishes_id)
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQueryDishesSpec`, `users, pageCount, recordCount, query_err = biz.QueryDishesSpec(queryCondition, dish_id)`, query_err)
		} else {
			result["page_count"] = pageCount
			result["record_count"] = recordCount
			result["dishesspec"] = dishesspeces
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 修改菜品规格
func WSUpdateDishSpec(ctx iris.Context) {
	var rest entities.DishSpec
	message := WebServiceMessage{Message: true, StatusCode: 200}
	body, _ := ctx.GetBody()
	err := json.Unmarshal(body, &rest)

	if err == nil {
		err = biz.UpdateDishSpec(&rest)
		if err != nil {
			tools.ProcessError("services.WSUpdateDishSpec", "err = biz.UpdateSysRole(&role)", err)
			message.Message = err.Error()
			message.StatusCode = 500
		}
	} else {
		tools.ProcessError("services.WSUpdateDishSpec", `role, err := entities.UnmarshalSysRole(body)`, err)
		message.Message = err.Error()
		message.StatusCode = 500
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}
