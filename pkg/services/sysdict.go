package services

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/entities"
	"RestBest/pkg/tools"
	"encoding/json"

	"github.com/kataras/iris/v12"
)

func WSCreateSysDict(ctx iris.Context) {
	var sysDict entities.SystemDict
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	body, err := ctx.GetBody()
	if err == nil {
		err = json.Unmarshal(body, &sysDict)

		//role, err := entities.UnmarshalSysRole(bytesRole)

		if err != nil {
			tools.ProcessError(`services.WSCreateSysDict`, `role, err := json.Unmarshal(body, &sysDict)`, err, `pkg/services/sysdict.go`)
			message.StatusCode = 500
			message.Message = err.Error()

		} else {
			id, err := biz.CreateSysDict(sysDict)
			if err != nil {
				message.StatusCode = 500
				message.Message = err.Error()
			} else {
				message.Message = id
			}
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetSysDictByCode(ctx iris.Context) {
	var message = WebServiceMessage{Message: true, StatusCode: 200}
	message.Message = true
	message.StatusCode = 200
	sysRole, err := biz.GetSysDictByCode(ctx.FormValue("code"))
	if err == nil {
		message.Message = sysRole
	} else {
		message.Message = err.Error()
		message.StatusCode = 500
		tools.ProcessError("services.WSGetSysDictByCode", `sysDict, err := biz.GetSysDictByCode(ctx.FormValue("role_id"))`, err)
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询列表
func WSQuerySysDicts(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var queryCondition entities.QueryCondition
	var sysdicts []entities.SystemDict
	var err, query_err error
	var pageCount, recordCount int64
	result := make(map[string]interface{})
	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)
	queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))

	if err != nil {
		tools.ProcessError(`services.WSQuerySysDicts`,
			`companyQueryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		sysdicts, pageCount, recordCount, query_err = biz.QuerySysDicts(queryCondition, ctx.FormValue("parent_code"))
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQuerySysRoles`, `roles, pageCount, recordCount, query_err = biz.QuerySysRoles(queryCondition)`, query_err)
		} else {
			result["page_count"] = pageCount
			result["record_count"] = recordCount
			result["sys_dicts"] = sysdicts
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 修改系统字典
func WSUpdateSysDict(ctx iris.Context) {
	var sysDict entities.SystemDict
	message := WebServiceMessage{Message: true, StatusCode: 200}
	body, _ := ctx.GetBody()
	err := json.Unmarshal(body, &sysDict)
	//fmt.Println(body)
	//fmt.Println("sysDict.ParentCode")

	//fmt.Println(sysDict.ParentCode)

	if err == nil {
		err = biz.UpdateSysDict(&sysDict)
		if err != nil {
			tools.ProcessError("services.WSUpdateSysRole", "err = biz.UpdateSysRole(&role)", err)
			message.Message = err.Error()
			message.StatusCode = 500
		}
	} else {
		tools.ProcessError("services.WSUpdateSysRole", `role, err := entities.UnmarshalSysRole(body)`, err)
		message.Message = err.Error()
		message.StatusCode = 500
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 删除系统菜单
func WSDeleteSysDict(ctx iris.Context) {
	var message WebServiceMessage
	var result bool = true
	var err error
	message.StatusCode = 200
	if ctx.FormValue("code") != "" {
		result, err = biz.DeleteSysDict(ctx.FormValue("code"))
		if err != nil {
			message.Message = err.Error()
			message.StatusCode = 500
		} else {
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}
