package services

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/entities"
	"RestBest/pkg/tools"
	"encoding/json"
	"strings"

	"github.com/kataras/iris/v12"
)

func WSCreateRestGroup(ctx iris.Context) {
	var sysDict entities.RestGroup
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	body, err := ctx.GetBody()
	if err == nil {
		err = json.Unmarshal(body, &sysDict)

		//role, err := entities.UnmarshalSysRole(bytesRole)

		if err != nil {
			tools.ProcessError(`services.WSCreateSysDict`, `role, err := json.Unmarshal(body, &sysDict)`, err, `pkg/services/sysdict.go`)
			message.StatusCode = 500
			message.Message = err.Error()

		} else {
			id, err := biz.CreateRestGroup(sysDict)
			if err != nil {
				message.StatusCode = 500
				message.Message = err.Error()
			} else {
				message.Message = id
			}
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetRestGroupById(ctx iris.Context) {
	var message = WebServiceMessage{Message: true, StatusCode: 200}
	message.Message = true
	message.StatusCode = 200
	sysRole, err := biz.GetRestGroupById(ctx.FormValue("id"))
	if err == nil {
		message.Message = sysRole
	} else {
		message.Message = err.Error()
		message.StatusCode = 500
		tools.ProcessError("services.WSGetRestById", `sysDict, err := biz.WSGetRestById(ctx.FormValue("id"))`, err)
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询列表
func WSQueryRestGroups(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var queryCondition entities.QueryCondition
	var rests []entities.RestGroupForQuery
	var err, query_err error
	var pageCount, recordCount int64
	result := make(map[string]interface{})
	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)
	queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))

	if err != nil {
		tools.ProcessError(`services.WSQueryRests`,
			`companyQueryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		rests, pageCount, recordCount, query_err = biz.QueryRestGroups(queryCondition)
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQueryRests`, `rests, pageCount, recordCount, query_err = biz.QueryRests(queryCondition)`, query_err)
		} else {

			result["page_count"] = pageCount
			result["record_count"] = recordCount
			result["rests"] = rests
			message.Message = result
		}
	}

	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 修改商家
func WSUpdateRestGroup(ctx iris.Context) {
	var rest entities.RestGroup
	message := WebServiceMessage{Message: true, StatusCode: 200}
	body, _ := ctx.GetBody()
	err := json.Unmarshal(body, &rest)
	//fmt.Println(body)
	//fmt.Println("sysDict.ParentCode")

	//fmt.Println(sysDict.ParentCode)

	if err == nil {
		err = biz.UpdateRestGroup(&rest)
		if err != nil {
			tools.ProcessError("services.WSUpdateRest", "err = biz.UpdateSysRole(&role)", err)
			message.Message = err.Error()
			message.StatusCode = 500
		}
	} else {
		tools.ProcessError("services.WSUpdateRest", `role, err := entities.UnmarshalSysRole(body)`, err)
		message.Message = err.Error()
		message.StatusCode = 500
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 删除商家
func WSDeleteRestGroup(ctx iris.Context) {
	var message WebServiceMessage
	var result bool = true
	var err error
	message.StatusCode = 200
	if ctx.FormValue("id") != "" {
		result, err = biz.DeleteRestGroupById(ctx.FormValue("id"))
		if err != nil {
			message.Message = err.Error()
			message.StatusCode = 500
		} else {
			if result {
				message.StatusCode = 200
				message.Message = "删除成功"
			} else {
				message.StatusCode = 404
				message.Message = "未找到删除对象"
			}
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSRestGroupJoinInRests(ctx iris.Context) {
	message := WebServiceMessage{Message: true, StatusCode: 200}
	rest_ids := strings.Split(ctx.FormValue("rest_ids"), ",")
	rest_group_id := ctx.FormValue("rest_group_id")
	err := biz.ClearRestGroupRests(rest_group_id)
	if err == nil {
		if len(rest_ids) > 0 && rest_ids[0] != "" {
			err = biz.RestGroupJoinInRests(rest_group_id, rest_ids)
			if err != nil {
				message.StatusCode = 500
				message.Message = err.Error()
			}
		}
	} else {
		message.StatusCode = 500
		message.Message = err.Error()
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSRestGroupJoinInTenantusers(ctx iris.Context) {
	message := WebServiceMessage{Message: true, StatusCode: 200}
	tenantuser_ids := strings.Split(ctx.FormValue("tenantuser_ids"), ",")
	rest_group_id := ctx.FormValue("rest_group_id")
	err := biz.ClearRestGroupTenantusers(rest_group_id)
	if err == nil {
		if len(tenantuser_ids) > 0 && tenantuser_ids[0] != "" {
			err = biz.RestGroupJoinInTenantusers(rest_group_id, tenantuser_ids)
			if err != nil {
				message.StatusCode = 500
				message.Message = err.Error()
			}
		}
	} else {
		message.StatusCode = 500
		message.Message = err.Error()
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询列表
func WSQueryRestGroupRests(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var rests []entities.Restaurant
	var query_err error

	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)

	rests, query_err = biz.QueryRestGroupRests(ctx.FormValue("id"))
	if query_err != nil {
		message.Message = query_err.Error()
		message.StatusCode = 500
		tools.ProcessError(`services.WSQueryRestGroupRests`, `rests, pageCount, recordCount, query_err = biz.QueryRestGroupRests(id)`, query_err)
	} else {
		message.Message = rests
	}

	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}
