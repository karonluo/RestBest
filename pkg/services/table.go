package services

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/entities"
	"RestBest/pkg/tools"
	"encoding/json"
	"fmt"

	"github.com/kataras/iris/v12"
)

// 创建商家餐桌
func WSCreateTable(ctx iris.Context) {
	var message WebServiceMessage
	var table entities.Table
	message.Message = "OK"
	message.StatusCode = 200
	bsysuser, _ := ctx.GetBody()
	err := json.Unmarshal(bsysuser, &table)
	if err != nil {
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		userId, err := biz.CreateTable(table)
		if err == nil {
			message.Message = userId

		} else {
			message.Message = err.Error()
			message.StatusCode = 500
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 删除商家餐桌
func WSDeleteTable(ctx iris.Context) {
	var message WebServiceMessage
	var result bool = true
	var err error
	message.StatusCode = 200
	if ctx.FormValue("id") != "" {
		result, err = biz.DeleteTableById(ctx.FormValue("id"))
		fmt.Println("DeleteTableById")
		fmt.Println(result)
		if err != nil {
			message.Message = err.Error()
			message.StatusCode = 500
		} else {
			if !result {
				message.Message = "删除对象不存在"
				message.StatusCode = 404
			}
		}
	} else {
		message.Message = "查询条件不为空"
		message.StatusCode = 404
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 修改商家餐桌
func WSUpdateTable(ctx iris.Context) {
	var user entities.Table
	message := WebServiceMessage{Message: true, StatusCode: 200}
	body, _ := ctx.GetBody()
	err := json.Unmarshal(body, &user)
	if err == nil {
		err = biz.UpdateTable(user)
		if err != nil {
			tools.ProcessError("services.WSUpdateTable", "err = biz.UpdateSysUser(user)", err)
			message.Message = err.Error()
			message.StatusCode = 500
		}
	} else {
		tools.ProcessError("services.WSUpdateTable", `err:=json.Unmarshal(body, &user)`, err)
		message.Message = err.Error()
		message.StatusCode = 500
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询商家餐桌
func WSQueryTables(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var queryCondition entities.QueryCondition
	var tables []entities.Table
	var err, query_err error
	var pageCount, recordCount int64
	result := make(map[string]interface{})
	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)
	queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))
	//companyID := ctx.FormValue("company_id")
	if err != nil {
		tools.ProcessError(`services.WSQueryTables`,
			`queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		tables, pageCount, recordCount, query_err = biz.QueryTables(queryCondition, ctx.FormValue("rest_id"))
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQueryTables`, `users, pageCount, recordCount, query_err = biz.QueryTables(queryCondition, rest_id)`, query_err)
		} else {
			result["page_count"] = pageCount
			result["record_count"] = recordCount
			result["tables"] = tables
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetTable(ctx iris.Context) {
	var message WebServiceMessage
	message.Message = true
	message.StatusCode = 200
	var res bool
	var sysuser entities.Table

	sysuser, res, _ = biz.GetTableById(ctx.FormValue("id"))

	if res {
		message.Message = sysuser
	} else {
		message.Message = false
		message.StatusCode = 404
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}
