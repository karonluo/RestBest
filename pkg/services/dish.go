package services

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/entities"
	"RestBest/pkg/tools"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/kataras/iris/v12"
)

func WSCreateDish(ctx iris.Context) {
	var menu entities.Dish
	message := WebServiceMessage{StatusCode: 200, Message: true}
	body, err := ctx.GetBody()
	if err == nil {
		err = json.Unmarshal(body, &menu)
		if err == nil {
			err = biz.CreateDish(&menu)
			//fmt.Println("====================	")
			//fmt.Println(err)
			if err == nil {
				message.Message = menu
			} else {
				message.StatusCode = 500
				message.Message = err
				tools.ProcessError("services.WSCreateMenu", "err = biz.CreateMenu(&menu)", err)
			}
		} else {
			message.StatusCode = 500
			message.Message = err
			tools.ProcessError("services.WSCreateMenu", "err = biz.CreateMenu(&menu)", err)
		}
	} else {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.WSCreateMenu", "body, err := ctx.GetBody()", err)
	}

	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetDishById(ctx iris.Context) {

	message := WebServiceMessage{StatusCode: 200, Message: true}
	id := ctx.FormValue("id")
	menu, err := biz.GetDishById(id)

	if err != nil {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.WSGetMenu", "menu, err := biz.GetMenu(menu_id)", err)
	} else {
		message.Message = menu
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSDeleteDish(ctx iris.Context) {

	message := WebServiceMessage{StatusCode: 200, Message: true}
	id := ctx.FormValue("id")
	deletedCount, err := biz.DeleteDishById(id)

	if err != nil {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.WSDeleteDish", "menu, err := biz.DeleteMenu(menu_id)", err)
	} else {
		if deletedCount {
			message.StatusCode = 200
			message.Message = "成功"
		} else {
			message.StatusCode = 404
			message.Message = "未找到删除对象"
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询菜单列表
func WSQueryDishes(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var queryCondition entities.QueryCondition
	var menus []entities.Dish
	var err, query_err error
	var pageCount, recordCount int64
	result := make(map[string]interface{})
	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)
	queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))

	if err != nil {
		tools.ProcessError(`services.WSQueryMenus`,
			`queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {

		rest_id := ctx.FormValue("rest_id")
		menus, pageCount, recordCount, query_err = biz.QueryDishes(queryCondition, rest_id)
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQueryMenus`, `users, pageCount, recordCount, query_err = biz.QueryMenus(queryCondition, restaurantID)`, query_err)
		} else {
			result["page_count"] = pageCount
			result["record_count"] = recordCount
			result["menus"] = menus
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 修改菜品
func WSUpdateDish(ctx iris.Context) {
	var rest entities.Dish
	message := WebServiceMessage{Message: true, StatusCode: 200}
	body, _ := ctx.GetBody()
	err := json.Unmarshal(body, &rest)

	if err == nil {
		err = biz.UpdateDish(&rest)
		if err != nil {
			tools.ProcessError("services.WSUpdateRest", "err = biz.UpdateSysRole(&role)", err)
			message.Message = err.Error()
			message.StatusCode = 500
		}
	} else {
		tools.ProcessError("services.WSUpdateRest", `role, err := entities.UnmarshalSysRole(body)`, err)
		message.Message = err.Error()
		message.StatusCode = 500
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 设置菜品配菜关系
func WSDishJoinToppings(ctx iris.Context) {
	message := WebServiceMessage{Message: true, StatusCode: 200}
	dish_id := ctx.FormValue("dish_id")                             //菜品唯一编码
	topping_ids := strings.Split(ctx.FormValue("topping_ids"), ",") // 配菜唯一编码，逗号分隔

	fmt.Println(dish_id)
	fmt.Println(topping_ids)

	err := biz.ClearDishToppings(dish_id)
	if err == nil {
		if len(topping_ids) > 0 && topping_ids[0] != "" {
			err = biz.DishJoinInToppings(dish_id, topping_ids)
			if err != nil {
				message.StatusCode = 500
				message.Message = err.Error()
			}
		}
	} else {
		message.StatusCode = 500
		message.Message = err.Error()
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}
