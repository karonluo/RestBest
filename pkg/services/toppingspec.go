package services

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/entities"
	"RestBest/pkg/tools"
	"encoding/json"
	"fmt"

	"github.com/kataras/iris/v12"
)

// 创建配菜
func WSCreateToppingSpec(ctx iris.Context) {
	var message WebServiceMessage
	var topping entities.ToppingSpec
	message.Message = "OK"
	message.StatusCode = 200
	bsysuser, _ := ctx.GetBody()
	err := json.Unmarshal(bsysuser, &topping)
	if err != nil {
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		fmt.Println(topping.Id)
		fmt.Println(topping.ToppingId)
		userId, err := biz.CreateToppingSpec(topping)
		if err == nil {
			message.Message = userId

		} else {
			message.Message = err.Error()
			message.StatusCode = 500
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 删除配菜
func WSDeleteToppingSpec(ctx iris.Context) {
	var message WebServiceMessage
	var result bool = true
	var err error
	message.StatusCode = 200
	if ctx.FormValue("id") != "" {
		result, err = biz.DeleteToppingSpecById(ctx.FormValue("id"))
		fmt.Println("WSDeleteToppingSpec")
		fmt.Println(result)
		if err != nil {
			message.Message = err.Error()
			message.StatusCode = 500
		} else {
			if !result {
				message.Message = "删除对象不存在"
				message.StatusCode = 404
			}
		}
	} else {
		message.Message = "查询条件不为空"
		message.StatusCode = 404
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 修改配菜
func WSUpdateToppingSpec(ctx iris.Context) {
	var topping entities.ToppingSpec
	message := WebServiceMessage{Message: true, StatusCode: 200}
	body, _ := ctx.GetBody()
	err := json.Unmarshal(body, &topping)
	if err == nil {
		err = biz.UpdateToppingSpec(topping)
		if err != nil {
			tools.ProcessError("services.WSUpdateToppingSpec", "err = biz.UpdateSysUser(user)", err)
			message.Message = err.Error()
			message.StatusCode = 500
		}
	} else {
		tools.ProcessError("services.WSUpdateToppingSpec", `err:=json.Unmarshal(body, &user)`, err)
		message.Message = err.Error()
		message.StatusCode = 500
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询配菜
func WSQueryToppingSpec(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var queryCondition entities.QueryCondition
	var toppingSpeces []entities.ToppingSpec
	var err, query_err error
	var pageCount, recordCount int64
	result := make(map[string]interface{})
	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)
	queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))
	//companyID := ctx.FormValue("company_id")
	if err != nil {
		tools.ProcessError(`services.WSQueryToppingSpec`,
			`queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		toppingSpeces, pageCount, recordCount, query_err = biz.QueryToppingSpeces(queryCondition, ctx.FormValue("rest_id"))
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQueryToppingSpec`, `users, pageCount, recordCount, query_err = biz.QueryToppingSpeces(queryCondition, rest_id)`, query_err)
		} else {
			result["page_count"] = pageCount
			result["record_count"] = recordCount
			result["toppingSpeces"] = toppingSpeces
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetToppingSpec(ctx iris.Context) {
	var message WebServiceMessage
	message.Message = true
	message.StatusCode = 200
	var res bool
	var toppingSpec entities.ToppingSpec

	toppingSpec, res, _ = biz.GetToppingSpecById(ctx.FormValue("id"))

	if res {
		message.Message = toppingSpec
	} else {
		message.Message = false
		message.StatusCode = 404
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}
