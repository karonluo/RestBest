package services

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/entities"
	"RestBest/pkg/tools"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/kataras/iris/v12"
)

func WSCreateMenu(ctx iris.Context) {
	var menu entities.Menu
	message := WebServiceMessage{StatusCode: 200, Message: true}
	body, err := ctx.GetBody()
	if err == nil {
		err = json.Unmarshal(body, &menu)
		if err == nil {
			err = biz.CreateMenu(&menu)
			//fmt.Println("====================	")
			//fmt.Println(err)
			if err == nil {
				message.Message = menu
			} else {
				message.StatusCode = 500
				message.Message = err
				tools.ProcessError("services.WSCreateMenu", "err = biz.CreateMenu(&menu)", err)
			}
		} else {
			message.StatusCode = 500
			message.Message = err
			tools.ProcessError("services.WSCreateMenu", "err = biz.CreateMenu(&menu)", err)
		}
	} else {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.WSCreateMenu", "body, err := ctx.GetBody()", err)
	}

	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSGetMenu(ctx iris.Context) {

	message := WebServiceMessage{StatusCode: 200, Message: true}
	menu_id := ctx.FormValue("id")
	menu, err := biz.GetMenu(menu_id)

	if err != nil {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.WSGetMenu", "menu, err := biz.GetMenu(menu_id)", err)
	} else {
		message.Message = menu
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

func WSDeleteMenu(ctx iris.Context) {

	message := WebServiceMessage{StatusCode: 200, Message: true}
	menu_id := ctx.FormValue("menu_id")
	menu, err := biz.DeleteMenu(menu_id)

	if err != nil {
		message.StatusCode = 500
		message.Message = err
		tools.ProcessError("services.WSDeleteMenu", "menu, err := biz.DeleteMenu(menu_id)", err)
	} else {
		message.Message = menu
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询菜单列表
func WSQueryMenus(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}
	var queryCondition entities.QueryCondition
	var menus []entities.Menu
	var err, query_err error
	var pageCount, recordCount int64
	result := make(map[string]interface{})
	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)
	queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))
	restaurantID := ctx.FormValue("rest_id")
	if err != nil {
		tools.ProcessError(`services.WSQueryMenus`,
			`queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		menus, pageCount, recordCount, query_err = biz.QueryMenus(queryCondition, restaurantID)
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQueryMenus`, `users, pageCount, recordCount, query_err = biz.QueryMenus(queryCondition, restaurantID)`, query_err)
		} else {
			result["page_count"] = pageCount
			result["record_count"] = recordCount
			result["menus"] = menus
			message.Message = result
		}
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 修改菜单
func WSUpdateMenu(ctx iris.Context) {
	var menu entities.Menu
	message := WebServiceMessage{Message: true, StatusCode: 200}
	body, _ := ctx.GetBody()
	err := json.Unmarshal(body, &menu)
	//fmt.Println(body)
	//fmt.Println("sysDict.ParentCode")

	//fmt.Println(sysDict.ParentCode)

	if err == nil {
		err = biz.UpdateMenu(&menu)
		if err != nil {
			tools.ProcessError("services.WSUpdateRest", "err = biz.UpdateSysRole(&role)", err)
			message.Message = err.Error()
			message.StatusCode = 500
		}
	} else {
		tools.ProcessError("services.WSUpdateRest", `role, err := entities.UnmarshalSysRole(body)`, err)
		message.Message = err.Error()
		message.StatusCode = 500
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 设置菜单与菜品关联
func WSMenuJoinInDishes(ctx iris.Context) {
	message := WebServiceMessage{Message: true, StatusCode: 200}
	menu_id := ctx.FormValue("menu_id")                           //菜单唯一编码
	rest_id := ctx.FormValue("rest_id")                           // 商家唯一编码
	dishes_ids := strings.Split(ctx.FormValue("dishes_ids"), ",") //菜品唯一编码，逗号分隔

	err := biz.ClearMenuDishes(menu_id)
	if err == nil {
		if len(dishes_ids) > 0 && dishes_ids[0] != "" {
			err = biz.MenuJoinInDishes(menu_id, dishes_ids, rest_id)
			if err != nil {
				message.StatusCode = 500
				message.Message = err.Error()
			}
		}
	} else {
		message.StatusCode = 500
		message.Message = err.Error()
	}
	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}

// 查询单个商店下所有菜单、菜品、配菜列表
func WSQueryFullMenus(ctx iris.Context) {
	message := WebServiceMessage{Message: "OK", StatusCode: 200}

	var menus entities.FullMenuList
	var err, query_err error

	result := make(map[string]interface{})
	// body, _ := ctx.GetBody()
	// err := json.Unmarshal(body, &companyQueryCondition)
	fmt.Println(ctx.JSON(result))
	restaurantID := ctx.FormValue("rest_id")

	fmt.Println("WSQueryFullMenus start restaurantID:" + restaurantID)

	if err != nil {
		tools.ProcessError(`services.WSQueryMenus`,
			`queryCondition, err = tools.GenerateQueryConditionFromWebParameters(ctx.FormValue("page_size"), ctx.FormValue("page_index"), ctx.FormValue("like_value"))`,
			err)
		message.Message = err.Error()
		message.StatusCode = 500
	} else {
		menus, query_err = biz.QueryFullMenus(restaurantID)
		if query_err != nil {
			message.Message = query_err.Error()
			message.StatusCode = 500
			tools.ProcessError(`services.WSQueryMenus`, `users, pageCount, recordCount, query_err = biz.QueryMenus(queryCondition, restaurantID)`, query_err)
		} else {

			result["menus"] = menus
			message.Message = result

			//fmt.Println(ctx.JSON(result))
		}

	}

	ctx.StatusCode(message.StatusCode)
	ctx.JSON(message)
}
