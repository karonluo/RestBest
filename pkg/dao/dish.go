package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

// 菜品
func CreateDish(dish entities.Dish) string {
	Database.Create(&dish)
	return dish.Id

}

func DeleteDishById(id string) (bool, error) {
	var result bool = true
	var menu entities.Menu
	menu.Id = id
	res := Database.Exec("DELETE FROM dishes WHERE id=?", id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除菜品: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除菜品: %s 失败, 原因是: %s \r\n", id, "未找到该菜品")
	}
	return result, res.Error
}

func DeleteDish(dish entities.Dish) (bool, error) {
	result := Database.Delete(&dish)
	var res bool = true
	if result.RowsAffected == 0 {
		res = false
	}
	return res, result.Error
}

func QueryDishes(queryCondition entities.QueryCondition, rest_id string) ([]entities.Dish, error) {
	var dish entities.Dish
	var dishes []entities.Dish
	var result *gorm.DB
	selectFields := `id,name,category,unit,image,small_image,banner,taste,create_datetime,modify_datetime,Creator,Modifier,Index,top,status,dishes.desc `
	if queryCondition.LikeValue != "" {
		if rest_id == "" {
			result = Database.Model(&dish).Select(selectFields).Where(
				`name LIKE ? `,
				"%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&dishes)
		} else {

			result = Database.Table("rel_menus_dishes").Select("id,name,category,unit,image,small_image,banner,taste,create_datetime,modify_datetime,Creator,Modifier,Index,top,status,dishes.desc").
				Joins("left join dishes on rel_menus_dishes.dishes_id = dish.id").
				Where(`name LIKE ? and rel_menus_dishes.rest_id = ?`, "%"+queryCondition.LikeValue+"%", rest_id).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&dishes)
		}
	} else {
		if rest_id == "" {
			result = Database.Model(&dish).
				Select(selectFields).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&dishes)
		} else {
			result = Database.Table(`rel_menus_dishes`).Select("dishes.id,dishes.name,dishes.category,dishes.unit,dishes.image,dishes.small_image,dishes.banner,dishes.taste,dishes.create_datetime,dishes.modify_datetime,dishes.Creator,dishes.Modifier,dishes.Index,dishes.top,dishes.status,dishes.desc").
				Joins("left join dishes on rel_menus_dishes.dishes_id = dishes.id").
				Where(`rel_menus_dishes.rest_id = ?`, rest_id).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&dishes)
		}
	}

	return dishes, result.Error
}

func GetDishesCount(queryCondition entities.QueryCondition, rest_id string) (int64, error) {
	var count int64
	var dish entities.Dish
	var result *gorm.DB

	if queryCondition.LikeValue != "" {
		if rest_id == "" {
			// result = Database.Model(&menu).Where(
			// 	`name LIKE ?`,
			// 	"%"+queryCodition.LikeValue+"%").Count(&count)

			result = Database.Model(&dish).Where(
				`name LIKE ? `,
				"%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").Count(&count)

		} else {
			// result = Database.Model(&menu).Where(
			// 	`name LIKE ? and rest_id = ?`,
			// 	"%"+queryCodition.LikeValue+"%", rest_id).Count(&count)
			result = Database.Table("rel_menus_dishes").Select("id,name,category,unit,image,small_image,banner,taste,create_datetime,modify_datetime,Creator,Modifier,Index,top,status,desc").
				Joins("left join dishes on rel_menus_dishes.dishes_id = dish.id").
				Where(`name LIKE ? and rel_menus_dishes.rest_id = ?`, "%"+queryCondition.LikeValue+"%", rest_id).
				Order("modify_datetime DESC").
				Count(&count)

		}
	} else {
		if rest_id == "" {
			// result = Database.Model(&menu).Count(&count)
			result = Database.Model(&dish).
				Count(&count)

		} else {
			// result = Database.Model(&menu).Where(
			// 	`rest_id = ?`, rest_id).Count(&count)
			result = Database.Table(`rel_menus_dishes`).Select("dishes.id,dishes.name,dishes.category,dishes.unit,dishes.image,dishes.small_image,dishes.banner,dishes.taste,dishes.create_datetime,dishes.modify_datetime,dishes.Creator,dishes.Modifier,dishes.Index,dishes.top,dishes.status,dishes.desc").
				Joins("left join dishes on rel_menus_dishes.dishes_id = dishes.id").
				Where(`rel_menus_dishes.rest_id = ?`, rest_id).
				Count(&count)
		}
	}
	return count, result.Error
}

func UpdateDish(rest *entities.Dish) error {
	result := Database.Table("dishes").Where("id=?", rest.Id).UpdateColumns(&rest)
	return result.Error
}

// 设置菜品与配菜关联
func DishJoinInToppings(dish_id string, topping_ids []string) error {
	var rolePages []map[string]interface{}
	for _, topping_id := range topping_ids {
		rolePage := make(map[string]interface{})
		rolePage["dish_id"] = dish_id
		rolePage["topping_id"] = topping_id

		fmt.Println("DishJoinInToppings:dish_id" + dish_id)
		fmt.Println("DishJoinInToppings:topping_id" + topping_id)
		rolePages = append(rolePages, rolePage)
	}
	return Database.Table("rel_dishes_toppings").Create(&rolePages).Error
}

func ClearDishToppings(dish_id string) error {
	return Database.Exec("DELETE FROM rel_dishes_toppings WHERE dish_id = ?", dish_id).Error
}

// 获取 商家 + 菜单下所有菜品
func QueryFullDishes(rest_id string, menu_id string) ([]entities.Dish, error) {
	//var dish entities.Dish
	var dishes []entities.Dish
	var result *gorm.DB
	//selectFields := `id,name,category,unit,image,small_image,banner,taste,create_datetime,modify_datetime,Creator,Modifier,Index,top,status,dishes.desc `

	result = Database.Table(`rel_menus_dishes`).Select("dishes.id,dishes.name,dishes.category,dishes.unit,dishes.image,dishes.small_image,dishes.banner,dishes.taste,dishes.create_datetime,dishes.modify_datetime,dishes.Creator,dishes.Modifier,dishes.Index,dishes.top,dishes.status,dishes.desc").
		Joins("left join dishes on rel_menus_dishes.dishes_id = dishes.id").
		Where(`rel_menus_dishes.rest_id = ? and rel_menus_dishes.menu_id = ?`, rest_id, menu_id).
		Order("modify_datetime DESC").
		Find(&dishes)

	return dishes, result.Error
}
