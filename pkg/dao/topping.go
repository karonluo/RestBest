package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

// 创建配菜
func CreateTopping(topping entities.Topping) (string, error) {
	var result = Database.Create(&topping)
	return topping.Id, result.Error

}

// 删除配菜
func DeleteToppingById(id string) (bool, error) {
	var result bool = true

	res := Database.Exec("DELETE FROM toppings WHERE id=?", id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除配菜: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除配菜: %s 失败, 原因是: %s \r\n", id, "未找到该配菜")
	}
	return result, res.Error
}

/**/
// 分页查询 配菜
// dishes_id : 配菜父级的菜品ID
func QueryToppings(queryCondition entities.QueryCondition, dishes_id string) ([]entities.Topping, error) {
	var topping entities.Topping
	var toppings []entities.Topping
	var result *gorm.DB
	selectFields := `id,name,category,unit,image,small_image,banner,create_datetime,modify_datetime,Creator,Modifier,Index `
	if queryCondition.LikeValue != "" {
		if dishes_id == "" {
			result = Database.Model(&topping).Select(selectFields).Where(
				`name LIKE ? `,
				"%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&toppings)
		} else {
			// result = Database.Model(&topping).Select(selectFields).Where(
			// 	`name LIKE ? and dishes_id = ?`,
			// 	"%"+queryCondition.LikeValue+"%", dishes_id).
			// 	Order("modify_datetime DESC").
			// 	Limit(int(queryCondition.PageSize)).
			// 	Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			// 	Find(&toppings)

			result = Database.Table("rel_dishes_toppings").Select(` toppings.id,toppings.name,toppings.category,toppings.unit,toppings.image,toppings.small_image,toppings.banner,toppings.create_datetime,toppings.modify_datetime,toppings.Creator,toppings.Modifier,toppings.Index `).
				Joins(`left join toppings on toppings.id = rel_dishes_toppings.topping_id`).
				Where(`name LIKE ? and rel_dishes_toppings.dish_id = ?`, "%"+queryCondition.LikeValue+"%", dishes_id).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&toppings)
		}

	} else {
		if dishes_id == "" {
			result = Database.Model(&topping).
				Select(selectFields).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&toppings)
		} else {
			// result = Database.Model(&topping).
			// 	Select(selectFields).Where(
			// 	` dishes_id = ?`,
			// 	dishes_id).
			// 	Order("modify_datetime DESC").
			// 	Limit(int(queryCondition.PageSize)).
			// 	Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			// 	Find(&toppings)
			result = Database.Table("rel_dishes_toppings").Select(` toppings.id,toppings.name,toppings.category,toppings.unit,toppings.image,toppings.small_image,toppings.banner,toppings.create_datetime,toppings.modify_datetime,toppings.Creator,toppings.Modifier,toppings.Index `).
				Joins(`left join toppings on toppings.id = rel_dishes_toppings.topping_id`).
				Where(`rel_dishes_toppings.dish_id = ?`, dishes_id).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&toppings)

		}

	}

	return toppings, result.Error
}

// TODO 补充关联查询
func GetToppingsCount(queryCondition entities.QueryCondition, dishes_id string) (int64, error) {
	var count int64
	var topping entities.Topping
	var result *gorm.DB

	if queryCondition.LikeValue != "" {

		if dishes_id == "" {
			result = Database.Model(&topping).Where(
				`name LIKE ? `,
				"%"+queryCondition.LikeValue+"%").
				Count(&count)
		} else {
			// result = Database.Model(&topping).Where(
			// 	`name LIKE ? and dishes_id = ?`,
			// 	"%"+queryCondition.LikeValue+"%", dishes_id).
			// 	Count(&count)

			result = Database.Table("rel_dishes_toppings").
				Joins(`left join toppings on toppings.id = rel_dishes_toppings.topping_id`).
				Where(`name LIKE ? and rel_dishes_toppings.dish_id = ?`, "%"+queryCondition.LikeValue+"%", dishes_id).
				Count(&count)

		}

	} else {

		if dishes_id == "" {
			result = Database.Model(&topping).Count(&count)
		} else {
			// result = Database.Model(&topping).
			// 	Where(
			// 		` dishes_id = ?`,
			// 		dishes_id).
			// 	Count(&count)

			result = Database.Table("rel_dishes_toppings").
				Joins(`left join toppings on toppings.id = rel_dishes_toppings.topping_id`).
				Where(`rel_dishes_toppings.dish_id = ?`, dishes_id).
				Count(&count)

		}

	}
	return count, result.Error
}

func UpdateTopping(topping *entities.Topping) error {
	result := Database.Table("toppings").Where("id=?", topping.Id).UpdateColumns(&topping)
	return result.Error
}

func GetToppingById(Id string) (entities.Topping, bool, error) {
	var result entities.Topping
	var bres bool = false

	res := Database.Where(("id=?"), Id).First(&result)

	if res.RowsAffected == 1 {
		bres = true
	}

	return result, bres, res.Error

}
