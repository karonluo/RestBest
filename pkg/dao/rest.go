package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

func CreateRest(rest entities.Restaurant) error {
	result := Database.Create(&rest)
	return result.Error
}

func DeleteRestById(id string) (bool, error) {
	var result bool = true
	var menu entities.Menu
	menu.Id = id
	res := Database.Exec("DELETE FROM restaurants WHERE id=?", id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除商家: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除商家: %s 失败, 原因是: %s \r\n", id, "未找到该系统用户")
	}
	return result, res.Error
}

func DeleteRest(rest entities.Restaurant) (bool, error) {
	result := Database.Delete(&rest)
	var res bool = true
	if result.RowsAffected == 0 {
		res = false
	}
	return res, result.Error
}

func GetRestById(Id string) (entities.Restaurant, error) {
	var result entities.Restaurant
	err := Database.Where(("id=?"), Id).First(&result).Error
	return result, err
}

func QueryRests(queryCondition entities.QueryCondition) ([]entities.Restaurant, error) {
	var rest entities.Restaurant
	var rests []entities.Restaurant
	var result *gorm.DB
	selectFields := `id, name, create_datetime, modify_datetime, creator, modifier, certificate, certificate_desc, phone, address, service_charge, tax, status, start_business_hours,end_business_hours `
	if queryCondition.LikeValue != "" {

		result = Database.Model(&rest).Select(selectFields).Where(
			`name LIKE ? `,
			"%"+queryCondition.LikeValue+"%").
			Order("modify_datetime DESC").
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&rests)

	} else {

		result = Database.Model(&rest).
			Select(selectFields).Where(
			`name LIKE ? `,
			"%"+queryCondition.LikeValue+"%").
			Order("modify_datetime DESC").
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&rests)

	}

	return rests, result.Error
}

func GetRestsCount(queryCodition entities.QueryCondition) (int64, error) {
	var count int64
	var rest entities.Restaurant
	var result *gorm.DB

	if queryCodition.LikeValue != "" {

		result = Database.Model(&rest).Where(
			`name LIKE ?`,
			"%"+queryCodition.LikeValue+"%").Count(&count)

	} else {

		result = Database.Model(&rest).Count(&count)

	}
	return count, result.Error
}

func UpdateRest(rest *entities.Restaurant) error {
	result := Database.Table("restaurants").Where("id=?", rest.Id).UpdateColumns(&rest)
	return result.Error
}
