package dao

import (
	"RestBest/pkg/cache"
	"RestBest/pkg/entities"
	"context"
	"encoding/json"
	"fmt"

	"gorm.io/gorm"
)

func CreateSysUser(sysuser entities.SysUserForCreate) string {
	Database.Table("sys_users").Create(&sysuser)
	var sysRoleIds []string
	sysRoleIds = append(sysRoleIds, "e118134a-b513-4dc8-9b35-231fd3b42c2b")
	SysUserJoinInSysRoles(sysuser.Id, sysRoleIds)

	return sysuser.Id

}

func CheckHaveReSysUser(sysuser entities.SysUserForCreate) bool {
	var result []entities.SysUser
	var res bool = false
	Database.Raw(`SELECT login_name FROM sys_users WHERE cellphone=? OR email=? OR login_name=? OR id_card_number=? LIMIT 1;`,
		sysuser.Cellphone, sysuser.Email, sysuser.LoginName, sysuser.IDCardNumber).Scan(&result)
	if len(result) > 0 {
		res = true
	}
	return res
}

func EnumSysUserFromDB() ([]entities.SysUser, int64) {

	var sysusers []entities.SysUser
	result := Database.Find(&sysusers)
	return sysusers, result.RowsAffected
}

func GetUserFromRedisByLoginName(login_name string) (entities.SysUser, bool) {
	var sysuser entities.SysUser
	var result bool = true
	ctx := context.Background()
	res := cache.RedisDatabase.Get(ctx, "restbest_sysuser_"+login_name)
	if res != nil {
		err := json.Unmarshal([]byte(res.Val()), &sysuser)
		if err != nil {
			result = false

		}
	} else {
		result = false
	}
	return sysuser, result
}

func GetUserFromDBByLoginName(login_name string) (entities.SysUser, bool, error) {
	var result entities.SysUser
	var bres bool = false
	//res := Database.Where(("login_name=?"), login_name).First(&result)
	fmt.Println("GetUserFromDBByLoginName start ")
	res := Database.Raw(`
	select sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, 
	sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, 
	sys_users.id_card_number, sys_users.line, user_role,sys_users.passwd_md5,
'[' || string_agg( '{ "id":"' || restaurants.id ||'","name":"' || restaurants.name || '"}' ,',') ||  ']'  as restaurants,
	string_agg( restaurants.code ,',')  as rest_codes,
	string_agg( restaurants.name ,',')  as rest_names


from
(
	select sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, 
	sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, 
	sys_users.id_card_number, sys_users.line, sys_users.passwd_md5,
	'[' || string_agg( '{ \"id\":\"' || sys_roles.id ||'\",\"name\":\"' || sys_roles.name || '\"}' ,',') ||  ']'  as user_role
	
	from public.sys_users
	left join rel_users_roles on rel_users_roles.user_id = sys_users.id
	left join sys_roles on rel_users_roles.role_id = sys_roles.id
  
		  Where sys_users.login_name=?
		  Group by sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, sys_users.id_card_number, sys_users.line,sys_users.passwd_md5
		  ) as sys_users
		  
				  left join rel_tenantusers_restgroup on rel_tenantusers_restgroup.tenant_users_id = sys_users.id
				  left join rest_groups on rel_tenantusers_restgroup.rest_group_id = rest_groups.id
				  left join rel_rest_restgroup on rel_rest_restgroup.rest_group_id = rest_groups.id
				  left join restaurants on rel_rest_restgroup.rest_id = restaurants.id
				  
				  Group by sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, 
				  sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, 
				  sys_users.id_card_number, sys_users.line, user_role,sys_users.passwd_md5
		  
		  `, login_name).First(&result)

	if res.RowsAffected == 1 {
		bres = true
	}

	fmt.Println("GetUserFromDBByLoginName result ")
	fmt.Println(result.RestCodes)
	fmt.Println(result.Restaurants)
	fmt.Println(bres)

	return result, bres, res.Error
}

func GetSysUserById(Id string) (entities.SysUser, bool, error) {
	var result entities.SysUser
	var bres bool = false
	fmt.Println("&&&" + Id + "&&&")
	//res := Database.Where(("id=?"), Id).First(&result)

	res := Database.Table("public.sys_users").
		Select("sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, sys_users.id_card_number, sys_users.line , '[' || string_agg( '{ \"id\":\"' || sys_roles.id ||'\",\"name\":\"' || sys_roles.name || '\"}' ,',') ||  ']'  as user_role").
		Joins("left join rel_users_roles on rel_users_roles.user_id = sys_users.id").
		Joins("left join sys_roles on rel_users_roles.role_id = sys_roles.id").
		Where(("sys_users.id=?"), Id).
		Group("sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, sys_users.id_card_number, sys_users.line ").
		Order("modify_datetime DESC").
		First(&result)

	if res.RowsAffected == 1 {
		bres = true
	}

	return result, bres, res.Error

}

func DeleteSysUser1(id string) bool {
	var result bool = true
	var user entities.SysUser
	user.Id = id
	res := Database.Exec("DELETE FROM sys_users WHERE id=?", id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除系统用户: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除系统用户: %s 失败, 原因是: %s \r\n", id, "未找到该系统用户")
	}
	return result
}

func DeleteSysUsers(userIds []string) error {
	var user entities.SysUser
	result := Database.Delete(&user, userIds)
	return result.Error
}

func DeleteSysUser(compnay entities.SysUser) (bool, error) {
	result := Database.Delete(&compnay)
	var res bool = true
	if result.RowsAffected == 0 {
		res = false
	}
	return res, result.Error
}

func GetSysUserCount(queryCodition entities.QueryCondition, companyID string) (int64, error) {
	var count int64
	var user entities.SysUser
	var result *gorm.DB

	if queryCodition.LikeValue != "" {
		if companyID != "" {
			result = Database.Model(&user).Where(
				`(login_name LIKE ? OR
			display_name LIKE ? OR
			cellphone LIKE ? OR
			email LIKE ? OR
			wechat LIKE ? OR
			id_card_number LIKE ? OR
			line LIKE ? OR
			qq LIKE ?) AND sports_company_id=?`,
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				companyID).Count(&count)
		} else {
			result = Database.Model(&user).Where(
				`login_name LIKE ? OR
			display_name LIKE ? OR
			cellphone LIKE ? OR
			email LIKE ? OR
			wechat LIKE ? OR
			id_card_number LIKE ? OR
			line LIKE ? OR
			qq LIKE ?`,
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%",
				"%"+queryCodition.LikeValue+"%").Count(&count)
		}
	} else {
		if companyID != "" {
			result = Database.Model(&user).Where("sports_company_id", companyID).Count(&count)
		} else {
			result = Database.Model(&user).Count(&count)
		}
	}
	return count, result.Error
}

func QuerySysUsers(queryCondition entities.QueryCondition, companyID string) ([]entities.SysUser, error) {
	var user entities.SysUser
	var users []entities.SysUser
	var result *gorm.DB
	//fmt.Println("aaaaaaa")
	//fmt.Println(queryCondition.LikeValue)

	selectFields := `id,  id_card_number, login_name, display_name, cellphone,email, wechat, qq, Modifier, Creator, modify_datetime, create_datetime`
	if queryCondition.LikeValue != "" {
		if companyID != "" {
			result = Database.Model(&user).Select(selectFields).Where(
				`(login_name LIKE ? OR display_name LIKE ? OR cellphone LIKE ? OR email LIKE ? OR wechat LIKE ? OR
			id_card_number LIKE ? OR
			line LIKE ? OR
			qq LIKE ?) AND sports_company_id = ?`,
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%", companyID).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&users)

		} else {
			// result = Database.Model(&user).Select(selectFields).Where(
			// 	`login_name LIKE ? OR display_name LIKE ? OR cellphone LIKE ? OR email LIKE ? OR wechat LIKE ? OR
			// id_card_number LIKE ? OR
			// line LIKE ? OR
			// qq LIKE ?`,
			// 	"%"+queryCondition.LikeValue+"%",
			// 	"%"+queryCondition.LikeValue+"%",
			// 	"%"+queryCondition.LikeValue+"%",
			// 	"%"+queryCondition.LikeValue+"%",
			// 	"%"+queryCondition.LikeValue+"%",
			// 	"%"+queryCondition.LikeValue+"%",
			// 	"%"+queryCondition.LikeValue+"%",
			// 	"%"+queryCondition.LikeValue+"%").
			// 	Order("modify_datetime DESC").
			// 	Limit(int(queryCondition.PageSize)).
			// 	Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			// 	Find(&users)
			fmt.Println(queryCondition.LikeValue)
			result = Database.Table("public.sys_users").
				//Select("sys_users.*, concat_ws(':', sys_roles.name) as user_role").
				Select("sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, sys_users.id_card_number, sys_users.line , '[' || string_agg( '{ \"id\":\"' || sys_roles.id ||'\",\"name\":\"' || sys_roles.name || '\"}' ,',') ||  ']'  as user_role").
				Joins("left join rel_users_roles on rel_users_roles.user_id = sys_users.id").
				Joins("left join sys_roles on rel_users_roles.role_id = sys_roles.id").
				Where(
					`login_name LIKE ? OR display_name LIKE ? OR cellphone LIKE ? OR email LIKE ? OR wechat LIKE ? OR
			id_card_number LIKE ? OR
			line LIKE ? OR
			qq LIKE ?`,
					"%"+queryCondition.LikeValue+"%",
					"%"+queryCondition.LikeValue+"%",
					"%"+queryCondition.LikeValue+"%",
					"%"+queryCondition.LikeValue+"%",
					"%"+queryCondition.LikeValue+"%",
					"%"+queryCondition.LikeValue+"%",
					"%"+queryCondition.LikeValue+"%",
					"%"+queryCondition.LikeValue+"%").
				Group("sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, sys_users.id_card_number, sys_users.line ").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Scan(&users)

		}

	} else {
		if companyID == "" {

			// result = Database.Model(&user).
			// 	Select(selectFields).
			// 	Order("modify_datetime DESC").
			// 	Limit(int(queryCondition.PageSize)).
			// 	Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			// 	Find(&users)

			result = Database.Table("public.sys_users").
				//Select("sys_users.*, concat_ws(':', sys_roles.name) as user_role").
				Select("sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, sys_users.id_card_number, sys_users.line , '[' || string_agg( '{ \"id\":\"' || sys_roles.id ||'\",\"name\":\"' || sys_roles.name || '\"}' ,',') ||  ']'  as user_role").
				Joins("left join rel_users_roles on rel_users_roles.user_id = sys_users.id").
				Joins("left join sys_roles on rel_users_roles.role_id = sys_roles.id").
				Group("sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, sys_users.id_card_number, sys_users.line ").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&users)

		} else {
			// result = Database.Model(&user).
			// 	Select(selectFields).Where("sports_company_id=?", companyID).
			// 	Order("modify_datetime DESC").
			// 	Limit(int(queryCondition.PageSize)).
			// 	Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			// 	Find(&users)

			result = Database.Table("public.sys_users").
				//Select("sys_users.*, concat_ws(':', sys_roles.name) as user_role").
				Select("sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, sys_users.id_card_number, sys_users.line , '[' || string_agg( '{ \"id\":\"' || sys_roles.id ||'\",\"name\":\"' || sys_roles.name || '\"}' ,',') ||  ']'  as user_role").
				Joins("left join rel_users_roles on rel_users_roles.user_id = sys_users.id").
				Joins("left join sys_roles on rel_users_roles.role_id = sys_roles.id").
				Where("sports_company_id=?", companyID).
				Group("sys_users.id, sys_users.login_name, sys_users.display_name, sys_users.cellphone, sys_users.email, sys_users.wechat, sys_users.qq, sys_users.is_disable_login, sys_users.create_datetime, sys_users.modify_datetime, sys_users.creator, sys_users.modifier, sys_users.id_card_number, sys_users.line ").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&users)

		}
	}

	return users, result.Error
}

func GetSysUserFromDBById(sysUserId string) (entities.SysUser, error) {
	var user entities.SysUser
	result := Database.Table("sys_users").Where("id=?", sysUserId).First(&user)
	return user, result.Error

}
func UpdateSysUserPassword(sysUserId string, newPassword string) error {
	result := Database.Table("sys_users").Where("id=?", sysUserId).UpdateColumn("passwd_md5", newPassword)
	return result.Error
}
func UpdateSysUser(user entities.SysUser) error {
	result := Database.Table("sys_users").Where("id=?", user.Id).UpdateColumns(&user)
	return result.Error
}

func UpdateSysUserCompanyName(coId string, coName string) error {
	result := Database.Table("sys_users").
		Where("sports_company_id=?", coId).
		UpdateColumn("sports_company_name", coName)

	return result.Error
}

// 通过体育公司唯一编号集合获取其所有下属系统用户
func EnumSysUsersFromSportsCompanyIds(siteIds []string) ([]entities.SysUser, error) {
	var users []entities.SysUser
	selectFields := `id, sports_company_name, id_card_number, login_name, display_name, cellphone,email, wechat, qq, Modifier, Creator, modify_datetime, create_datetime`
	result := Database.Table("sys_users").Select(selectFields).Order("modify_datetime DESC, display_name ASC").
		Where("sports_company_id in ?", siteIds).Find(&users)
	return users, result.Error
}

func ClearSysUserSysRoles(sysUserId string) error {
	return Database.Exec("DELETE FROM rel_users_roles WHERE user_id = ?", sysUserId).Error
}

// 将系统角色和系统用户进行绑定
func SysUserJoinInSysRoles(sysUserId string, sysRoleIds []string) error {
	var rolePages []map[string]interface{}

	//var err error
	for _, sysRoleId := range sysRoleIds {
		fmt.Println("sysRoleId")
		fmt.Println(sysRoleId)
		fmt.Println("sysUserId")
		fmt.Println(sysUserId)

		rolePage := make(map[string]interface{})
		rolePage["user_id"] = sysUserId
		rolePage["role_id"] = sysRoleId
		rolePages = append(rolePages, rolePage)

		// err = Database.Table("rel_users_roles").Create(&rolePage).Error

		// if err != nil {
		// 	continue
		// }

	}
	return Database.Table("rel_users_roles").Create(&rolePages).Error
	//return err

}
