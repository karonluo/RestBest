package dao

import (
	"RestBest/pkg/cache"
	"RestBest/pkg/entities"
	"context"
	"encoding/json"
	"fmt"

	"gorm.io/gorm"
)

func GetTenantUserFromRedisByLoginName(login_name string) (entities.TenantUser, bool) {
	var sysuser entities.TenantUser
	var result bool = true
	ctx := context.Background()
	res := cache.RedisDatabase.Get(ctx, "restbest_tenantusers_"+login_name)
	if res != nil {
		err := json.Unmarshal([]byte(res.Val()), &sysuser)
		if err != nil {
			result = false

		}
	} else {
		result = false
	}
	return sysuser, result
}

func CheckHaveReTenantUser(sysuser entities.TenantUser) bool {
	var result []entities.TenantUser
	var res bool = false
	Database.Raw(`SELECT login_name FROM tenant_users WHERE cellphone=? OR email=? OR login_name=? OR id_card_number=? LIMIT 1;`,
		sysuser.Cellphone, sysuser.Email, sysuser.LoginName, sysuser.IDCardNumber).Scan(&result)
	if len(result) > 0 {
		res = true
	}
	return res
}

func CreateTenantUser(tenantUser entities.TenantUser) string {
	Database.Create(&tenantUser)
	return tenantUser.Id

}

func GetTenantUserByLoginName(login_name string) (entities.TenantUser, bool, error) {
	var result entities.TenantUser
	var bres bool = false
	res := Database.Where(("login_name=?"), login_name).First(&result)

	if res.RowsAffected == 1 {
		bres = true
	}
	return result, bres, res.Error
}

func GetTenantUserById(Id string) (entities.TenantUser, bool, error) {
	var result entities.TenantUser
	var bres bool = false
	fmt.Println("&&&" + Id + "&&&")
	res := Database.Where(("id=?"), Id).First(&result)

	if res.RowsAffected == 1 {
		bres = true
	}

	return result, bres, res.Error

}

func DeleteTenantUserById(id string) bool {
	var result bool = true
	var user entities.TenantUser
	user.Id = id
	res := Database.Exec("DELETE FROM tenant_users WHERE id=?", id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除商户用户: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除商户用户: %s 失败, 原因是: %s \r\n", id, "未找到该商户用户")
	}
	return result
}

func DeleteTenantUsers(userIds []string) error {
	var user entities.TenantUser
	result := Database.Delete(&user, userIds)
	return result.Error
}

func DeleteTenantUser(compnay entities.TenantUser) (bool, error) {
	result := Database.Delete(&compnay)
	var res bool = true
	if result.RowsAffected == 0 {
		res = false
	}
	return res, result.Error
}

func GetTenantUserCount(queryCodition entities.QueryCondition) (int64, error) {
	var count int64
	var user entities.TenantUser
	var result *gorm.DB

	if queryCodition.LikeValue != "" {

		result = Database.Model(&user).Where(
			`login_name LIKE ? OR
			display_name LIKE ? OR
			cellphone LIKE ? OR
			email LIKE ? OR
			wechat LIKE ? OR
			id_card_number LIKE ? OR
			line LIKE ? OR
			qq LIKE ?`,
			"%"+queryCodition.LikeValue+"%",
			"%"+queryCodition.LikeValue+"%",
			"%"+queryCodition.LikeValue+"%",
			"%"+queryCodition.LikeValue+"%",
			"%"+queryCodition.LikeValue+"%",
			"%"+queryCodition.LikeValue+"%",
			"%"+queryCodition.LikeValue+"%",
			"%"+queryCodition.LikeValue+"%").Count(&count)

	} else {

		result = Database.Model(&user).Count(&count)

	}
	return count, result.Error
}

func QueryTenantUsers(queryCondition entities.QueryCondition) ([]entities.TenantUser, error) {
	var user entities.TenantUser
	var users []entities.TenantUser
	var result *gorm.DB
	//fmt.Println("aaaaaaa")
	//fmt.Println(queryCondition.LikeValue)

	selectFields := `id, id_card_number, login_name, display_name, cellphone,email, wechat, qq, "Creator", "Modifier", modify_datetime, create_datetime`
	if queryCondition.LikeValue != "" {

		result = Database.Model(&user).Select(selectFields).Where(
			`login_name LIKE ? OR display_name LIKE ? OR cellphone LIKE ? OR email LIKE ? OR wechat LIKE ? OR
			id_card_number LIKE ? OR
			line LIKE ? OR
			qq LIKE ?`,
			"%"+queryCondition.LikeValue+"%",
			"%"+queryCondition.LikeValue+"%",
			"%"+queryCondition.LikeValue+"%",
			"%"+queryCondition.LikeValue+"%",
			"%"+queryCondition.LikeValue+"%",
			"%"+queryCondition.LikeValue+"%",
			"%"+queryCondition.LikeValue+"%",
			"%"+queryCondition.LikeValue+"%").
			Order("modify_datetime DESC").
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&users)

	} else {

		result = Database.Model(&user).
			Select(selectFields).
			Order("modify_datetime DESC").
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&users)

	}

	return users, result.Error
}

func UpdateTenantUserPassword(sysUserId string, newPassword string) error {
	result := Database.Table("tenant_users").Where("id=?", sysUserId).UpdateColumn("passwd_md5", newPassword)
	return result.Error
}
func UpdateTenantUser(user entities.TenantUser) error {
	result := Database.Table("tenant_users").Where("id=?", user.Id).UpdateColumns(&user)
	return result.Error
}

// func UpdateSysUserCompanyName(coId string, coName string) error {
// 	result := Database.Table("sys_users").
// 		Where("sports_company_id=?", coId).
// 		UpdateColumn("sports_company_name", coName)

// 	return result.Error
// }

// // 通过体育公司唯一编号集合获取其所有下属系统用户
// func EnumSysUsersFromSportsCompanyIds(siteIds []string) ([]entities.SysUser, error) {
// 	var users []entities.SysUser
// 	selectFields := `id, sports_company_name, id_card_number, login_name, display_name, cellphone,email, wechat, qq, Modifier, Creator, modify_datetime, create_datetime`
// 	result := Database.Table("sys_users").Select(selectFields).Order("modify_datetime DESC, display_name ASC").
// 		Where("sports_company_id in ?", siteIds).Find(&users)
// 	return users, result.Error
// }

// func ClearSysUserSysRoles(sysUserId string) error {
// 	return Database.Exec("DELETE FROM rel_users_roles WHERE user_id = ?", sysUserId).Error
// }

// // 将系统角色和系统用户进行绑定
// func SysUserJoinInSysRoles(sysUserId string, sysRoleIds []string) error {
// 	var rolePages []map[string]interface{}

// 	//var err error
// 	for _, sysRoleId := range sysRoleIds {
// 		fmt.Println("sysRoleId")
// 		fmt.Println(sysRoleId)
// 		fmt.Println("sysUserId")
// 		fmt.Println(sysUserId)

// 		rolePage := make(map[string]interface{})
// 		rolePage["user_id"] = sysUserId
// 		rolePage["role_id"] = sysRoleId
// 		rolePages = append(rolePages, rolePage)

// 		// err = Database.Table("rel_users_roles").Create(&rolePage).Error

// 		// if err != nil {
// 		// 	continue
// 		// }

// 	}
// 	return Database.Table("rel_users_roles").Create(&rolePages).Error
// 	//return err

// }
