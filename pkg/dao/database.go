package dao

import (
	"RestBest/conf"
	"RestBest/pkg/entities"
	"fmt"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Database *gorm.DB // globle指针

func InitDatabase() bool {
	var result bool
	if Database == nil {
		fmt.Print("数据库初始化")
		dbconf := conf.WebConfiguration.DBConf
		dsn := "host=" + dbconf.Host + " user=" + dbconf.User + " password=" + dbconf.Password + " dbname=" + dbconf.DBName + " port=" + dbconf.Port + " sslmode=disable TimeZone=Asia/Shanghai"
		db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
		if err == nil {

			sql, _ := db.DB()
			sql.SetConnMaxIdleTime(time.Hour)
			sql.SetConnMaxLifetime(24 * time.Hour)
			sql.SetMaxIdleConns(1000)
			sql.SetMaxOpenConns(100000)
			Database = db
			fmt.Println("......成功")
			result = true
			var menu entities.Menu
			Database.AutoMigrate(&menu)
		} else {
			fmt.Println("......失败")
			result = false

		}

	}
	return result
}
