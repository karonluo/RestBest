package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

func CreateTable(table entities.Table) string {
	Database.Create(&table)
	return table.Id

}

func GetTableById(Id string) (entities.Table, bool, error) {
	var result entities.Table
	var bres bool = false

	res := Database.Where(("id=?"), Id).First(&result)

	if res.RowsAffected == 1 {
		bres = true
	}

	return result, bres, res.Error

}

func DeleteTableById(id string) (bool, error) {
	// var result bool = true
	// var user entities.Table
	// user.Id = id
	// res := Database.Exec("DELETE FROM tables WHERE id=?", id)
	// if res.Error != nil {
	// 	result = false
	// 	fmt.Printf("删除商户餐桌: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	// }
	// if res.RowsAffected == 0 {
	// 	result = false
	// 	fmt.Printf("删除商户餐桌: %s 失败, 原因是: %s \r\n", id, "未找到该商户餐桌")
	// }
	// return result
	var user entities.Table
	result := Database.Delete(&user, id)

	return true, result.Error
}

/*
查询餐桌数据总数
rest_id： 用于查询商家下所有餐桌列表
*/
func GetTableCount(queryCondition entities.QueryCondition, rest_id string) (int64, error) {
	var count int64
	var table entities.Table
	var result *gorm.DB

	if queryCondition.LikeValue != "" {
		if rest_id != "" {

			result = Database.Model(&table).Where(
				`(Code LIKE ? or Name like ?) and rest_id = ?`,
				"%"+queryCondition.LikeValue+"%", "%"+queryCondition.LikeValue+"%", rest_id).Count(&count)
		} else {

			fmt.Println("QueryTables2")
			fmt.Println(queryCondition.LikeValue + "||" + rest_id)

			result = Database.Model(&table).Where(
				`Code LIKE ? or Name like ?`,
				"%"+queryCondition.LikeValue+"%", "%"+queryCondition.LikeValue+"%").Count(&count)
		}

	} else {
		if rest_id != "" {
			result = Database.Model(&table).
				Where(
					`rest_id = ?`,
					rest_id).
				Count(&count)
		} else {
			result = Database.Model(&table).Count(&count)
		}

	}
	return count, result.Error
}

/*
分页查询餐桌表
rest_id： 用于查询商家下所有餐桌列表
*/
func QueryTables(queryCondition entities.QueryCondition, rest_id string) ([]entities.Table, error) {
	var table entities.Table
	var tables []entities.Table
	var result *gorm.DB

	selectFields := `id, name,rest_id, hall_id, room_id, image_url ,status, category, code, "Creator", "Modifier", modify_datetime, create_datetime`
	if queryCondition.LikeValue != "" {

		if rest_id != "" {
			result = Database.Model(&table).Select(selectFields).Where(
				`(Code LIKE ? or Name like ?) and rest_id = ?`,
				"%"+queryCondition.LikeValue+"%", "%"+queryCondition.LikeValue+"%", rest_id).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&tables)
		} else {
			result = Database.Model(&table).Select(selectFields).Where(
				`Code LIKE ? or Name LIKE ? `,
				"%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&tables)
		}

	} else {
		if rest_id != "" {

			result = Database.Table("tables").
				Select(selectFields).
				Where(`rest_id = ?`, rest_id).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&tables)
		} else {
			result = Database.Model(&table).
				Select(selectFields).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&tables)
		}
	}

	return tables, result.Error
}

func UpdateTable(table entities.Table) error {
	result := Database.Table("tables").Where("id=?", table.Id).UpdateColumns(&table)
	return result.Error
}
