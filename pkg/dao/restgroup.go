package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

func CreateRestGroup(rest entities.RestGroup) error {
	result := Database.Create(&rest)
	return result.Error
}

func DeleteRestGroupById(id string) (bool, error) {
	var result bool = true
	var menu entities.Menu
	menu.Id = id
	res := Database.Exec("DELETE FROM rest_groups WHERE id=?", id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除商家组: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除商家组: %s 失败, 原因是: %s \r\n", id, "未找到该商家组")
	}
	return result, res.Error
}

func GetRestGroupByIdSimple(Id string) (entities.RestGroup, error) {
	var result entities.RestGroup
	err := Database.Where(("id=?"), Id).First(&result).Error
	return result, err
}

func GetRestGroupById(Id string) (entities.RestGroupForQuery, error) {
	var result entities.RestGroupForQuery

	//err := Database.Where(("id=?"), Id).First(&result).Error
	// 原始SQL
	// SELECT rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier,
	// rest_groups.status,rest_groups.parent_id,rest_groups.haschild,
	// '[' || string_agg( '{ \"id\":\"' || tenant_users.id ||'\",\"name\":\"' || tenant_users.login_name || '\"}' ,',') ||  ']'  as tenantUsers,
	// '[' || string_agg( '{ \"id\":\"' || restaurants.id  ||'\",\"name\":\"' || restaurants.name        || '\"}' ,',') ||  ']'  as restaurants
	// FROM public.rest_groups
	// left join rel_tenantusers_restgroup on rest_groups.id = rel_tenantusers_restgroup.rest_group_id
	// left join tenant_users on tenant_users.id =  rel_tenantusers_restgroup.tenant_users_id
	// left join rel_rest_restgroup on rest_groups.id = rel_rest_restgroup.rest_group_id
	// left join restaurants on restaurants.id =  rel_rest_restgroup.rest_id
	// group by rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier,
	// rest_groups.status,rest_groups.parent_id,rest_groups.haschild
	err := Database.Table("public.rest_groups").
		Select("rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier,rest_groups.status,rest_groups.parent_id,rest_groups.haschild,'[' || string_agg( '{ \"id\":\"' || tenant_users.id ||'\",\"name\":\"' || tenant_users.login_name || '\"}' ,',') ||  ']'  as tenantusers,'[' || string_agg( '{ \"id\":\"' || restaurants.id ||'\",\"name\":\"' || restaurants.name || '\"}' ,',') ||  ']'  as restaurants").
		Joins("left join rel_tenantusers_restgroup on rest_groups.id = rel_tenantusers_restgroup.rest_group_id").
		Joins(" left join tenant_users on tenant_users.id =  rel_tenantusers_restgroup.tenant_users_id").
		Joins("left join rel_rest_restgroup on rest_groups.id = rel_rest_restgroup.rest_group_id").
		Joins(" left join restaurants on restaurants.id =  rel_rest_restgroup.rest_id").
		Where(("rest_groups.id=?"), Id).
		Group("rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier, rest_groups.status,rest_groups.parent_id,rest_groups.haschild").
		First(&result).Error

	return result, err
}

func QueryRestGroups(queryCondition entities.QueryCondition) ([]entities.RestGroupForQuery, error) {
	//var rest entities.RestGroupForQuery
	var rests []entities.RestGroupForQuery
	var result *gorm.DB

	if queryCondition.LikeValue != "" {

		result = Database.Raw(`
					select  rest_groups2.id, rest_groups2.group_name,rest_groups2.modify_datetime,rest_groups2.creator,rest_groups2.modifier,
							rest_groups2.status,rest_groups2.parent_id,rest_groups2.haschild,rest_groups2.tenantusers,
					'[' || string_agg( '{ "id":"' || restaurants.id ||'","name":"' || restaurants.name || '"}' ,',') ||  ']'  as restaurants
					from
					(
						Select rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier,
						rest_groups.status,rest_groups.parent_id,rest_groups.haschild,
						'[' || string_agg( '{ "id":"' || tenant_users.id ||'","name":"' || tenant_users.login_name || '"}' ,',') ||  ']'  as tenantusers
						from public.rest_groups
						left join rel_tenantusers_restgroup on rest_groups.id = rel_tenantusers_restgroup.rest_group_id
						left join sys_users as tenant_users on tenant_users.id =  rel_tenantusers_restgroup.tenant_users_id
						Group by rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier, rest_groups.status,rest_groups.parent_id,rest_groups.haschild
						Order by modify_datetime DESC
					) as rest_groups2
					left join rel_rest_restgroup on rest_groups2.id = rel_rest_restgroup.rest_group_id
					left join restaurants on restaurants.id =  rel_rest_restgroup.rest_id
					where group_name LIKE ? 
					Group by rest_groups2.id, rest_groups2.group_name,rest_groups2.modify_datetime,rest_groups2.creator,rest_groups2.modifier,
							rest_groups2.status,rest_groups2.parent_id,rest_groups2.haschild,rest_groups2.tenantusers
					Order by modify_datetime DESC`, "%"+queryCondition.LikeValue+"%").
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&rests)

	} else {

		result = Database.Raw(`
					select  rest_groups2.id, rest_groups2.group_name,rest_groups2.modify_datetime,rest_groups2.creator,rest_groups2.modifier,
							rest_groups2.status,rest_groups2.parent_id,rest_groups2.haschild,rest_groups2.tenantusers,
							'[' || string_agg( '{ "id":"' || restaurants.id ||'","name":"' || restaurants.name || '"}' ,',') ||  ']'  as restaurants
					from
					(
						Select rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier,
						rest_groups.status,rest_groups.parent_id,rest_groups.haschild,
						'[' || string_agg( '{ "id":"' || tenant_users.id ||'","name":"' || tenant_users.login_name || '"}' ,',') ||  ']'  as tenantusers
						from public.rest_groups
						left join rel_tenantusers_restgroup on rest_groups.id = rel_tenantusers_restgroup.rest_group_id
						left join sys_users as tenant_users on tenant_users.id =  rel_tenantusers_restgroup.tenant_users_id
						Group by rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier, rest_groups.status,rest_groups.parent_id,rest_groups.haschild
						Order by modify_datetime DESC
					) as rest_groups2
					left join rel_rest_restgroup on rest_groups2.id = rel_rest_restgroup.rest_group_id
					left join restaurants on restaurants.id =  rel_rest_restgroup.rest_id
					Group by rest_groups2.id, rest_groups2.group_name,rest_groups2.modify_datetime,rest_groups2.creator,rest_groups2.modifier,
							rest_groups2.status,rest_groups2.parent_id,rest_groups2.haschild,rest_groups2.tenantusers
					Order by modify_datetime DESC`).
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&rests)
	}

	// for _, rest := range rests {

	// 	fmt.Println(rest.Restaurants)

	// 	rest.Restaurants = strings.Replace(rest.Restaurants, `///`, `/`, -1)
	// 	rest.Tenantusers = strings.Replace(rest.Tenantusers, `///`, `/`, -1)

	// }

	return rests, result.Error
}

// left join
func QueryRestGroups2(queryCondition entities.QueryCondition) ([]entities.RestGroupForQuery, error) {
	//var rest entities.RestGroupForQuery
	var rests []entities.RestGroupForQuery
	var result *gorm.DB
	//selectFields := `id, group_name, create_datetime, modify_datetime, creator, modifier`
	if queryCondition.LikeValue != "" {

		// result = Database.Table("public.rest_groups").Select(selectFields).Where(
		// 	`group_name LIKE ? `,
		// 	"%"+queryCondition.LikeValue+"%").
		// 	Order("modify_datetime DESC").
		// 	Limit(int(queryCondition.PageSize)).
		// 	Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
		// 	Find(&rests)
		result = Database.Table("public.rest_groups").
			Select("rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier,rest_groups.status,rest_groups.parent_id,rest_groups.haschild,'[' || string_agg( '{ \"id\":\"' || tenant_users.id ||'\",\"name\":\"' || tenant_users.login_name || '\"}' ,',') ||  ']'  as tenantusers,'[' || string_agg( '{ \"id\":\"' || restaurants.id ||'\",\"name\":\"' || restaurants.name || '\"}' ,',') ||  ']'  as restaurants").
			Joins("left join rel_tenantusers_restgroup on rest_groups.id = rel_tenantusers_restgroup.rest_group_id").
			Joins(" left join tenant_users on tenant_users.id =  rel_tenantusers_restgroup.tenant_users_id").
			Joins("left join rel_rest_restgroup on rest_groups.id = rel_rest_restgroup.rest_group_id").
			Joins(" left join restaurants on restaurants.id =  rel_rest_restgroup.rest_id").
			Where(`group_name LIKE ? `, "%"+queryCondition.LikeValue+"%").
			Order("modify_datetime DESC").
			Group("rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier, rest_groups.status,rest_groups.parent_id,rest_groups.haschild").
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&rests)

	} else {

		// Select rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier,rest_groups.status,rest_groups.parent_id,rest_groups.haschild,'[' || string_agg( '{ \"id\":\"' || tenant_users.id ||'\",\"name\":\"' || tenant_users.login_name || '\"}' ,',') ||  ']'  as tenantusers,'[' || string_agg( '{ \"id\":\"' || restaurants.id ||'\",\"name\":\"' || restaurants.name || '\"}' ,',') ||  ']'  as restaurants
		// from public.rest_groups
		// left join rel_tenantusers_restgroup on rest_groups.id = rel_tenantusers_restgroup.rest_group_id
		// left join tenant_users on tenant_users.id =  rel_tenantusers_restgroup.tenant_users_id
		// left join rel_rest_restgroup on rest_groups.id = rel_rest_restgroup.rest_group_id
		// left join restaurants on restaurants.id =  rel_rest_restgroup.rest_id
		// Group by rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier, rest_groups.status,rest_groups.parent_id,rest_groups.haschild
		// Order by modify_datetime DESC

		result = Database.Table("public.rest_groups").
			Select("rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier,rest_groups.status,rest_groups.parent_id,rest_groups.haschild,'[' || string_agg( '{ \"id\":\"' || tenant_users.id ||'\",\"name\":\"' || tenant_users.login_name || '\"}' ,',') ||  ']'  as tenantusers,'[' || string_agg( '{ \"id\":\"' || restaurants.id ||'\",\"name\":\"' || restaurants.name || '\"}' ,',') ||  ']'  as restaurants").
			Joins("left join rel_tenantusers_restgroup on rest_groups.id = rel_tenantusers_restgroup.rest_group_id").
			Joins(" left join tenant_users on tenant_users.id =  rel_tenantusers_restgroup.tenant_users_id").
			Joins("left join rel_rest_restgroup on rest_groups.id = rel_rest_restgroup.rest_group_id").
			Joins(" left join restaurants on restaurants.id =  rel_rest_restgroup.rest_id").
			Order("modify_datetime DESC").
			Group("rest_groups.id, rest_groups.group_name,rest_groups.modify_datetime,rest_groups.creator,rest_groups.modifier, rest_groups.status,rest_groups.parent_id,rest_groups.haschild").
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&rests)
	}

	return rests, result.Error
}

func GetRestsGroupCount(queryCodition entities.QueryCondition) (int64, error) {
	var count int64
	var rest entities.RestGroup
	var result *gorm.DB

	if queryCodition.LikeValue != "" {

		result = Database.Model(&rest).Where(
			`name LIKE ?`,
			"%"+queryCodition.LikeValue+"%").Count(&count)

	} else {

		result = Database.Model(&rest).Count(&count)

	}
	return count, result.Error
}

func UpdateRestGroup(rest *entities.RestGroup) error {
	result := Database.Table("rest_groups").Where("id=?", rest.Id).UpdateColumns(&rest)
	return result.Error
}

// 设置商家组商家关系
func RestGroupJoinInRests(restGroupId string, restIds []string) error {
	var rolePages []map[string]interface{}
	for _, restId := range restIds {
		rolePage := make(map[string]interface{})
		rolePage["rest_group_id"] = restGroupId
		rolePage["rest_id"] = restId
		rolePages = append(rolePages, rolePage)
	}
	return Database.Table("rel_rest_restgroup").Create(&rolePages).Error
}

func ClearRestGroupRests(restGroupId string) error {
	return Database.Exec("DELETE FROM rel_rest_restgroup WHERE rest_group_id = ?", restGroupId).Error
}

// 设置商家组商家关系
func RestGroupJoinInTenantusers(restGroupId string, tenantUserIds []string) error {
	var rolePages []map[string]interface{}
	for _, tenantUserId := range tenantUserIds {
		rolePage := make(map[string]interface{})
		rolePage["rest_group_id"] = restGroupId
		rolePage["tenant_users_id"] = tenantUserId
		rolePages = append(rolePages, rolePage)
	}
	return Database.Table("rel_tenantusers_restgroup").Create(&rolePages).Error
}

func ClearRestGroupTenantusers(restGroupId string) error {
	return Database.Exec("DELETE FROM rel_tenantusers_restgroup WHERE rest_group_id = ?", restGroupId).Error
}

func GetChildRestGroup(Id string) (int64, error) {
	var count int64
	var user entities.RestGroup

	result := Database.Model(&user).Where(("parent_id=?"), Id).Count(&count)

	return count, result.Error
}

func GetRestGroupByParentId(ParentId string) ([]entities.RestGroup, error) {
	var result []entities.RestGroup

	err := Database.Where(("parent_id=?"), ParentId).Find(&result).Error

	return result, err
}

func UpdateRestGroupHasChild(Id string, hasChild bool) error {
	result := Database.Table("rest_groups").Where("id=?", Id).Update("haschild", hasChild)
	return result.Error
}

func QueryRestGroupRests(id string) ([]entities.Restaurant, error) {
	//var rest entities.RestGroupForQuery
	var rests []entities.Restaurant
	var result *gorm.DB

	// 	SELECT restaurants.*
	// FROM rel_rest_restgroup
	// left join restaurants on restaurants.id = rel_rest_restgroup.rest_id
	// where rest_group_id = 'a81a57f6-6c9b-490b-a742-1b17a8401123' and restaurants.id is not null
	// ORDER BY id ASC

	fmt.Println(id)

	result = Database.Table("rel_rest_restgroup").
		Select(" restaurants.* ").
		Joins("left join restaurants on restaurants.id = rel_rest_restgroup.rest_id").
		Where("rest_group_id=? and restaurants.id is not null", id).
		Order(" modify_datetime ASC ").
		Find(&rests)

	return rests, result.Error
}
