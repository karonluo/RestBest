package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

func CreateSysDict(sysdict entities.SystemDict) error {
	result := Database.Create(&sysdict)
	return result.Error
}

func DeleteSysDict(systemDict entities.SystemDict) (bool, error) {
	result := Database.Delete(&systemDict)
	var res bool = true
	if result.RowsAffected == 0 {
		res = false
	}
	return res, result.Error
}

func DeleteSysDicts(sysDictCodes []string) error {
	var sysdict entities.SystemDict
	result := Database.Delete(&sysdict, sysDictCodes)
	return result.Error
}

func GetSysDictByCode(code string) (entities.SystemDict, error) {
	var result entities.SystemDict
	err := Database.Where(("code=?"), code).First(&result).Error
	return result, err
}

func GetSysDictCount(queryCondition entities.QueryCondition) (int64, error) {
	var count int64
	var sysdict entities.SystemDict
	var result *gorm.DB

	if queryCondition.LikeValue != "" {

		result = Database.Model(&sysdict).Where(
			`code LIKE ? OR
			value LIKE ? OR
			key LIKE ? OR
			parent_code LIKE ? OR
			data_type LIKE ? `,
			"%"+queryCondition.LikeValue+"%",
			"%"+queryCondition.LikeValue+"%",
			"%"+queryCondition.LikeValue+"%",
			"%"+queryCondition.LikeValue+"%",
			"%"+queryCondition.LikeValue+"%").Count(&count)

	} else {

		result = Database.Model(&sysdict).Count(&count)

	}
	return count, result.Error
}

// 默认为空的情况下，只显示 parent_code = null的父节点列表，给parent_code赋值后可以获取子节点
func QuerySysDicts(queryCondition entities.QueryCondition, parent_code string) ([]entities.SystemDict, error) {
	var sysdict entities.SystemDict
	var sysdicts []entities.SystemDict
	var result *gorm.DB

	fmt.Println("QuerySysDicts:" + queryCondition.LikeValue)
	fmt.Println("parent_code:" + parent_code)
	selectFields := `code,  key, value, parent_code, data_type,top_code, Modifier, Creator, modify_datetime, create_datetime`
	if queryCondition.LikeValue != "" {
		if parent_code == "" {
			result = Database.Model(&sysdict).Select(selectFields).Where(
				`code LIKE ? OR
				value LIKE ? OR
				key LIKE ? OR
				data_type LIKE ? `,
				"%"+parent_code+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&sysdicts)
		} else {
			result = Database.Model(&sysdict).Select(selectFields).Where(
				`parent_code = ? AND (
				code LIKE ? OR
				value LIKE ? OR
				key LIKE ? OR
				data_type LIKE ? )`,
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&sysdicts)

		}

	} else {
		if parent_code == "" {
			result = Database.Model(&sysdict).
				Select(selectFields).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&sysdicts)

		} else {
			result = Database.Model(&sysdict).
				Select(selectFields).
				Where(`parent_code = ? `, parent_code).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&sysdicts)

		}

	}

	return sysdicts, result.Error
}

func UpdateSysDict(sysMenu *entities.SystemDict) error {
	result := Database.Table("system_dict").Where("code=?", sysMenu.Code).UpdateColumns(&sysMenu)
	return result.Error
}
