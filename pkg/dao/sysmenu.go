package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

func CreateSysMenu(sysmenu entities.SysMenu) error {
	result := Database.Create(&sysmenu)
	fmt.Println("==========333333=============")
	fmt.Println(result.Error)
	fmt.Println(sysmenu.Haschild)
	return result.Error
}

func DeleteSysMenu(sysMenu entities.SysMenu) (bool, error) {
	result := Database.Delete(&sysMenu)
	var res bool = true
	if result.RowsAffected == 0 {
		res = false
	}
	return res, result.Error
}

func DeleteSysMenus(sysMenuIds []string) error {
	var sysmenu entities.SysMenu
	result := Database.Delete(&sysmenu, sysMenuIds)
	return result.Error
}

func GetSysMenuById(Id string) (entities.SysMenu, error) {
	var result entities.SysMenu

	err := Database.Where(("id=?"), Id).First(&result).Error

	return result, err
}

func GetSysMenuByParentId(ParentId string) ([]entities.SysMenu, error) {
	var result []entities.SysMenu

	err := Database.Where(("parent_id=?"), ParentId).Find(&result).Error

	return result, err
}

func GetSysMenuCount(queryCondition entities.QueryCondition, parent_id string) (int64, error) {
	var count int64
	var user entities.SysMenu
	var result *gorm.DB

	if queryCondition.LikeValue != "" {
		if parent_id == "" {
			result = Database.Model(&user).Where(
				`parent_id = '' AND (
				name LIKE ? OR
			category LIKE ? OR
			url LIKE ? OR
			status LIKE ? )`,
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%").Count(&count)
		} else {

			result = Database.Model(&user).Where(
				`parent_id = ? AND (
				name LIKE ? OR
				category LIKE ? OR
				url LIKE ? OR
				status LIKE ? )`,
				parent_id,
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%").Count(&count)

		}

	} else {
		if parent_id == "" {
			result = Database.Model(&user).Where(
				`parent_id = ''`).Count(&count)
		} else {

			result = Database.Model(&user).Where(
				`parent_id = ?`, parent_id).Count(&count)
		}
	}
	return count, result.Error
}

func QuerySysMenus(queryCondition entities.QueryCondition, parent_id string) ([]entities.SysMenu, error) {
	var sysMenu entities.SysMenu
	var sysMenus []entities.SysMenu
	var result *gorm.DB

	fmt.Println("****" + parent_id + "****")
	selectFields := `id,  name, Category, URL, Status, parent_id, "Modifier", "Creator", modify_datetime, create_datetime, haschild `
	if queryCondition.LikeValue != "" {
		if parent_id == "" {
			result = Database.Model(&sysMenu).Select(selectFields).Where(
				`parent_id = '' AND (
				name LIKE ? OR
			category LIKE ? OR
			url LIKE ? OR
			status LIKE ? )`,
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&sysMenus)
		} else {
			result = Database.Model(&sysMenu).Select(selectFields).Where(
				`parent_id = ? AND (
					name LIKE ? OR
					category LIKE ? OR
					url LIKE ? OR
					status LIKE ? )`,
				parent_id,
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%",
				"%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&sysMenus)
		}

	} else {
		if parent_id == "" {
			result = Database.Model(&sysMenu).
				Select(selectFields).
				Where(`parent_id = '' `).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&sysMenus)
		} else {

			result = Database.Model(&sysMenu).
				Select(selectFields).
				Where(
					`parent_id = ?`, parent_id).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&sysMenus)
		}

	}

	return sysMenus, result.Error
}

func QueryAllSysMenus() ([]entities.SysMenu, error) {
	var sysMenu entities.SysMenu
	var sysMenus []entities.SysMenu
	var result *gorm.DB

	selectFields := `id,  name, Category, URL, Status, parent_id, "Modifier", "Creator", modify_datetime, create_datetime, haschild `
	result = Database.Model(&sysMenu).Select(selectFields).
		Order("modify_datetime DESC").
		Find(&sysMenus)

	return sysMenus, result.Error
}

// TODO: 通过角色唯一编号获取所有绑定菜单
func EnumSysMenusFromSysRoleIds(roleId string) ([]entities.SysMenu, error) {
	var sysMenus []entities.SysMenu

	// select sys_menus.*
	// from rel_sysroles_sysmenus
	// left join public.sys_menus on sys_menus.id = rel_sysroles_sysmenus.menu_id
	// where rel_sysroles_sysmenus.role_id = 'e118134a-b513-4dc8-9b35-231fd3b42c2a'

	result := Database.Raw(`select sys_menus.* from rel_sysroles_sysmenus left join public.sys_menus on sys_menus.id = rel_sysroles_sysmenus.menu_id WHERE rel_sysroles_sysmenus.role_id =? ;`,
		roleId).Scan(&sysMenus)

	return sysMenus, result.Error
}

func UpdateSysMenus(sysMenu *entities.SysMenu) error {
	result := Database.Table("sys_menus").Where("id=?", sysMenu.Id).UpdateColumns(&sysMenu)
	return result.Error
}

func UpdateSysMenuHasChild(Id string, hasChild bool) error {
	result := Database.Table("sys_menus").Where("id=?", Id).Update("haschild", hasChild)
	return result.Error
}

func GetChildItem(Id string) (int64, error) {
	var count int64
	var sysMenu entities.SysMenu

	result := Database.Model(&sysMenu).Where(("parent_id=?"), Id).Count(&count)

	return count, result.Error
}
