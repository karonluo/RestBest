package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

// 创建订单菜品
func CreateOrderDish(orderDish entities.OrderDish) string {
	Database.Create(&orderDish)
	return orderDish.Id
}

// 使用ID删除订单菜品
func DeleteOrderDishById(id string) (bool, error) {
	var result bool = true

	res := Database.Exec("DELETE FROM order_dishes WHERE id=?", id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除订单: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除订单: %s 失败, 原因是: %s \r\n", id, "未找到该订单")
	}
	return result, res.Error
}

// 删除订单下所有菜品
func DeleteOrderDishByOrderID(order_id string) (bool, error) {
	var result bool = true

	res := Database.Exec("DELETE FROM order_dishes WHERE order_id=?", order_id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除订单下所有菜品: %s 失败, 原因是: %s \r\n", order_id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除订单下所有菜品: %s 失败, 原因是: %s \r\n", order_id, "未找到该订单")
	}
	return result, res.Error
}

// 查询订单菜品
func QueryOrderDishes(queryCondition entities.QueryCondition, table_id string, rest_id string) ([]entities.Order, error) {
	var order entities.Order
	var orders []entities.Order
	var result *gorm.DB
	selectFields := `id,rest_id, total_amount, actual_amount, refund_amount, status, create_datetime, modify_datetime, Creator, Modifier,guest_num,remark,service_charge,tax `

	if rest_id == "" && table_id == "" {

		result = Database.Model(&order).Select(selectFields).
			Order("modify_datetime DESC").
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&orders)
	} else if rest_id != "" && table_id == "" {

		result = Database.Table("rel_tables_orders").Select(selectFields).
			Joins("left join orders on rel_tables_orders.image.order_id = orders.id").
			Where(` rel_tables_orders.rest_id = ?`, rest_id).
			Order("modify_datetime DESC").
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&orders)
	} else if rest_id == "" && table_id != "" {

		result = Database.Table("rel_tables_orders").Select(selectFields).
			Joins("left join orders on rel_tables_orders.image.order_id = orders.id").
			Where(` rel_tables_orders.table_id = ?`, table_id).
			Order("modify_datetime DESC").
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&orders)
	} else if rest_id != "" && table_id != "" {

		result = Database.Table("rel_tables_orders").Select(selectFields).
			Joins("left join orders on rel_tables_orders.image.order_id = orders.id").
			Where(` rel_tables_orders.rest_id = ? and rel_tables_orders.table_id = ? `, rest_id, table_id).
			Order("modify_datetime DESC").
			Limit(int(queryCondition.PageSize)).
			Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			Find(&orders)
	}

	return orders, result.Error
}

// 查询订单数量
func GetOrderDishesCount(queryCondition entities.QueryCondition, table_id string, rest_id string) (int64, error) {
	var count int64
	var order entities.Order
	var result *gorm.DB

	selectFields := `id,rest_id, total_amount, actual_amount, refund_amount, status, create_datetime, modify_datetime, Creator, Modifier,guest_num,remark,service_charge,tax `

	if rest_id == "" && table_id == "" {

		result = Database.Model(&order).Select(selectFields).
			Count(&count)
	} else if rest_id != "" && table_id == "" {

		result = Database.Table("rel_tables_orders").Select(selectFields).
			Joins("left join orders on rel_tables_orders.image.order_id = orders.id").
			Where(` orders.rest_id = ?`, rest_id).
			Count(&count)
	} else if rest_id == "" && table_id != "" {

		result = Database.Table("rel_tables_orders").Select(selectFields).
			Joins("left join orders on rel_tables_orders.image.order_id = orders.id").
			Where(` rel_tables_orders.table_id = ?`, table_id).
			Count(&count)
	} else if rest_id != "" && table_id != "" {

		result = Database.Table("rel_tables_orders").Select(selectFields).
			Joins("left join orders on rel_tables_orders.image.order_id = orders.id").
			Where(` orders.rest_id = ? and rel_tables_orders.table_id = ? `, rest_id, table_id).
			Count(&count)
	}

	return count, result.Error
}

// func UpdateOrder(rest *entities.Order) error {
// 	result := Database.Table("orders").Where("id=?", rest.Id).UpdateColumns(&rest)
// 	return result.Error
// }

// // 设置订单与餐桌关联
// func OrderJoinInTables(order_id string, table_ids []string) error {
// 	var rolePages []map[string]interface{}
// 	for _, table_id := range table_ids {
// 		rolePage := make(map[string]interface{})
// 		rolePage["order_id"] = order_id
// 		rolePage["table_id"] = table_id

// 		fmt.Println("DishJoinInToppings:order_id" + order_id)
// 		fmt.Println("DishJoinInToppings:table_id" + table_id)
// 		rolePages = append(rolePages, rolePage)
// 	}
// 	return Database.Table("rel_tables_orders").Create(&rolePages).Error
// }

// func ClearTablesOrders(order_id string) error {
// 	return Database.Exec("DELETE FROM rel_tables_orders WHERE order_id = ?", order_id).Error
// }
