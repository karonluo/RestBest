package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

func CreateSysRole(role entities.SysRole) error {
	result := Database.Create(&role)
	return result.Error
}

// 将系统角色和系统功能页面进行绑定
func SysRoleJoinInSysMenus(sysRoleId string, sysMenuIds []string) error {
	var rolePages []map[string]interface{}
	for _, sysMenuId := range sysMenuIds {
		rolePage := make(map[string]interface{})
		rolePage["role_id"] = sysRoleId
		rolePage["menu_id"] = sysMenuId
		rolePages = append(rolePages, rolePage)
	}
	return Database.Table("rel_sysroles_sysmenus").Create(&rolePages).Error
}

func ClearSysRoleSysMenus(sysRoleId string) error {
	return Database.Exec("DELETE FROM rel_sysroles_sysmenus WHERE role_id = ?", sysRoleId).Error
}

func GetSysRoleById(sysRoleId string) (entities.SysRole, error) {
	var role entities.SysRole
	role.Id = sysRoleId
	err := Database.First(&role).Error
	return role, err
}

func GetSysRoleCount(companyQueryCodition entities.QueryCondition) (int64, error) {
	var count int64
	var role entities.SysRole
	var result *gorm.DB
	if companyQueryCodition.LikeValue != "" {
		result = Database.Model(&role).Where(`name LIKE ? OR 
		description LIKE ?`,
			"%"+companyQueryCodition.LikeValue+"%",
			"%"+companyQueryCodition.LikeValue+"%").Count(&count)
	} else {
		result = Database.Model(&role).Count(&count)
	}
	return count, result.Error
}

func QuerySysRoles(queryCodition entities.QueryCondition) ([]entities.SysRole, error) {
	var role entities.SysRole
	var roles []entities.SysRole
	var result *gorm.DB
	selectFields := `id, name, description, Modifier, Creator, modify_datetime, create_datetime`
	if queryCodition.LikeValue != "" {

		result = Database.Model(&role).Select(selectFields).Where(`name LIKE ? or description LIKE ?`,
			"%"+queryCodition.LikeValue+"%",
			"%"+queryCodition.LikeValue+"%").
			Order("modify_datetime DESC").
			Limit(int(queryCodition.PageSize)).
			Offset(int(queryCodition.PageSize * (queryCodition.PageIndex - 1))).
			Find(&roles)

	} else {
		result = Database.Model(&role).
			Select(selectFields).
			Order("modify_datetime DESC").
			Limit(int(queryCodition.PageSize)).
			Offset(int(queryCodition.PageSize * (queryCodition.PageIndex - 1))).
			Find(&roles)
	}
	return roles, result.Error
}

func UpdateSysRole(role *entities.SysRole) error {
	return Database.Table("sys_roles").Where("id=?", role.Id).UpdateColumns(&role).Error
}

// 根据角色编号获取其功能页面
func WSEnumAllSysMenusByRoleId(sysRoleId string) ([]entities.SysMenu, error) {
	var pages []entities.SysMenu
	result := Database.Table("rel_sysroles_sysmenus").Select("sys_menus.*").
		Joins("left join sys_menus on sys_menus.id = rel_sysroles_sysmenus.menu_id").
		Where("rel_sysroles_sysmenus.role_id=?", sysRoleId).Find(&pages)
	return pages, result.Error
}

// 删除系统角色
func DeleteSysRoleById(id string) (bool, error) {
	var result bool = true
	var menu entities.Menu
	menu.Id = id
	res := Database.Exec("DELETE FROM sys_roles WHERE id=?", id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除系统角色: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除系统角色: %s 失败, 原因是: %s \r\n", id, "未找到该系统角色:")
	}
	return result, res.Error
}
