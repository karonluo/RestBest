package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

// 菜品
func CreateDishSpec(dishspec entities.DishSpec) string {
	Database.Create(&dishspec)
	return dishspec.Id

}

func DeleteDishSpecById(id string) (bool, error) {
	var result bool = true
	var menu entities.Menu
	menu.Id = id
	res := Database.Exec("DELETE FROM dish_specs WHERE id=?", id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除菜品规格: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除菜品规格: %s 失败, 原因是: %s \r\n", id, "未找到该菜品规格")
	}
	return result, res.Error
}

func DeleteDishSpec(dishspec entities.DishSpec) (bool, error) {
	result := Database.Delete(&dishspec)
	var res bool = true
	if result.RowsAffected == 0 {
		res = false
	}
	return res, result.Error
}

func QueryDishesSpec(queryCondition entities.QueryCondition, dishes_id string) ([]entities.DishSpec, error) {
	var dish entities.DishSpec
	var dishes []entities.DishSpec
	var result *gorm.DB
	selectFields := `id,dishes_id,name,price,calories,create_datetime,modify_datetime,"Creator","Modifier",unit`
	if queryCondition.LikeValue != "" {
		if dishes_id == "" {
			result = Database.Model(&dish).Select(selectFields).Where(
				`name LIKE ? `,
				"%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&dishes)
		} else {
			result = Database.Model(&dish).Select(selectFields).Where(
				`dishes_id = ? and name LIKE ? `,
				dishes_id, "%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&dishes)
		}

	} else {
		if dishes_id == "" {
			result = Database.Model(&dish).
				Select(selectFields).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&dishes)
		} else {
			result = Database.Model(&dish).
				Select(selectFields).
				Where(`dishes_id = ?  `, dishes_id).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&dishes)
		}

	}

	return dishes, result.Error
}

func GetDishesSpecCount(queryCondition entities.QueryCondition, dishes_id string) (int64, error) {
	var count int64
	var menu entities.DishSpec
	var result *gorm.DB

	if queryCondition.LikeValue != "" {
		if dishes_id == "" {
			result = Database.Model(&menu).Where(
				`name LIKE ?`,
				"%"+queryCondition.LikeValue+"%").Count(&count)
		} else {
			result = Database.Model(&menu).Where(
				`dishes_id = ? and name LIKE ? `,
				dishes_id, "%"+queryCondition.LikeValue+"%").Count(&count)
		}

	} else {
		if dishes_id == "" {
			result = Database.Model(&menu).Count(&count)
		} else {
			result = Database.Model(&menu).Where(
				`dishes_id = ? `,
				dishes_id).Count(&count)
		}

	}
	return count, result.Error
}

func UpdateDishSpec(rest *entities.DishSpec) error {
	result := Database.Table("dish_specs").Where("id=?", rest.Id).UpdateColumns(&rest)
	return result.Error
}
