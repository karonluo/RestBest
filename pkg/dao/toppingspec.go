package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

// 创建配菜规格
func CreateToppingSpec(toppingSpec entities.ToppingSpec) string {
	Database.Create(&toppingSpec)
	return toppingSpec.Id

}

// 删除配菜规格
func DeleteToppingSpecById(id string) (bool, error) {
	var result bool = true

	res := Database.Exec("DELETE FROM topping_specs WHERE id = ?", id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除配菜规格: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除配菜规格: %s 失败, 原因是: %s \r\n", id, "未找到该配菜")
	}
	return result, res.Error

}

/**/
// 分页查询 配菜规格
// dishes_id : 配菜父级的菜品ID
func QueryToppingSpeces(queryCondition entities.QueryCondition, topping_id string) ([]entities.ToppingSpec, error) {
	var toppingSpec entities.ToppingSpec
	var toppingSpeces []entities.ToppingSpec
	var result *gorm.DB
	selectFields := `id,topping_id,name, price, calories, create_datetime,modify_datetime,Creator,Modifier,status,unit `
	if queryCondition.LikeValue != "" {
		if topping_id == "" {
			result = Database.Model(&toppingSpec).Select(selectFields).Where(
				`name LIKE ? `,
				"%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&toppingSpeces)
		} else {
			result = Database.Model(&toppingSpec).Select(selectFields).Where(
				`name LIKE ? and topping_id = ?`,
				"%"+queryCondition.LikeValue+"%", topping_id).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&toppingSpeces)
		}

	} else {
		if topping_id == "" {
			result = Database.Model(&toppingSpec).
				Select(selectFields).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&toppingSpeces)
		} else {
			result = Database.Model(&toppingSpec).
				Select(selectFields).
				Where(
					`topping_id = ?`,
					topping_id).
				Order("modify_datetime DESC").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&toppingSpeces)
		}

	}

	return toppingSpeces, result.Error
}

// TODO 补充关联查询
func GetToppingSpecCount(queryCondition entities.QueryCondition, toppings_id string) (int64, error) {
	var count int64
	var toppingSpec entities.ToppingSpec
	var result *gorm.DB

	if queryCondition.LikeValue != "" {

		// result = Database.Model(&topping).Where(
		// 	`name LIKE ?`,
		// 	"%"+queryCodition.LikeValue+"%").Count(&count)

		if toppings_id == "" {
			result = Database.Model(&toppingSpec).Where(
				`name LIKE ? `,
				"%"+queryCondition.LikeValue+"%").
				Count(&count)
		} else {
			result = Database.Model(&toppingSpec).Where(
				`name LIKE ? and toppings_id = ?`,
				"%"+queryCondition.LikeValue+"%", toppings_id).
				Count(&count)
		}

	} else {

		// result = Database.Model(&topping).Count(&count)
		if toppings_id == "" {
			result = Database.Model(&toppingSpec).Count(&count)
		} else {
			result = Database.Model(&toppingSpec).
				Where(
					`toppings_id = ?`,
					toppings_id).
				Count(&count)
		}

	}
	return count, result.Error
}

func UpdateToppingSpec(toppingSpec *entities.ToppingSpec) error {
	result := Database.Table("topping_specs").Where("id=?", toppingSpec.Id).UpdateColumns(&toppingSpec)
	return result.Error
}

func GetToppingSpecById(Id string) (entities.ToppingSpec, bool, error) {
	var result entities.ToppingSpec
	var bres bool = false

	res := Database.Where(("id=?"), Id).First(&result)

	if res.RowsAffected == 1 {
		bres = true
	}

	return result, bres, res.Error

}
