package dao

import (
	"RestBest/pkg/entities"
	"fmt"

	"gorm.io/gorm"
)

func CreateMenu(menu entities.Menu) string {
	Database.Create(&menu)
	return menu.Id

}

func DeleteMenuById(id string) (bool, error) {
	var result bool = true
	var menu entities.Menu
	menu.Id = id
	res := Database.Exec("DELETE FROM menus WHERE id=?", id)
	if res.Error != nil {
		result = false
		fmt.Printf("删除系统用户: %s 失败, 原因是: %s \r\n", id, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		result = false
		fmt.Printf("删除系统用户: %s 失败, 原因是: %s \r\n", id, "未找到该系统用户")
	}
	return result, res.Error
}

func DeleteMenu(menu entities.Menu) (bool, error) {
	result := Database.Delete(&menu)
	var res bool = true
	if result.RowsAffected == 0 {
		res = false
	}
	return res, result.Error
}

func QueryMenus(queryCondition entities.QueryCondition, restaurantID string) ([]entities.Menu, error) {
	//var menu entities.Menu
	var menus []entities.Menu
	var result *gorm.DB
	//selectFields := `id,rest_id,name,category,image,small_image,banner,create_datetime,modify_datetime, "Creator", "Modifier","order",status`
	if queryCondition.LikeValue != "" {
		if restaurantID != "" {
			// result = Database.Model(&menu).Select(selectFields).Where(
			// 	`name LIKE ? AND rest_id = ?`,
			// 	"%"+queryCondition.LikeValue+"%", restaurantID).
			// 	Order("modify_datetime DESC").
			// 	Limit(int(queryCondition.PageSize)).
			// 	Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			// 	Find(&menus)

			result = Database.Table("public.menus").
				Select("menus.id, menus.rest_id, menus.name, menus.category, menus.image, menus.small_image,menus.banner, menus.create_datetime, menus.modify_datetime, menus.\"Creator\",Modifier, \"order\", menus.status, '[' || string_agg( '{ \"id\":\"' || dishes.id ||'\",\"name\":\"' || dishes.name || '\"}' ,',') ||  ']'  as dishes").
				Joins("left join rel_menus_dishes on menus.id = rel_menus_dishes.menu_id").
				Joins(" left join dishes on dishes.id =  rel_menus_dishes.dishes_id").
				Where(`menus.name LIKE ? AND menus.rest_id = ?`, "%"+queryCondition.LikeValue+"%", restaurantID).
				Order("modify_datetime DESC").
				Group("menus.id, menus.rest_id,menus.name,menus.modify_datetime,menus.\"Creator\",Modifier, menus.status").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&menus)

		} else {
			// result = Database.Model(&menu).Select(selectFields).Where(
			// 	`name LIKE ? `,
			// 	"%"+queryCondition.LikeValue+"%").
			// 	Order("modify_datetime DESC").
			// 	Limit(int(queryCondition.PageSize)).
			// 	Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
			// 	Find(&menus)

			result = Database.Table("public.menus").
				Select("menus.id, menus.rest_id, menus.name, menus.category, menus.image, menus.small_image,menus.banner, menus.create_datetime, menus.modify_datetime, menus.\"Creator\",Modifier, \"order\", menus.status, '[' || string_agg( '{ \"id\":\"' || dishes.id ||'\",\"name\":\"' || dishes.name || '\"}' ,',') ||  ']'  as dishes").
				Joins("left join rel_menus_dishes on menus.id = rel_menus_dishes.menu_id").
				Joins(" left join dishes on dishes.id =  rel_menus_dishes.dishes_id").
				Where(`menus.name LIKE ? `, "%"+queryCondition.LikeValue+"%").
				Order("modify_datetime DESC").
				Group("menus.id, menus.rest_id,menus.name,menus.modify_datetime,menus.\"Creator\",Modifier, menus.status").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&menus)

		}

	} else {
		if restaurantID == "" {

			result = Database.Table("public.menus").
				Select("menus.id, menus.rest_id, menus.name, menus.category, menus.image, menus.small_image,menus.banner, menus.create_datetime, menus.modify_datetime, menus.\"Creator\",Modifier, \"order\", menus.status, '[' || string_agg( '{ \"id\":\"' || dishes.id ||'\",\"name\":\"' || dishes.name || '\"}' ,',') ||  ']'  as dishes").
				Joins("left join rel_menus_dishes on menus.id = rel_menus_dishes.menu_id").
				Joins(" left join dishes on dishes.id =  rel_menus_dishes.dishes_id").
				Order("modify_datetime DESC").
				Group("menus.id, menus.rest_id,menus.name,menus.modify_datetime,menus.\"Creator\",Modifier, menus.status").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&menus)

		} else {

			fmt.Println("====================================")
			fmt.Println(restaurantID)
			result = Database.Table("public.menus").
				Select("menus.id, menus.rest_id, menus.name, menus.category, menus.image, menus.small_image,menus.banner, menus.create_datetime, menus.modify_datetime, menus.\"Creator\",Modifier, \"order\", menus.status, '[' || string_agg( '{ \"id\":\"' || dishes.id ||'\",\"name\":\"' || dishes.name || '\"}' ,',') ||  ']'  as dishes").
				Joins("left join rel_menus_dishes on menus.id = rel_menus_dishes.menu_id").
				Joins(" left join dishes on dishes.id =  rel_menus_dishes.dishes_id").
				Where(`  menus.rest_id = ?`, restaurantID).
				Order("modify_datetime DESC").
				Group("menus.id, menus.rest_id,menus.name,menus.modify_datetime,menus.\"Creator\",Modifier, menus.status").
				Limit(int(queryCondition.PageSize)).
				Offset(int(queryCondition.PageSize * (queryCondition.PageIndex - 1))).
				Find(&menus)

		}
	}

	return menus, result.Error
}

func GetMenusCount(queryCodition entities.QueryCondition, restaurantID string) (int64, error) {
	var count int64
	var menu entities.Menu
	var result *gorm.DB

	if queryCodition.LikeValue != "" {
		if restaurantID != "" {
			result = Database.Model(&menu).Where(
				`(name LIKE ? ) AND rest_id=?`,
				"%"+queryCodition.LikeValue+"%", restaurantID).Count(&count)
		} else {
			result = Database.Model(&menu).Where(
				`name LIKE ?`,
				"%"+queryCodition.LikeValue+"%").Count(&count)
		}
	} else {
		if restaurantID != "" {
			result = Database.Model(&menu).Where("rest_id", restaurantID).Count(&count)
		} else {
			result = Database.Model(&menu).Count(&count)
		}
	}
	return count, result.Error
}

func UpdateMenu(rest *entities.Menu) error {
	result := Database.Table("menus").Where("id=?", rest.Id).UpdateColumns(&rest)
	return result.Error
}

// 设置菜单与菜品关联
func MenuJoinInDishes(menuId string, dishesIds []string, restId string) error {
	var rolePages []map[string]interface{}
	for _, dishesId := range dishesIds {
		rolePage := make(map[string]interface{})
		rolePage["menu_id"] = menuId
		rolePage["dishes_id"] = dishesId
		rolePage["rest_id"] = restId
		rolePages = append(rolePages, rolePage)
	}
	return Database.Table("rel_menus_dishes").Create(&rolePages).Error
}

func ClearMenuDishes(menuId string) error {
	return Database.Exec("DELETE FROM rel_menus_dishes WHERE menu_id = ?", menuId).Error
}
