package cache

import (
	"RestBest/conf"
	"RestBest/pkg/entities"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
)

var RedisDatabase *redis.Client

//TODO: Switch to redis-cluster mode, when redis-cluster service is available
//var RedisDatabase *redis.ClusterClient

func InitRedisDatabase() bool {
	var res bool
	rdbconf := conf.WebConfiguration.RedisConf
	fmt.Print("内存数据库初始化")
	RedisDatabase = redis.NewClient(&redis.Options{
		Addr:     rdbconf.Hosts[0],
		Password: rdbconf.Password,
		DB:       0,
		PoolSize: 100,
	})
	// if len(rdbconf.Host) == 0 {
	// 	RedisDatabase = redis.NewClient(&redis.Options{
	// 		Addr:     rdbconf.Host[0],
	// 		Password: rdbconf.Password,
	// 		DB:       rdbconf.DBId, // use default DB
	// 		PoolSize: 100,
	// 	})
	// } else {

	// }
	// TODO: Switch to redis-cluster mode, when redis-cluster service is available
	// RedisDatabase = redis.NewClusterClient(&redis.ClusterOptions{
	// 	Addrs:    rdbconf.Hosts,
	// 	Password: rdbconf.Password,
	// 	PoolSize: 100,
	// })

	ctx := context.Background()
	result := RedisDatabase.Ping(ctx)
	if result.Val() == "PONG" {
		res = true
		fmt.Println("......成功")
	} else {
		res = false
		fmt.Println("......失败")
		fmt.Println(result.Err().Error())
	}
	return res

}

func WriteSystemLogToRedis(log *entities.SystemLog) error {
	bytes, err := json.Marshal(log)
	if err == nil {
		rctx := context.Background()
		cmd := RedisDatabase.LPush(rctx, "restbest_SystemLog", string(bytes))
		if cmd.Err() != nil {
			err = cmd.Err()
		}
	}
	return err
}

// 将用户信息放置到 Redis 缓存中。
// 当设置为0分钟时，按照24小时设置超时时间
func SetUserToRedis(user entities.SysUser, timeOutDurationMinute int) {
	rctx := context.Background()
	result, _ := json.Marshal(&user)
	if timeOutDurationMinute == 0 {
		RedisDatabase.Set(rctx, "restbest_sysuser_"+user.LoginName, string(result), time.Hour*24)
	} else {

		tmpTimeout := time.Duration(timeOutDurationMinute) * time.Minute
		RedisDatabase.Set(rctx, "restbest_sysuser_"+user.LoginName, string(result), tmpTimeout)
	}

}

// 将用户信息放置到 Redis 缓存中。
// 当设置为0分钟时，按照24小时设置超时时间
func SetTenantUserToRedis(user entities.TenantUser, timeOutDurationMinute int) {
	rctx := context.Background()
	result, _ := json.Marshal(&user)
	if timeOutDurationMinute == 0 {
		RedisDatabase.Set(rctx, "restbest_tenantusers_"+user.LoginName, string(result), time.Hour*24)
	} else {

		tmpTimeout := time.Duration(timeOutDurationMinute) * time.Minute
		RedisDatabase.Set(rctx, "restbest_tenantusers_"+user.LoginName, string(result), tmpTimeout)
	}

}
