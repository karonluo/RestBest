package web

import (
	"RestBest/pkg/biz"
	"RestBest/pkg/services"
	"RestBest/pkg/tools"
	"encoding/json"
	"fmt"

	"github.com/kataras/iris/v12"
)

func Index(ctx iris.Context) {
	ctx.HTML("<h1>Hello, World!</h1>")
}

type WebMessage struct {
	StatusCode int
	Message    interface{}
}

func TopCheck(ctx iris.Context) {
	ctx.Header("Access-Control-Allow-Origin", "*")
	ctx.Header("Access-Control-Allow-Credentials", "true")
	ctx.Header("Access-Control-Allow-Methods", "*")
	ctx.Header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers,Authorization,User-Agent, Keep-Alive, Content-Type, X-Requested-With,X-CSRF-Token,AccessToken,Token")
	ctx.Next()
}

// 拦截器，用于验证用户信息和权限信息
func Before(ctx iris.Context) {
	ctx.Header("Access-Control-Allow-Origin", "*")
	if ctx.Request().Method == "OPTIONS" {
		ctx.Header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,PATCH,OPTIONS")
		ctx.Header("Access-Control-Allow-Headers", "Content-Type, Accept, Authorization")
		ctx.Header("Access-Control-Allow-Credentials", "true")
		ctx.StatusCode(204)
		return
	}
	authorization := ctx.GetHeader("Authorization")
	requestPath := ctx.RequestPath(true)
	if authorization == "" {
		if requestPath != "/login" && requestPath != "/download" {
			message := WebMessage{Message: "未登录", StatusCode: 401}
			fmt.Println("Authorization Error!")
			ctx.StatusCode(401)
			ctx.JSON(message)
		} else {
			ctx.Next()
		}

	} else {
		// TODO: Check authorization in redis
		res, _ := biz.CheckLogin(authorization) //sss
		// TODO: 为了方便测试，authorization 为 test 时，直接通过，不进行权限检测。
		if authorization == "test" {
			ctx.Next()
			return
		}
		if res {
			acl, err := biz.GetLoginInformation(authorization, "acl") // 获取访问权限
			if err != nil {
				tools.ProcessError("web.Before", `acl, err := biz.GetLoginInformation(authorization, "acl")`, err)
				message := WebMessage{Message: "无访问权限", StatusCode: 403}
				ctx.StatusCode(403)
				ctx.JSON(message)
			} else {

				var t []interface{} // = auto / 未知 T[]
				json.Unmarshal([]byte(acl), &t)
				if tools.HaveElementInRange(t, requestPath) { // 匹配请求的地址
					ctx.Next()
				} else {
					fmt.Println("test 403")
					fmt.Println(t)
					message := WebMessage{Message: "无访问权限", StatusCode: 403}
					ctx.StatusCode(403)
					ctx.JSON(message)
				}
			}
		} else {
			message := WebMessage{Message: "未登录", StatusCode: 401}
			ctx.StatusCode(401)
			ctx.JSON(message)
		}
	}
}

func Cors(ctx iris.Context) {
	ctx.Text("")
}

func RegisterServices(app *iris.Application) {

	app.Get("/index", Index)

	RegisterMenuServices(app)

	RegisterDishServices(app)

	RegisterSysUserServices(app)

	RegisterRestaurantsServices(app)

	RegisterTenantUserServices(app)
	RegisterToppingServices(app)
	// RegisterSportsCompanyServices(app)
	// RegisterSwimmerServices(app)
	// RegisterSiteServices(app)
	// RegisterSportsCompanyMgtGroup(app)
	// RegisterUWBMgtServices(app)
	RegisterSystemManagement(app)
	// RegisterCalendarService(app)
	// RegisterWifiDogService(app)
	// app.Get("/siteowners/list", services.WSEnumSiteOwners) //列举场地负责人
	// app.Put("/siteowners/setowners", services.WSSetSiteOwners) //设置场地负责人

	RegisterOrdersServices(app)
}

// 注册菜单服务
func RegisterMenuServices(app *iris.Application) {
	app.Post("/menu", services.WSCreateMenu)
	app.Get("/menu", services.WSGetMenu)
	app.Delete("/menu", services.WSDeleteMenu)
	app.Get("/menus/query", services.WSQueryMenus)
	app.Put("/menu", services.WSUpdateMenu)
	app.Put("/menu/joindishes", services.WSMenuJoinInDishes) // 新增菜单与菜品关联

	app.Get("/menu/fullmenu", services.WSQueryFullMenus) // 查询单个商店下所有菜单、菜品、配菜列表
}

// 注册菜品服务
func RegisterDishServices(app *iris.Application) {

	app.Post("/dish", services.WSCreateDish)
	app.Get("/dish", services.WSGetDishById)
	app.Delete("/dish", services.WSDeleteDish)
	app.Get("/dish/query", services.WSQueryDishes)
	app.Put("/dish", services.WSUpdateDish)
	app.Put("/dish/jointoppings", services.WSDishJoinToppings) // 设置菜品配菜关系

	app.Post("/dishspec", services.WSCreateDishSpec)
	app.Get("/dishspec", services.WSGetDishSpecById)
	app.Delete("/dishspec", services.WSDeleteDishSpec)
	app.Get("/dishspec/query", services.WSQueryDishesSpec)
	app.Put("/dishspec", services.WSUpdateDishSpec)

}

// 注册配菜服务
func RegisterToppingServices(app *iris.Application) {

	app.Post("/topping", services.WSCreateTopping)
	app.Get("/topping", services.WSGetTopping)
	app.Delete("/topping", services.WSDeleteTopping)
	app.Get("/topping/query", services.WSQueryToppings)
	app.Put("/topping", services.WSUpdateTopping)

	app.Post("/toppingspec", services.WSCreateToppingSpec)
	app.Get("/toppingspec", services.WSGetToppingSpec)
	app.Delete("/toppingspec", services.WSDeleteToppingSpec)
	app.Get("/toppingspec/query", services.WSQueryToppingSpec)
	app.Put("/toppingspec", services.WSUpdateToppingSpec)

}

// 绑定用户相关WEB服务
func RegisterSysUserServices(app *iris.Application) {

	app.Post("/sysuser", services.WSCreateUser)           //新增系统用户
	app.Delete("/sysuser", services.WSDeleteSysUser)      //删除系统用户
	app.Put("/sysuser", services.WSUpdateSysUser)         // 修改系统用户
	app.Get("/sysuser", services.WSGetSysUserByLoginName) //获取系统用户信息(通过登录名)

	app.Post("/login", services.WSLoginSystem)                                             //登录系统
	app.Get("/sysuser/listall", services.WSEnumSysUsers)                                   //列举系统用户
	app.Get("/sysuser/listall/fromcompanies", services.WSEnumSysUsersFromSportsCompanyIds) // 通过体育公司唯一编号集合获取所有下属系统用户

	app.Get("/sysuser/query", services.WSQuerySysUsers) // 查询系统用户列表
	app.Delete("/sysusers", services.WSDeleteSysUsers)  // 批量删除用户

	app.Put("/sysuser/sysroles", services.WSSysUserJoinInSysRoles) // 设置系统用户关联角色

	app.Get("/athorizationinfo", services.WSGetLoginInformation) //获取登录信息
}

// 绑定商家用户相关WEB服务
func RegisterTenantUserServices(app *iris.Application) {

	app.Post("/tenantuser", services.WSCreateTenantUser)        //新增商家用户
	app.Delete("/tenantuser", services.WSDeleteTenantUser)      //删除商家用户
	app.Put("/tenantuser", services.WSUpdateTenantUser)         // 修改商家用户
	app.Get("/tenantuser", services.WSGetTenantUserByLoginName) // 商家用户详细信息
	app.Get("/tenantuser/query", services.WSQueryTenantUsers)   // 商家用户列表

	// app.Get("/tenantusers", services.WSGetSysUserByLoginName) //获取商家用户信息(通过登录名)
	// app.Post("/login", services.WSLoginSystem) // 登录系统
	// app.Get("/sysuser/listall", services.WSEnumSysUsers)                                   //列举系统用户
	// app.Get("/sysuser/listall/fromcompanies", services.WSEnumSysUsersFromSportsCompanyIds) // 通过体育公司唯一编号集合获取所有下属系统用户

	// app.Get("/sysuser/query", services.WSQuerySysUsers) // 查询系统用户列表
	// app.Delete("/sysusers", services.WSDeleteSysUsers)  // 批量删除用户
	// app.Put("/sysuser/sysroles", services.WSSysUserJoinInSysRoles) // 设置系统用户关联角色
	// app.Get("/athorizationinfo", services.WSGetLoginInformation) //获取登录信息
}

// 绑定系统管理相关服务
func RegisterSystemManagement(app *iris.Application) {
	app.Post("/role", services.WSCreateSysRole)                       // 创建系统角色
	app.Get("/role", services.WSGetRoleByRoleId)                      // 获取系统角色信息
	app.Put("/role/joinsysmenu", services.WSSysRoleJoinInSysMenus)    // 设置角色和后台页面绑定
	app.Get("/role/enumsysmenus", services.WSEnumAllSysMenusByRoleId) // 根据角色查询其下所有后台页面
	app.Get("/role/query", services.WSQuerySysRoles)                  // 查询系统角色
	app.Put("/role", services.WSUpdateSysRole)                        // 更新角色信息
	app.Delete("/role", services.WSDeleteSysRole)                     // 删除角色信息

	// app.Post("/sysfuncpage", services.WSCreateSysFuncPage)            // 创建系统功能页面
	// app.Delete("/sysfuncpage", services.WSDeleteSysFuncPage)          // 删除系统功能界面
	// app.Get("/sysfuncpage/list", services.WSEnumSysFuncPages)         //列举所有系统功能页面
	// app.Post("/systemlog/operationlog", services.WSWriteOperationLog) // 创建操作日志
	// app.Get("/athorizationinfo", services.WSGetLoginInformation)      //获取登录信息

	app.Post("/sysmenu", services.WSCreateSysMenu)                       // 创建系统菜单
	app.Get("/sysmenu/query", services.WSQuerySysMenus)                  // 查询系统菜单
	app.Get("/sysmenu/query/all", services.WSQueryAllSysMenus)           // 查询系统菜单
	app.Get("/sysmenu", services.WSGetSysMenuById)                       // 查询系统菜单明细
	app.Put("/sysmenu", services.WSUpdateSysMenu)                        // 更新系统菜单
	app.Delete("/sysmenu", services.WSDeleteSysMenu)                     // 删除系统菜单
	app.Get("/sysmenu/query/childlist", services.WSGetSysMenuByParentId) // 查询系统菜单

	app.Post("/systemdict", services.WSCreateSysDict)      // 创建系统字典
	app.Get("/systemdict/query", services.WSQuerySysDicts) // 查询系统字典
	app.Get("/systemdict", services.WSGetSysDictByCode)    // 查询系统字典明细
	app.Put("/systemdict", services.WSUpdateSysDict)       // 更新系统字典
	app.Delete("/systemdict", services.WSDeleteSysDict)    // 删除系统字典
}

func RegisterRestaurantsServices(app *iris.Application) {
	app.Post("/rest", services.WSCreateRest)       // 新增商家
	app.Delete("/rest", services.WSDeleteRest)     //删除商家
	app.Put("/rest", services.WSUpdateRest)        // 修改商家
	app.Get("/rest", services.WSGetRestById)       //获取商家明细
	app.Get("/rests/query", services.WSQueryRests) //获取商家列表

	app.Post("/restgroup", services.WSCreateRestGroup)      // 新增商家组
	app.Delete("/restgroup", services.WSDeleteRestGroup)    //删除商家组
	app.Put("/restgroup", services.WSUpdateRestGroup)       // 修改商家组
	app.Get("/restgroup", services.WSGetRestGroupById)      //获取商家组明细
	app.Get("/restgroup/query", services.WSQueryRestGroups) //获取商家组列表

	app.Get("/restgroup/queryrest", services.WSQueryRestGroupRests) // 根据商家组id获取商家组内商家

	app.Put("/restgroup/joinrest", services.WSRestGroupJoinInRests)             // 绑定商家组 商家关系
	app.Put("/restgroup/jointenantuser", services.WSRestGroupJoinInTenantusers) // 绑定商家组 商家用户关系

	app.Post("/table", services.WSCreateTable)      // 创建商家餐桌
	app.Delete("/table", services.WSDeleteTable)    //删除商家餐桌
	app.Put("/table", services.WSUpdateTable)       // 修改商家餐桌
	app.Get("/table", services.WSGetTable)          // 商家餐桌详细信息
	app.Get("/table/query", services.WSQueryTables) // 商家餐桌列表

}

func RegisterOrdersServices(app *iris.Application) {
	app.Post("/order", services.WSCreateOrder)     // 新增订单
	app.Delete("/order", services.WSDeleteRest)    // 删除订单
	app.Put("/order", services.WSUpdateRest)       // 修改订单
	app.Get("/order", services.WSGetRestById)      //获取订单明细
	app.Get("/order/query", services.WSQueryRests) //获取订单列表

}
